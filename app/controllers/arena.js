const router = require('express')();
const connector = require('../systems/connector')
const { validateToken } = require('../middleware')
const { arenaService } = require('../services')
const cache = require('../config/cache')

router.put('/',
  validateToken(true),
  async (req, res) => {
    arenaService.checkOpponent(req.user)
    res.send(200)
  }
)

router.delete('/',
  validateToken(true),
  async (req, res) => {
    arenaService.stopSearch(req.user)
    res.send(200)
  }
)

router.get('/my-logs',
  validateToken(true),
  async (req, res) => {
    const logs = await connector.end(
      connector.select()
      .from('arena_log','al')
      .field('al.rate')
      .field('ul.name', 'user_loss_name').field('ul.id', 'user_loss_id')
      .field('uw.name', 'user_win_name').field('uw.id', 'user_win_id')
      .left_join('users', 'uw', 'uw.id = al.user_win_id')
      .left_join('users', 'ul', 'ul.id = al.user_loss_id')
      .where(`al.user_win_id = ${req.user.id} OR al.user_loss_id = ${req.user.id}`)
      .limit(6)
      .toString()
    )
    res.send(200, logs)
  }
)

router.get('/top',
  async (req, res) => {
    try{
      res.send(200, cache.get( "arenaTop", true ));
    } catch( err ){
      res.send(200, [])
    }

  }
)

module.exports = router;
