const connector = require('../../systems/connector')
let dataCraftRecipes = {}

const getRecipes = async () => {
  let result = {}
  const recipes = await connector.end(
    connector.select()
    .from('craft_recipes', 'cr')
    .field('i.name', 'result_name')
    .field('i.img', 'result_img')
    .field('i.discription', 'result_discription')
    .field('cr.*')
    .field('crt.slug', 'type_slug')
    .left_join('items', 'i', 'i.id = cr.item_result')
    .left_join('craft_recipes_types', 'crt', 'crt.id = cr.craft_recipe_type')
    .order('cr.id')
    .toString()
  )

  await Promise.all(recipes.map( async (recipe) => {
    recipe.items = await connector.end(
      connector.select()
      .from('craft_recipes_items', 'cri')
      .field('i.*')
      .field('cri.id')
      .field('cri.count')
      .field('i.id', 'item_id')
      .left_join('items', 'i', 'i.id = cri.item_id')
      .where('cri.craft_recipe_id = ?', recipe.id)
      .toString()
    )

    if (!result[recipe.type_slug]) result[recipe.type_slug] = []
    result[recipe.type_slug].push(recipe)
  }))

  Object.keys(result).forEach(key => {
    result[key] = result[key].sort((a, b) => a.id - b.id)
  })

  dataCraftRecipes = result


}

getRecipes()

const getAll = (level) => {
  return dataCraftRecipes
}


module.exports = {
  getAll
}
