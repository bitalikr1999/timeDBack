const { validateReq, parseDb } = require('../middleware')
const connector = require('../systems/connector')
const { jwtService, charactersService } = require('../services')
const logger = require('../systems/logger')
const router = require('express')();
const config = require('../config')
const async = require('async')
const md5 = require('md5')

router.post('/',
  validateReq([
    { name: 'login', type: 'string', in: 'body', required: true },
    { name: 'password', type: 'string', in: 'body', required: true }
  ]),
  parseDb([
    { name: 'login', model: 'users', in: 'body', one: true }
  ]),
  async (req, res) => {
    const user = req.dbFields.login
    const password = md5(req.body.password)

    if (user && user.password == password) {
      const token = await jwtService.create(user.id)
      res.send(200, token)
    }
    else res.send(401)
  }
)

router.put('/',
  validateReq([
    { name: 'login', type: 'string', in: 'body', required: true },
    { name: 'password', type: 'string', in: 'body', required: true },
    { name: 'name', type: 'string', in: 'body', required: true },
    { name: 'email', type: 'email', in: 'body', required: true },
    { name: 'heroSlug', type: 'string', in: 'body', required: true },
  ]),
  parseDb([
    { name: 'login', model: 'users', in: 'body', one: true },
    { name: 'email', model: 'users', in: 'body', one: true }
  ]),
  async (req, res) => {
    try {
      const userByLogin = req.dbFields.login
      const userByEmail = req.dbFields.email
      const { name, login, email, password, heroSlug } = req.body

      if (userByLogin || userByEmail) return res.send(401)

      const query = connector.insert().into('users').setFields({
        'name'        : name,
        'login'       : login,
        'password'    : md5(password),
        'email'       : email
      }).toString()

      const result = await connector.end(query)

      const token = await jwtService.create(result.insertId)

      const $newTown = (next) => {
        connector.end(
          connector.insert().into('towns').setFields({
            'user_id'        : connector.select().field('id').from('users').where('login = ?', login),
            'mine_id'        : connector.select().field('id').from('mines').where('level = 0'),
            'barrack_id'     : connector.select().field('id').from('barracks').where('level = 0'),
            'type_towns_id'  : connector.select().field('id').from('type_towns').where('slug = ?', 'village')
          }).toString()
        ).then(result => next())
      }

      const $addCharacters = (next) => {
        charactersService.addCharactersToUser([heroSlug], result.insertId)
        .then(() => next())
        .catch(next)
      }

      async.parallel([ $newTown, $addCharacters ], async (errors, result) => {
        await connector.end(
          connector.insert().into('users_campains')
          .setFields({
            user_id: connector.select().field('id').from('users').where('login = ?', login),
            progress: 0,
            campain_id: connector.select().from('campains').field('id').where(
              'town_type_id = ?', connector.select().from('type_towns').field('id').where('slug = ?', 'village')
            )
          })
          .toString()
        )
        if (errors) {
          logger.error(errors)
          return res.send(500, errors)
        }
        res.send(200, token)
      })
    } catch (error) {
      console.log(error)
      logger.error(error)
      return res.send(500, error)
    }

  }
)

router.get('/heros',
  async (req, res) => {
    res.send(200, await connector.end(
      connector.select()
      .from('register_characters', 'rc')
      .left_join('character_levels', 'cl', 'cl.id = rc.character_level_id')
      .order('group_id')
      .toString()
    ))
  }
)

module.exports = router;
