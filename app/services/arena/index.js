const uuidv4 = require('uuid/v4');
const battleCoreService = require('../battleCore')
const connector = require('../../systems/connector')
const cache = require('../../config/cache')

const arena = new class Arena {
  constructor () {
    this.queues = {}
    this.genereteTop()
    setInterval(this.genereteTop, 21600000) //21600000000
  }

  addQueue (user) {
    const rateGroup = this.rateGroup(user)
    if (!this.queues[rateGroup]) this.queues[rateGroup] = {}
    this.queues[rateGroup][user.id] = user
  }

  searchOponent (rateGroup1, rateGroup2, i) {
    if (i > 5) return false
    if (rateGroup2 < 0) rateGroup2 = 0
    if (this.queues[rateGroup1] && Object.keys(this.queues[rateGroup1]).length > 0) {
      return rateGroup1
    } else if (this.queues[rateGroup2] && Object.keys(this.queues[rateGroup2]).length > 0) {
      return rateGroup2
    } else {
      rateGroup1++;
      rateGroup2--;
      i++;
      return this.searchOponent(rateGroup1, rateGroup2, i)
    }
  }

  checkOpponent (user) {
    let rateGroup = this.rateGroup(user)
    rateGroup = this.searchOponent(rateGroup, rateGroup, 0)
    if (rateGroup !== false){
      if (this.queues[rateGroup][user.id]) return;
      const opponentKey = Object.keys(this.queues[rateGroup])[0]
      const opponent = this.queues[rateGroup][opponentKey]
      delete this.queues[rateGroup][opponentKey]

      if (Math.floor(Math.random()*(100-0+1)+0) > 50)
        this.startBattle(opponent, user)
      else
        this.startBattle(user, opponent)

    } else this.addQueue(user)
  }

  rateGroup (user) {
    return user.rating ? Math.round(user.rating / 100) : 0
  }

  startBattle (user, opponent) {
    user.isUser = true
    user.type = 'user'
    opponent.isUser = true
    opponent.type = 'user'

    const roomId = uuidv4()
    global.Sockets[global.userSockets[user.login]].socket.join(roomId)
    global.Sockets[global.userSockets[opponent.login]].socket.join(roomId)

    battleCoreService.newBatte(user, opponent, roomId, 'arena')
  }

  stopSearch (user) {
    const rateGroup = this.rateGroup(user)
    if (!this.queues[rateGroup] || !Object.keys(this.queues[rateGroup])) return;
    delete this.queues[rateGroup][user.id]
  }

  async genereteTop () {
    const preLogs = await connector.end(
      connector.select()
      .from('arena_log','al')
      .field('uw.name', 'user_win_name').field('uw.id', 'user_win_id')
      .left_join('users', 'uw', 'uw.id = al.user_win_id')
      .toString()
    )
    const logs = {}

    preLogs.forEach(val => {
      if (!logs[val.user_win_id]) {
        logs[val.user_win_id] = {
          name: val.user_win_name,
          wins: 0
        }
      }
      logs[val.user_win_id].wins++
    })

    const sortable = []

    for (var log in logs) {
      sortable.push([log, logs[log]])
    }
    sortable.sort(function(a, b) {
        return b[1].wins - a[1].wins
    })
    cache.set('arenaTop', sortable)
  }
}

module.exports = arena
