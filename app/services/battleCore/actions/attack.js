const battleCharacters = require('../characters')
const connector = require('../../../systems/connector')

const randomIntFromInterval = (min,max) => Math.floor(Math.random()*(max-min+1)+min)

const getChance = (chance) => {
  return chance > randomIntFromInterval(0, 100)
}

/*
* 1 - Физическая атака
* 2 - Магическая Атака
* 3 - Чистая атака
*/

const getTypeCode = (attacker) => {
  switch (attacker.typeAttack) {
    case 'melee':
    case 'distance_rifle':
      return 1
    break;
    case 'distance_magic':
    case 'medication':
      return 2
    break;
  }
}

const calcAttack = (attacker, target) => {
  let resultAttack = attacker.attack
  let evasion = false
  let resultNull = false
  if (getChance(attacker.critical_hit_chance)) {
    resultAttack *= 2
  }
  const attackTypeCode = getTypeCode(attacker)


  if (attackTypeCode === 1) {
    resultAttack -= target.physical_armor
    if (getChance(target.evasion)) {
      evasion = true
      resultAttack = 0
    } else if (getChance(target.evasion / 2)) {
      resultAttack = resultAttack / 2
    }
    if (target.physical_armor >= attacker.attack * 2) resultNull = true
  } else {
    resultAttack -= target.magic_armor
    if (target.magic_armor * 2 > attacker.attack) resultNull = true
  }


  if (evasion || resultNull) return 0
  return ( resultAttack > 0) ? resultAttack : 1
}

const attack = async (data) => {
  const { attackerId, targetId } = data
  let [ attacker, target ] = await battleCharacters.getTwoCharacters(attackerId, targetId, true)

  const attackPoint = calcAttack(attacker, target)
  target.health -= attackPoint

  if (target.health < 1) {
    await connector.end(connector.delete().from('battle_characters').where('id = ?', target.id).toString())
  } else {
    await connector.end(connector.update().table('battle_characters').set('health', target.health).where('id = ?', target.id).toString())
  }

  return {
    targetDead: target.health < 1,
    attackPoint
  }
}

module.exports = {
  attack,
  calcAttack
}
