const connector = require('../../../app/systems/connector')


connector.end(
  connector.insert().into('register_characters')
  .setFieldsRows([
    // {
    //   character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'amunI')
    // },
    // {
    //   character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'sehitI')
    // },
    // {
    //   character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'lotanariI')
    // },
    {
      character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'hasanI')
    }
  ]).toString()
)
