module.exports = class Round {
  constructor () {
    this._characters = []
    this.roundsCount = 0
  }

  getNextAttacker () {
    return this._characters[0]
  }

  nextRound () {
    this._characters.push(this._characters[0])
    this.roundsCount++
    this._characters.shift()
  }

  getAll () {
    return this._characters
  }

  init (team1, team2) {
    const characters = shuffle([...team1, ...team2])
    const result = []


    characters.sort( (a, b) => {
      return b.initiative - a.initiative
    })


    characters.forEach((val) => {
      result.push({
        id        : val.id,
        img       : val.img,
        parentId  : val.parentId
      })

      if(val.number_actions > 1) {
        for (let index = 0; index < val.number_actions - 1; index++) {
          result.push({
            id        : val.id,
            img       : val.img,
            parentId  : val.parentId
          })
        }
      }
    })

    this._characters = result
  }

  deleteCharacter (id) {
    let newArray = []
    let result = {}
    this._characters.forEach(val => {
      if (val.id != id) newArray.push(val)
    })
    this._characters = newArray
    return newArray
  }

  checkIsTeamDead () {
    const teamWinId = this.getNextAttacker().parentId
    let result = true
    this._characters.forEach(val => {
        if (val.parentId !== teamWinId) result = false
    })

    return result ? teamWinId : false
  }

}

const shuffle = (array) => {
  var currentIndex = array.length, temporaryValue, randomIndex;

  while (0 !== currentIndex) {

    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}
