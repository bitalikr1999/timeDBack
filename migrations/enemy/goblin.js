const connector = require('../../app/systems/connector')


// connector.end(
//   connector.insert().into('character_levels')
//   .setFieldsRows([
//     {
//       img: '/character/goblin/thumbnail/small-goblin.jpg',
//       name: 'Маленький гоблин',
//       slug: 'goblinI',
//       attack: 1,
//       health: 3,
//       type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
//       evasion: 0,
//       full_img: '/character/goblin/small-goblin.jpg',
//       initiative: 1,
//       theft_life: 0,
//       inheriot_at: 0,
//       type_attack: 1,
//       magic_armor: 0,
//       physical_armor: 0,
//       number_actions: 1,
//       need_experience: 0,
//       critical_hit_chance: 0
//     },
//     {
//       img: '/character/goblin/thumbnail/goblin.jpg',
//       name: 'Слабый гоблин',
//       slug: 'goblinII',
//       attack: 1,
//       health: 4,
//       type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
//       evasion: 1,
//       full_img: '/character/goblin/goblin.jpg',
//       initiative: 2,
//       theft_life: 0,
//       inheriot_at: 'goblinI',
//       type_attack: 1,
//       magic_armor: 0,
//       physical_armor: 0,
//       number_actions: 1,
//       need_experience: 100,
//       critical_hit_chance: 0
//     },
//     {
//       img: '/character/goblin/thumbnail/goblin.jpg',
//       name: 'Гоблин',
//       slug: 'goblinIII',
//       attack: 2,
//       health: 4,
//       type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
//       evasion: 1,
//       full_img: '/character/goblin/goblin.jpg',
//       initiative: 2,
//       theft_life: 0,
//       inheriot_at: 'goblinII',
//       type_attack: 1,
//       magic_armor: 0,
//       physical_armor: 0,
//       number_actions: 1,
//       need_experience: 100,
//       critical_hit_chance: 0
//     }
//   ]).toString()
// )

connector.end(
  connector.insert().into('character_levels')
  .setFieldsRows([
    {
      img: '/character/goblin/thumbnail/goblin-shaman.jpg',
      name: 'Гоблин шаман',
      slug: 'goblinShamanI',
      attack: 2,
      health: 3,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'distance_magic'),
      evasion: 0,
      full_img: '/character/goblin/goblin-shaman.jpg',
      initiative: 3,
      theft_life: 0,
      inheriot_at: 0,
      type_attack: 1,
      magic_armor: 0,
      physical_armor: 0,
      number_actions: 1,
      need_experience: 0,
      critical_hit_chance: 0
    },
    {
      img: '/character/goblin/thumbnail/goblin-shaman.jpg',
      name: 'Гоблин заклинатель',
      slug: 'goblinShamanII',
      attack: 3,
      health: 3,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 0,
      full_img: '/character/goblin/goblin-shaman.jpg',
      initiative: 3,
      theft_life: 0,
      inheriot_at: 0,
      type_attack: 1,
      magic_armor: 0,
      physical_armor: 0,
      number_actions: 1,
      need_experience: 0,
      critical_hit_chance: 0
    },
  ]).toString()
)
