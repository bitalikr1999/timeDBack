const connector = require('../systems/connector')
const { jwtService } = require('../services')

const auth = async (socket, data) => {
  const token = data.token
  let decode
  try {
    decode = await jwtService.verify(token)
  } catch (error) {
    return socket.emit('auth-fail')
  }
  connector.end(
    connector.select().from('users').where('id = ?', decode.id).toString()
  ).then(users => {
    global.userSockets[users[0].login] = socket.id
    global.Sockets[socket.id] = { socket, user: users[0] }
    return socket.emit('auth-susseful', {id: socket.id})
  })
}

module.exports = (socket) => {
  socket.on('auth', (data) => {
    auth(socket, data)
  })
}
