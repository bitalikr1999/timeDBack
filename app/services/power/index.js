const prepareCharacter = (character) => {
  character = JSON.parse(JSON.stringify(character))
  for (let key in character) {
    if (character.hasOwnProperty(key)) {
      if (!character[key]) character[key] = 1
    }
  }
  return character
}

const calcPower = (character) => {
  let power = 0;
  character = prepareCharacter(character)
  character.pa = character.physical_armor
  character.ma = character.magic_armor
  power += character.attack * character.number_actions
  power += (character.health * (character.pa + character.ma))
  power += character.initiative 

  if (character.type_characters === 'distance_magic') power += character.attack
  if (character.theft_life > 1) power += (character.attack * character.theft_life ) / 100
  if (character.evasion > 1) power += (character.health * (character.evasion / 10) ) / character.evasion
  if (character.critical_hit_chance > 1) power += (character.attack * (character.critical_hit_chance / 10)) / character.critical_hit_chance

  return Math.round(power)

}

module.exports = {
  calcPower
}
