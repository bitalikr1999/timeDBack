const connector = require('../systems/connector')
const { battleCoreService } = require('../services')
const uuidv4 = require('uuid/v4');

const startBattle = async (socket, data) => {
  data = data ? data : {}
  const errors = []
  const { user } = global.Sockets[socket.id]
  const { oponentId, oponentType } = data
  const from = (oponentType == 'user') ? 'users' : 'enemy_district_teams'

  if (!user) errors.push('user')
  if (!oponentId) errors.push('oponentId')
  if (errors.length)
    return socket.emit('battle/new', {
      status: 'error',
      data: errors
    })

  const [ oponent ] = await connector.end(
    connector.select().from(from)
    .where('id = ?', oponentId)
    .toString()
  )

  if (oponentType != 'user') oponent.isUser = false
  oponent.type = oponentType
  user.isUser = true
  user.type = 'user'

  const roomId = uuidv4()
  socket.join(roomId)

  battleCoreService.newBatte(user, oponent, roomId, 'disctrict')

}

module.exports = (socket, io) => {
  socket.on('battle/new', (data) => {
    startBattle(socket, data)
  })
}
