const auth       = require('./auth')
const battle     = require('./battle')
const chats     = require('./chats')

global.userSockets = {}
global.Sockets     = {}

module.exports = (io) => {
  global.io = io

  io.on('connection', (socket) => {
    socket.emit('connection')
    global.socket = socket
    auth(socket)
    battle(socket)
    chats(socket)
  })

}
