const connector = require('../../../app/systems/connector')
const func = async () => {
  //
  // await connector.end(
  //   connector.insert().into('skills')
  //   .setFieldsRows([
  //     {
  //       slug: 'amun_comanderI',
  //       name: 'Амун предвотель',
  //       chance: 0,
  //       data: JSON.stringify({
  //         evasion: 10
  //       }),
  //       turns_count: 0,
  //       mana_cost: 0,
  //       group_id: connector.select().from('skills_groups').field('id').where('slug = ?', 'pas_magnification_rase'),
  //       skill_type_id: connector.select().from('skills_types').field('id').where('slug = ?', 'passive_single'),
  //     },
  //     {
  //       slug: 'amun_comanderII',
  //       name: 'Амун предвотель',
  //       chance: 0,
  //       data: JSON.stringify({
  //         evasion: 10
  //       }),
  //       turns_count: 0,
  //       mana_cost: 0,
  //       group_id: connector.select().from('skills_groups').field('id').where('slug = ?', 'pas_magnification_rase'),
  //       skill_type_id: connector.select().from('skills_types').field('id').where('slug = ?', 'passive_single'),
  //     },
  //   ]).toString()
  // )
  //
  await connector.end(
    connector.insert().into('skills')
    .setFieldsRows([
      {
        slug: 'poisonI',
        name: 'Отравленный удар',
        chance: 0,
        data: 1,
        turns_count: 2,
        mana_cost: 2,
        buff_id: connector.select().from('skills_buffs').field('id').where('slug = ?', 'poison'),
        group_id: connector.select().from('skills_groups').field('id').where('slug = ?', 'negative_effect'),
        skill_type_id: connector.select().from('skills_types').field('id').where('slug = ?', 'active'),
      },
    ]).toString()
  )
  //
  // await connector.end(
  //   connector.insert().into('skills')
  //   .setFieldsRows([
  //     {
  //       slug: 'stealth',
  //       name: 'Скрытность',
  //       chance: 0,
  //       data: JSON.stringify({
  //         evasion: 10,
  //         attack: 1
  //       }),
  //       turns_count: 3,
  //       mana_cost: 5,
  //       group_id: connector.select().from('skills_groups').field('id').where('slug = ?', 'magnification_yourself'),
  //       skill_type_id: connector.select().from('skills_types').field('id').where('slug = ?', 'active'),
  //     },
  //   ]).toString()
  // )

  // AMUN I
  // await connector.end(
  //   connector.insert().into('characters_levels_skills')
  //   .setFieldsRows([
  //     {
  //       skill_id: connector.select().from('skills').field('id').where('slug = ?', 'amun_comanderI'),
  //       character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'amunI'),
  //     },
  //   ]).toString()
  // )
  //
  // // AMUN II
  // await connector.end(
  //   connector.insert().into('characters_levels_skills')
  //   .setFieldsRows([
  //     {
  //       skill_id: connector.select().from('skills').field('id').where('slug = ?', 'amun_comanderI'),
  //       character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'amunII'),
  //     },
  //   ]).toString()
  // )
  //
  // await connector.end(
  //   connector.insert().into('characters_levels_skills')
  //   .setFieldsRows([
  //     {
  //       skill_id: connector.select().from('skills').field('id').where('slug = ?', 'poisonI'),
  //       character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'amunII'),
  //     },
  //   ]).toString()
  // )
  //
  // // AMUN III
  //
  // await connector.end(
  //   connector.insert().into('characters_levels_skills')
  //   .setFieldsRows([
  //     {
  //       skill_id: connector.select().from('skills').field('id').where('slug = ?', 'amun_comanderII'),
  //       character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'amunIII'),
  //     },
  //   ]).toString()
  // )
  //
  // await connector.end(
  //   connector.insert().into('characters_levels_skills')
  //   .setFieldsRows([
  //     {
  //       skill_id: connector.select().from('skills').field('id').where('slug = ?', 'poisonI'),
  //       character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'amunIII'),
  //     },
  //   ]).toString()
  // )
  //
  // // AMUN IV
  //
  // await connector.end(
  //   connector.insert().into('characters_levels_skills')
  //   .setFieldsRows([
  //     {
  //       skill_id: connector.select().from('skills').field('id').where('slug = ?', 'amun_comanderII'),
  //       character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'amunIV'),
  //     },
  //   ]).toString()
  // )
  //
  // await connector.end(
  //   connector.insert().into('characters_levels_skills')
  //   .setFieldsRows([
  //     {
  //       skill_id: connector.select().from('skills').field('id').where('slug = ?', 'poisonI'),
  //       character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'amunIV'),
  //     },
  //   ]).toString()
  // )
  //
  // // AMUN V
  //
  // await connector.end(
  //   connector.insert().into('characters_levels_skills')
  //   .setFieldsRows([
  //     {
  //       skill_id: connector.select().from('skills').field('id').where('slug = ?', 'amun_comanderII'),
  //       character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'amunV'),
  //     },
  //   ]).toString()
  // )
  //
  // await connector.end(
  //   connector.insert().into('characters_levels_skills')
  //   .setFieldsRows([
  //     {
  //       skill_id: connector.select().from('skills').field('id').where('slug = ?', 'poisonI'),
  //       character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'amunV'),
  //     },
  //   ]).toString()
  // )

  await connector.end(
    connector.insert().into('characters_levels_skills')
    .setFieldsRows([
      {
        skill_id: connector.select().from('skills').field('id').where('slug = ?', 'stealth'),
        character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'amunV'),
      },
    ]).toString()
  )
}

func()
