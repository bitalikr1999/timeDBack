const url = require('url')
const { jwtService } = require('../services')
const connector = require('../systems/connector')

module.exports = (getUser) => async (req, res, next) => {
  let token = req.headers.authorization
  if (!token) return res.send(401)
  token = token.split('Bearer ')[1]
  let decode
  try {
    decode = await jwtService.verify(token)
  } catch (error) {
    return res.send(401)
  }

  if (getUser) {
    connector.end(
      connector.select().from('users').where('id = ?', decode.id).toString()
    ).then(users => {
      req.user = users[0]
      next()
    })
  } else {
    req.user = decode
    next()
  }
}
