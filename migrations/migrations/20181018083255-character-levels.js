'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('character_levels', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true
      },
      name: 'string',
      slug: {
        type: 'string',
        unique: true,
        notNull: true
      },
      full_img: {
        type: 'string',
        notNull: true
      },
      img: {
        type: 'string',
        notNull: true
      },
      health: {
        type: 'int',
        notNull: true
      },
      attack: {
        type: 'int'
      },
      evasion: {
        type: 'int'
      },
      initiative: {
        type: 'int'
      },
      theft_life: {
        type: 'int'
      },
      physical_armor: {
        type: 'int'
      },
      magic_armor: {
        type: 'int'
      },
      critical_hit_chance: {
        type: 'int'
      },
      type_attack: {
        type: 'int'
      },
      number_actions: {
        type: 'int'
      },
      mana: {
        type: 'int'
      },
      group_id: {
        type: 'int',
        foreignKey: {
          name: 'group_character_levels_f',
          table: 'characters_groups',
          mapping: 'id',
          rules: {
            onDelete: 'RESTRICT',
            onUpdate: 'RESTRICT'
          }
        }
      },
      type_id: {
        type: 'int',
        foreignKey: {
          name: 'type_character_level_f',
          table: 'type_characters',
          mapping: 'id',
          rules: {
            onDelete: 'RESTRICT',
            onUpdate: 'RESTRICT'
          }
        }
      },
      need_experience: {
        type: 'int'
      },
      inheriot_at: {
        type: 'string'
      },
      created_at: {
        type: 'timestamp',
        notNull: true,
        defaultValue: 'CURRENT_TIMESTAMP'
      }
    },
    ifNotExists: true
  })
};

exports.down = function(db) {
  return db.dropTable('character_levels');
};

exports._meta = {
  "version": 1
};
