const router = require('express')();
const connector = require('../systems/connector')
const { validateToken, parseDb } = require('../middleware')
const getRandom = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1) ) + min;
}
router.get('/characters',
  validateToken(),
  async (req, res) => {
    const userId = req.user.id
    const nowDate = new Date().getTime()
    const [ town ] = await connector.end(
      connector.select()
      .from('towns', 't')
      .field('tt.id', 'typeTownId')
      .field('b.level', 'barrackLevel')
      .field('t.id')
      .left_join('barracks', 'b', 'b.id = t.barrack_id')
      .left_join('type_towns', 'tt', 'tt.id = t.type_towns_id')
      .where('t.user_id = ?', userId)
      .toString()
    )

    const [ interval ] = await connector.end(
      connector.select()
      .from('barrack_intervals')
      .where('town_id = ?', town.id)
      .where('user_id = ?', userId)
      .toString()
    )

    if (!interval || (new Date(interval.end).getTime() < nowDate)) {
      if (interval) {
        connector.end(connector.delete().from('barrack_intervals').where('id = ?', interval.id).toString())
      }
      const possible = await connector.end(
        connector.select().from('barrack_possible_character', 'bp')
        .field('bp.chance').field('bp.*')
        .where('bp.town_type_id = ?', town.typeTownId)
        .where('bp.barrack_level <= ?', town.barrackLevel)
        .toString()
      )

      const numberOf = getRandom(1, 3)
      const characters = []
      let isFull = false

      while (!isFull) {
        const char = possible[getRandom(0, possible.length - 1)]
        characters.push({
          character_level_id: char.character_level_id,
          price: char.price ,
          town_id: town.id
        })
        if (characters.length >= numberOf) isFull = true
      }

      await connector.end(
        connector.insert().into('barrack_available_character')
        .setFieldsRows(characters)
        .toString()
      )
      const endNewInterval = new Date(nowDate + getRandom(4, 12) * 60 * 60 * 1000)
      await connector.end(
        connector.insert()
        .into('barrack_intervals')
        .setFields({
          'end'     : endNewInterval,
          'town_id' : town.id,
          'user_id' : userId
        })
        .toString()
      )
    }

    const aviableCharacters = await connector.end(
      connector.select()
      .from('barrack_available_character', 'ac')
      .field('ac.*')
      .field('cl.img')
      .field('cl.name')
      .left_join('character_levels', 'cl', 'cl.id = ac.character_level_id')
      .where('ac.town_id = ?', town.id)
      .toString()
    )

    res.send(200, aviableCharacters)
  }
)

module.exports = router;
