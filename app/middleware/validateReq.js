const moment = require('moment');
const validator = require('validator');

module.exports = (validationConfig, valiadtionOptions) => async (req, res, next) => {
  const config = validationConfig || [];
  const options = valiadtionOptions || {};
  const validationErrors = req.validationErrors = {}
  for (const fieldConfig of config) {
    let fieldValue;
    switch (fieldConfig.in) {
      case ('query'):
        fieldValue = req.query[fieldConfig.name];
        break;
      case ('params'):
        fieldValue = req.params[fieldConfig.name];
        break;
      case ('body'):
      default:
        fieldValue = req.body[fieldConfig.name];
        break;
    }
    if (typeof fieldValue === 'string' && fieldConfig.type === 'number'
      && !Number.isNaN(parseFloat(fieldValue)) && (/^[\d.]+$/).test(fieldValue)) {
      fieldValue = parseFloat(fieldValue);
    }
    if (typeof fieldValue === 'string' && fieldConfig.type === 'boolean' && ['true', 'false'].includes(fieldValue)) {
      fieldValue = (fieldValue === 'true');
    }
    if (fieldValue !== undefined && typeof fieldValue === 'string' && fieldConfig.isArray) {
      fieldValue = [fieldValue];
    }
    if (
      fieldConfig.required && (
        (fieldConfig.isArray && (fieldValue === undefined || fieldValue === null || !fieldValue.length))
        || (!fieldConfig.isArray && (fieldValue === undefined || fieldValue === null))
      )
    ) {
      validationErrors[fieldConfig.name] = 'required'
    }
    if (fieldValue === undefined || fieldValue === null) {
      continue;
    }
    switch (fieldConfig.type) {
      case ('string'):
        if (typeof fieldValue !== 'string') {
          validationErrors[fieldConfig.name] = 'invalid'

        }
        break;
      case ('date'):
        if (!moment(fieldValue).isValid()) {
          validationErrors[fieldConfig.name] = 'invalid'
        }
        break;
      case ('number'):
        if (typeof fieldValue !== 'number') {
          validationErrors[fieldConfig.name] = 'invalid'
        }
        break;
      case ('boolean'):
        if (typeof fieldValue !== 'boolean') {
          validationErrors[fieldConfig.name] = 'invalid'
        }
        break;
      case ('file'):
        if (typeof fieldValue !== 'object') {
          validationErrors[fieldConfig.name] = 'invalid'
        }
        break;
      case ('email'):
        if (!validator.isEmail(fieldValue)) {
          validationErrors[fieldConfig.name] = 'invalid'
        }
        break;
      default:
          validationErrors[fieldConfig.name] = 'invalid'
        break;
    }
  }

  if (options.allowEmptyBody === false) {
    const bodyIsNotEmpty = config.filter(i => i.in === 'body').some(fieldConfig => ctx.request.fields[fieldConfig.name] !== undefined);
    if (!bodyIsNotEmpty) {
      validationErrors.push({
        message: 'empty request'
      });
    }
  }

  if (Object.keys(validationErrors).length) {
    res.send(400, {
      message: 'Invalid request',
      errors: validationErrors
    })
  } else next();
};
