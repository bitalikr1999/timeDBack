const connector = require('../../../app/systems/connector')


connector.end(
  connector.insert().into('character_levels')
  .setFieldsRows([
    {
      img: '/character/heroes/thumbnail/lotanari.jpg',
      name: 'Лотанари',
      slug: 'lotanariI',
      attack: 2,
      health: 10,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'distance_magic'),
      evasion: 0,
      full_img: '/character/heroes/lotanari.jpg',
      initiative: 1,
      theft_life: 0,
      inheriot_at: 0,
      type_attack: 1,
      magic_armor: 2,
      physical_armor: 0,
      number_actions: 1,
      need_experience: 0,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'elf'),
      mana: 5
    },

    {
      img: '/character/heroes/thumbnail/lotanari.jpg',
      name: 'Лотанари',
      slug: 'lotanariII',
      attack: 2,
      health: 15,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'distance_magic'),
      evasion: 0,
      full_img: '/character/heroes/lotanari.jpg',
      initiative: 1,
      theft_life: 0,
      inheriot_at: 'lotanariI',
      type_attack: 1,
      magic_armor: 2,
      physical_armor: 0,
      number_actions: 1,
      need_experience: 1000,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'elf'),
      mana: 5
    },

    {
      img: '/character/heroes/thumbnail/lotanari.jpg',
      name: 'Лотанари',
      slug: 'lotanariIII',
      attack: 2,
      health: 15,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'distance_magic'),
      evasion: 0,
      full_img: '/character/heroes/lotanari.jpg',
      initiative: 1,
      theft_life: 0,
      inheriot_at: 'lotanariII',
      type_attack: 1,
      magic_armor: 2,
      physical_armor: 0,
      number_actions: 1,
      need_experience: 1000,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'elf'),
      mana: 5
    },

    {
      img: '/character/heroes/thumbnail/lotanari.jpg',
      name: 'Лотанари',
      slug: 'lotanariIV',
      attack: 3,
      health: 15,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'distance_magic'),
      evasion: 0,
      full_img: '/character/heroes/lotanari.jpg',
      initiative: 1,
      theft_life: 0,
      inheriot_at: 'lotanariIII',
      type_attack: 1,
      magic_armor: 2,
      physical_armor: 0,
      number_actions: 1,
      need_experience: 1000,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'elf'),
      mana: 5
    },

    {
      img: '/character/heroes/thumbnail/lotanari.jpg',
      name: 'Лотанари',
      slug: 'lotanariV',
      attack: 3,
      health: 15,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'distance_magic'),
      evasion: 0,
      full_img: '/character/heroes/lotanari.jpg',
      initiative: 1,
      theft_life: 0,
      inheriot_at: 'lotanariIV',
      type_attack: 1,
      magic_armor: 2,
      physical_armor: 0,
      number_actions: 1,
      need_experience: 1000,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'elf'),
      mana: 8
    },

  ]).toString()
)
