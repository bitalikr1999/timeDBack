'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('barrack_available_character', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true
      },
      price:{
        type: 'int'
      },
      town_id: {
        type: 'int',
        foreignKey: {
          name: 'towns_avaliable_character_f',
          table: 'towns',
          mapping: 'id',
          rules: {
            onDelete: 'RESTRICT',
            onUpdate: 'RESTRICT'
          }
        }
      },
      character_level_id: {
        type: 'int',
        foreignKey: {
          name: 'character_avaliable_character_f',
          table: 'character_levels',
          mapping: 'id',
          rules: {
            onDelete: 'RESTRICT',
            onUpdate: 'RESTRICT'
          }
        }
      },
      created_at: {
        type: 'timestamp',
        notNull: true,
        defaultValue: 'CURRENT_TIMESTAMP'
      }
    },
    ifNotExists: true
  })
};

exports.down = function(db) {
  return db.dropTable('barrack_available_character');
};

exports._meta = {
  "version": 1
};
