const connector = require('../../../app/systems/connector')


connector.end(
  connector.insert().into('character_levels')
  .setFieldsRows([
    {
      img: '/character/heroes/thumbnail/hasan.jpg',
      name: 'Хасан',
      slug: 'hasanI',
      attack: 2,
      health: 10,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'distance_rifle'),
      evasion: 0,
      full_img: '/character/heroes/hasan.jpg',
      initiative: 4,
      theft_life: 0,
      inheriot_at: 0,
      type_attack: 1,
      magic_armor: 0,
      physical_armor: 0,
      number_actions: 2,
      need_experience: 0,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'people'),
      mana: 2
    },

    {
      img: '/character/heroes/thumbnail/hasan.jpg',
      name: 'Хасан',
      slug: 'hasanII',
      attack: 2,
      health: 10,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'distance_rifle'),
      evasion: 0,
      full_img: '/character/heroes/hasan.jpg',
      initiative: 5,
      theft_life: 0,
      inheriot_at: 'hasanI',
      type_attack: 1,
      magic_armor: 0,
      physical_armor: 0,
      number_actions: 2,
      need_experience: 1000,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'people'),
      mana: 3
    },

    {
      img: '/character/heroes/thumbnail/hasan.jpg',
      name: 'Хасан',
      slug: 'hasanIII',
      attack: 2,
      health: 15,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'distance_rifle'),
      evasion: 0,
      full_img: '/character/heroes/hasan.jpg',
      initiative: 5,
      theft_life: 0,
      inheriot_at: 'hasanII',
      type_attack: 1,
      magic_armor: 0,
      physical_armor: 0,
      number_actions: 2,
      need_experience: 1000,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'people'),
      mana: 3
    },

    {
      img: '/character/heroes/thumbnail/hasan.jpg',
      name: 'Хасан',
      slug: 'hasanIV',
      attack: 2,
      health: 15,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'distance_rifle'),
      evasion: 0,
      full_img: '/character/heroes/hasan.jpg',
      initiative: 6,
      theft_life: 0,
      inheriot_at: 'hasanIII',
      type_attack: 1,
      magic_armor: 0,
      physical_armor: 0,
      number_actions: 2,
      need_experience: 1000,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'people'),
      mana: 3
    },

    {
      img: '/character/heroes/thumbnail/hasan.jpg',
      name: 'Хасан',
      slug: 'hasanV',
      attack: 3,
      health: 15,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'distance_rifle'),
      evasion: 0,
      full_img: '/character/heroes/hasan.jpg',
      initiative: 6,
      theft_life: 0,
      inheriot_at: 'hasanIV',
      type_attack: 1,
      magic_armor: 0,
      physical_armor: 0,
      number_actions: 2,
      need_experience: 1000,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'people'),
      mana: 3
    }
  ]).toString()
)
