const connector = require('../../../systems/connector')
const { addItems, getItems } = require('../helpers')

const getPossibleItems = async (parentId) => {
  return await connector.end(
    connector.select()
    .from('items_chests_items')
    .where('item_parent_id = ?', parentId)
    .toString()
  )
}

const randomIntFromInterval = (min,max) => Math.floor(Math.random()*(max-min+1)+min)

const getChance = (chance) => {
  return chance > randomIntFromInterval(0, 100)
}

module.exports = async (item, userId) => {
  const possibleItems = await getPossibleItems(item.id)
  const itemsToSave = []
  possibleItems.forEach(val => {
    if (getChance(val.chance)) {
      itemsToSave.push({
        item_id: val.item_id,
        count: 1,
        user_id: userId
      })
    }
  })

  if (!itemsToSave.length)
    return []

  await addItems(itemsToSave, userId)

  return await getItems(itemsToSave)
}
