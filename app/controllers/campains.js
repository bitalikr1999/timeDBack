const router = require('express')();
const connector = require('../systems/connector')
const { validateToken, parseDb } = require('../middleware')
const { battleCoreService } = require('../services')
const uuidv4 = require('uuid/v4');

router.get('/',
  validateToken(true),
  parseDb([
    { name: 'token', model: 'towns', one: true, forResult: 'town', dbField: 'user_id' }
  ]),
  async (req, res) => {
    const user = req.user
    const { town } = req.dbFields

    const campainLevels = await connector.end(
      connector.select()
      .from('campains_levels', 'cl')
      .field('cl.*')
      .where('cl.campain_id = ?', connector.select().from('users_campains').field('campain_id').where('user_id = ?', user.id))
      .order('cl.id')
      .toString()
    )

    const userCampainsLevel = await connector.end(
      connector.select()
      .from('users_campains_levels')
      .where('user_campain_id = ?', connector.select().from('users_campains').field('id').where('user_id = ?', user.id))
      .toString()
    )
    const userCampainsLevelN = {}
    userCampainsLevel.forEach(val => {
      userCampainsLevelN[val.campain_level_id] = val
    })

    campainLevels.forEach( (val, i) => {
      if (!userCampainsLevelN[val.id]) return;
      campainLevels[i].is_complete_1 = userCampainsLevelN[val.id].is_complete_1
      campainLevels[i].is_complete_2 = userCampainsLevelN[val.id].is_complete_2
      campainLevels[i].is_complete_3 = userCampainsLevelN[val.id].is_complete_3
    })
    await Promise.all(
      campainLevels.map(async (val, i, arr) => {
        arr[i].rewardItems = await connector.end(
          connector.select()
          .from('campains_levels_rewards','clr')
          .field('i.*')
          .field('it.name', 'type_name')
          .field('it.slug', 'type_slug')
          .left_join('items', 'i', 'i.id = clr.item_id')
          .left_join('items_types', 'it', 'it.id = i.type_id')
          .where('clr.campain_level_id = ?', val.id)
          .toString()
        )
      })
    )

    res.send(200, campainLevels)
  }
)

router.put('/',
  validateToken(true),
  async (req, res) => {
    const { levelId } = req.body
    const user = req.user
    let newUserLevel = null

    console.log(  connector.select()
      .from('users_campains_levels')
      .where('campain_level_id = ?', levelId)
      .where('user_campain_id = ?', connector.select().from('users_campains').field('id').where('user_id = ?', user.id))
      .toString())
    const [ userLevel ] = await connector.end(
      connector.select()
      .from('users_campains_levels')
      .where('campain_level_id = ?', levelId)
      .where('user_campain_id = ?', connector.select().from('users_campains').field('id').where('user_id = ?', user.id))
      .toString()
    )
    
    if (userLevel && userLevel.is_complete_3) res.send(400)

    if (!userLevel) {
      newUserLevel = await connector.end(
        connector.insert().into('users_campains_levels')
        .setFields({
          campain_level_id: levelId,
          is_complete_1: false,
          is_complete_2: false,
          is_complete_3: false,
          user_campain_id: connector.select().from('users_campains').field('id').where('user_id = ?', user.id)
        })
        .toString()
      )
    }

    const [ oponent ] = await connector.end(
      connector.select().from('campains_levels')
      .where('id = ?', levelId)
      .toString()
    )

    oponent.isUser = false
    oponent.type = 'campain'
    user.isUser = true
    user.type = 'user'

    const roomId = uuidv4()
    global.Sockets[global.userSockets[user.login]].socket.join(roomId)

    battleCoreService.newBatte(user, oponent, roomId, 'campain')
    res.send(200)

  }
)

module.exports = router;
