const connector = require('../../app/systems/connector')


connector.end(
  connector.insert().into('items')
  .setFieldsRows([
    {
      slug: 'i-exp-scroll-50',
      name: 'I Свиток опыта',
      discription: 'В этом спитке заключен опыт других солдат, можно передать его другому воину. В свитке заключенно 50 единиц опыта',
      img: 'items-exp-scroll-1',
      type_id: connector.select().from('items_types').field('id').where('slug = ?', 'exp-scroll'),
      data: 50
    },
    {
      slug: 'i-exp-scroll-100',
      name: 'II Свиток опыта',
      discription: 'В этом спитке заключен опыт других солдат, можно передать его другому воину. В свитке заключенно 50 единиц опыта',
      img: 'items-exp-scroll-2',
      type_id: connector.select().from('items_types').field('id').where('slug = ?', 'exp-scroll'),
      data: 100
    },
  ]).toString()
)
