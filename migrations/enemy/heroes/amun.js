const connector = require('../../../app/systems/connector')


connector.end(
  connector.insert().into('character_levels')
  .setFieldsRows([
    {
      img: '/character/heroes/thumbnail/amun.jpg',
      name: 'Амун',
      slug: 'amunI',
      attack: 3,
      health: 15,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 5,
      full_img: '/character/heroes/amun.jpg',
      initiative: 1,
      theft_life: 0,
      inheriot_at: 0,
      type_attack: 1,
      magic_armor: 0,
      physical_armor: 0,
      number_actions: 1,
      need_experience: 0,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'people'),
      mana: 3
    },

    {
      img: '/character/heroes/thumbnail/amun.jpg',
      name: 'Амун',
      slug: 'amunII',
      attack: 3,
      health: 15,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 10,
      full_img: '/character/heroes/amun.jpg',
      initiative: 1,
      theft_life: 0,
      inheriot_at: 'amunI',
      type_attack: 1,
      magic_armor: 1,
      physical_armor: 1,
      number_actions: 1,
      need_experience: 1000,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'people'),
      mana: 4
    },

    {
      img: '/character/heroes/thumbnail/amun.jpg',
      name: 'Амун',
      slug: 'amunIII',
      attack: 3,
      health: 15,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 10,
      full_img: '/character/heroes/amun.jpg',
      initiative: 1,
      theft_life: 0,
      inheriot_at: 'amunII',
      type_attack: 1,
      magic_armor: 1,
      physical_armor: 1,
      number_actions: 1,
      need_experience: 1000,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'people'),
      mana: 5
    },

    {
      img: '/character/heroes/thumbnail/amun.jpg',
      name: 'Амун',
      slug: 'amunIV',
      attack: 3,
      health: 20,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 10,
      full_img: '/character/heroes/amun.jpg',
      initiative: 1,
      theft_life: 0,
      inheriot_at: 'amunIII',
      type_attack: 1,
      magic_armor: 1,
      physical_armor: 1,
      number_actions: 1,
      need_experience: 1000,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'people'),
      mana: 5
    },

    {
      img: '/character/heroes/thumbnail/amun.jpg',
      name: 'Амун',
      slug: 'amunV',
      attack: 3,
      health: 25,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 15,
      full_img: '/character/heroes/amun.jpg',
      initiative: 1,
      theft_life: 0,
      inheriot_at: 'amunIV',
      type_attack: 1,
      magic_armor: 1,
      physical_armor: 1,
      number_actions: 1,
      need_experience: 1000,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'people'),
      mana: 6
    }
  ]).toString()
)
