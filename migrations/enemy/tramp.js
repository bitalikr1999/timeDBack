const connector = require('../../app/systems/connector')


connector.end(
  connector.insert().into('character_levels')
  .setFieldsRows([
    {
      img: '/character/tramp/thumbnail/1.jpg',
      name: 'Бродяга',
      slug: 'tramp',
      attack: 1,
      health: 4,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 0,
      full_img: '/character/tramp/1.jpg',
      initiative: 1,
      theft_life: 0,
      inheriot_at: 0,
      type_attack: 1,
      magic_armor: 0,
      physical_armor: 0,
      number_actions: 1,
      need_experience: 0,
      critical_hit_chance: 0
    },
    {
      img: '/character/tramp/thumbnail/1.jpg',
      name: 'Бродяга I',
      slug: 'tramp2',
      attack: 1,
      health: 5,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 1,
      full_img: '/character/tramp/1.jpg',
      initiative: 1,
      theft_life: 0,
      inheriot_at: 'tramp',
      type_attack: 1,
      magic_armor: 0,
      physical_armor: 0,
      number_actions: 1,
      need_experience: 100,
      critical_hit_chance: 0
    },
    {
      img: '/character/tramp/thumbnail/1.jpg',
      name: 'Бродяга II',
      slug: 'tramp3',
      attack: 2,
      health: 6,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 0,
      full_img: '/character/tramp/1.jpg',
      initiative: 1,
      theft_life: 0,
      inheriot_at: 'tramp2',
      type_attack: 1,
      magic_armor: 0,
      physical_armor: 1,
      number_actions: 1,
      need_experience: 300,
      critical_hit_chance: 0
    },
    {
      img: '/character/tramp/thumbnail/thief.jpg',
      name: 'Исскустный вор',
      slug: 'thief',
      attack: 3,
      health: 6,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 5,
      full_img: '/character/tramp/thief.jpg',
      initiative: 1,
      theft_life: 0,
      inheriot_at: 'tramp3',
      type_attack: 1,
      magic_armor: 0,
      physical_armor: 1,
      number_actions: 1,
      need_experience: 300,
      critical_hit_chance: 0
    }
  ]).toString()
)
