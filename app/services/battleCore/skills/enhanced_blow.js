const { calcAttack } = require('../actions/attack')
const connector = require('../../../systems/connector')

module.exports = async (skill, attacker, round, target) => {

  const data = JSON.parse(skill.data)

  Object.keys(data).forEach(key => {
    attacker[key] += data[key]
  })

  const attackPoint = calcAttack(attacker, target)

  target.health -= attackPoint

  if (target.health < 1) {
    await connector.end(connector.delete().from('battle_characters').where('id = ?', target.id).toString())
  } else {
    await connector.end(connector.update().table('battle_characters').set('health', target.health).where('id = ?', target.id).toString())
  }

  round.nextRound()

  return {
    targetDead: target.health < 1,
    targetId: target.id,
    attackPoint
  }


}
