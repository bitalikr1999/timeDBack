const connector = require('../../../app/systems/connector')
const func = async () => {

  await connector.end(
    connector.insert().into('items')
    .setFieldsRows([
      {
        slug: 'i-chest_old',
        name: 'Старый сундук',
        discription: 'Открой что бы узнать что внутри!',
        img: 'items-chest1',
        type_id: connector.select().from('items_types').field('id').where('slug = ?', 'chest'),
      },
    ]).toString()
  )


  await connector.end(
    connector.insert().into('items_chests_items')
    .setFieldsRows([
      {
        chance: 50,
        item_parent_id: connector.select().from('items').field('id').where('slug = ?', 'i-chest_old'),
        item_id: connector.select().from('items').field('id').where('slug = ?', 'i-char-warriorI')
      },
      {
        chance: 25,
        item_parent_id: connector.select().from('items').field('id').where('slug = ?', 'i-chest_old'),
        item_id: connector.select().from('items').field('id').where('slug = ?', 'i-crystal-red-1')
      },
      {
        chance: 15,
        item_parent_id: connector.select().from('items').field('id').where('slug = ?', 'i-chest_old'),
        item_id: connector.select().from('items').field('id').where('slug = ?', 'i-exp-scroll-50')
      },
      {
        chance: 5,
        item_parent_id: connector.select().from('items').field('id').where('slug = ?', 'i-chest_old'),
        item_id: connector.select().from('items').field('id').where('slug = ?', 'i-exp-scroll-100')
      },
    ]).toString()
  )

}

func()
