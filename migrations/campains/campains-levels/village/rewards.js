const connector = require('../../../../app/systems/connector')


connector.end(
  connector.insert().into('campains_levels_rewards')
  .setFieldsRows([
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_1'),
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-playoni-shard-1'),
    },
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_1'),
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-chest_old'),
    },
  ]).toString()
)

connector.end(
  connector.insert().into('campains_levels_rewards')
  .setFieldsRows([
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_2'),
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-playoni-shard-1'),
      count: 2
    },
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_2'),
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-exp-scroll-50'),
      count: 1
    },
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_2'),
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-chest_old'),
      count: 1
    },
  ]).toString()
)

connector.end(
  connector.insert().into('campains_levels_rewards')
  .setFieldsRows([
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_3'),
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-playoni-shard-1'),
      count: 2
    },
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_3'),
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-exp-scroll-50'),
      count: 1
    },
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_3'),
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-chest_old'),
      count: 1
    },
  ]).toString()
)

connector.end(
  connector.insert().into('campains_levels_rewards')
  .setFieldsRows([
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_4'),
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-exp-scroll-50'),
      count: 2
    }
  ]).toString()
)

connector.end(
  connector.insert().into('campains_levels_rewards')
  .setFieldsRows([
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_5'),
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-playoni-shard-1'),
      count: 5
    },
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_5'),
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-exp-scroll-50'),
      count: 1
    },
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_5'),
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-char-warriorI'),
      count: 1
    },
  ]).toString()
)
