const connector = require('../../../systems/connector')

module.exports = async (item, userId) => {

  await connector.end(
    connector.update()
    .table('towns')
    .set('type_towns_id', (connector.select().from('type_towns').field('id').where('slug = ?', item.data)))
    .where('user_id = ?', userId)
    .toString()
  )

  return true
}
