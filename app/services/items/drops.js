const randomIntFromInterval = (min,max) => Math.floor(Math.random()*(max-min+1)+min)

const getChance = (chance) => {
  return chance > randomIntFromInterval(0, 100)
}

module.exports = (items) => {
  const result = []

  items.forEach(val => {
    if (val.max_count < 2) {

      if (getChance(val.chance)) result.push({
        item_id: val.item_id,
        count: 1
      })

    } else {

      let count = 0

      for (var i = 0; i < val.max_count; i++) {

        if (getChance(val.chance)) count++
      }

      if (count > 0) result.push({
        item_id: val.item_id,
        count
      })
    }
  })

  return result
}
