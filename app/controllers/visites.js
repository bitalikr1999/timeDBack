const router = require('express')();
const connector = require('../systems/connector')
const { validateToken } = require('../middleware')

router.get('/',
  async (req, res) => {
    const visitesRewards = await connector.end(
      connector.select()
      .from('visits_rewards')
      .order('day')
      .toString()
    )

    res.json(visitesRewards)
  }
)

module.exports = router;
