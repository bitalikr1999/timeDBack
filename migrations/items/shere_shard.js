const connector = require('../../app/systems/connector')


connector.end(
  connector.insert().into('items')
  .setFieldsRows([
    {
      slug: 'i-shere_shard_eather',
      name: 'Осколок земли',
      discription: 'В этом осколке заключенна сила стихи земли',
      img: 'items-shere_shard_eather',
      type_id: connector.select().from('items_types').field('id').where('slug = ?', 'sphere_shard')
    },
    {
      slug: 'i-shere_shard_wather',
      name: 'Осколок воды',
      discription: 'В этом осколке заключенна сила стихи воды',
      img: 'items-shere_shard_wather',
      type_id: connector.select().from('items_types').field('id').where('slug = ?', 'sphere_shard')
    },
    {
      slug: 'i-shere_shard_light',
      name: 'Осколок света',
      discription: 'В этом осколке заключенный небесный свет',
      img: 'items-shere_shard_light',
      type_id: connector.select().from('items_types').field('id').where('slug = ?', 'sphere_shard')
    },
    {
      slug: 'i-shere_shard_dark',
      name: 'Осколок темноты',
      discription: 'В этом осколке заключенна сама темнота',
      img: 'items-shere_shard_dark',
      type_id: connector.select().from('items_types').field('id').where('slug = ?', 'sphere_shard')
    },
    {
      slug: 'i-shere_shard_fire',
      name: 'Осколок огня',
      discription: 'В этом осколке заключенна сила стихи огня',
      img: 'items-shere_shard_fire',
      type_id: connector.select().from('items_types').field('id').where('slug = ?', 'sphere_shard')
    }
  ]).toString()
)
