const connector = require('../systems/connector')
const models = [
  'users',
  'towns',
  'character_levels',
  'characters'
]

module.exports = (parseFields) => async (req, res, next) => {
  req.dbFields = {}
  const errors = {}

  for (const field of parseFields) {
    let model = field.model
    let name
    let iN
    let value
    let forResult = field.forResult ? field.forResult : field.name
    let dbField = field.dbField ? field.dbField : field.name

    if (field.name == 'token') {
      name  = 'user_id',
      value = req.user.id
    } else {
      name  = field.name
      iN    = field.in
      value = req[iN][name]
    }

    if (models.includes(model)) {
      const query = connector.select().from(model).where(dbField + ' = ?', value).toString()
      console.log(query)
      try {
        req.dbFields[forResult] = await connector.end(query)
        if (field.one && req.dbFields[forResult].length) req.dbFields[forResult] = req.dbFields[forResult][0]
        else if (field.one) req.dbFields[forResult] = false

      } catch (error) {
        req.dbFields[forResult] = undefined
      }
    } else errors.model = 'invalid'
  }

  if (errors.length) res.send(400, errors)
  else next()
}
