const router = require('express')();
const connector = require('../systems/connector')
const { validateToken } = require('../middleware')

router.get('/',
  validateToken(true),
  async (req, res) => {
    const user = req.user
    res.send(200, user)
  }
)

module.exports = router;
