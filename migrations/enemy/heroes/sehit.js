const connector = require('../../../app/systems/connector')


connector.end(
  connector.insert().into('character_levels')
  .setFieldsRows([
    {
      img: '/character/heroes/thumbnail/sehit.jpg',
      name: 'Сехит',
      slug: 'sehitI',
      attack: 3,
      health: 10,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 0,
      full_img: '/character/heroes/sehit.jpg',
      initiative: 1,
      theft_life: 0,
      inheriot_at: 0,
      type_attack: 1,
      magic_armor: 1,
      physical_armor: 1,
      number_actions: 1,
      need_experience: 0,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'people'),
      mana: 3
    },

    {
      img: '/character/heroes/thumbnail/sehit.jpg',
      name: 'Сехит',
      slug: 'sehitII',
      attack: 3,
      health: 10,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 0,
      full_img: '/character/heroes/sehit.jpg',
      initiative: 3,
      theft_life: 0,
      inheriot_at: 'sehitI',
      type_attack: 1,
      magic_armor: 1,
      physical_armor: 1,
      number_actions: 1,
      need_experience: 1000,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'people'),
      mana: 3
    },

    {
      img: '/character/heroes/thumbnail/sehit.jpg',
      name: 'Сехит',
      slug: 'sehitIII',
      attack: 3,
      health: 15,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 0,
      full_img: '/character/heroes/sehit.jpg',
      initiative: 3,
      theft_life: 0,
      inheriot_at: 'sehitII',
      type_attack: 1,
      magic_armor: 1,
      physical_armor: 1,
      number_actions: 1,
      need_experience: 1000,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'people'),
      mana: 5
    },

    {
      img: '/character/heroes/thumbnail/sehit.jpg',
      name: 'Сехит',
      slug: 'sehitIV',
      attack: 3,
      health: 17,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 0,
      full_img: '/character/heroes/sehit.jpg',
      initiative: 3,
      theft_life: 0,
      inheriot_at: 'sehitIII',
      type_attack: 1,
      magic_armor: 1,
      physical_armor: 2,
      number_actions: 1,
      need_experience: 1000,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'people'),
      mana: 5
    },

    {
      img: '/character/heroes/thumbnail/sehit.jpg',
      name: 'Сехит',
      slug: 'sehitV',
      attack: 5,
      health: 20,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 0,
      full_img: '/character/heroes/sehit.jpg',
      initiative: 3,
      theft_life: 0,
      inheriot_at: 'sehitIV',
      type_attack: 1,
      magic_armor: 1,
      physical_armor: 2,
      number_actions: 1,
      need_experience: 1000,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'people'),
      mana: 5
    }
  ]).toString()
)
