const router = require('express')();
const connector = require('../systems/connector')
const { validateToken, validateReq } = require('../middleware')

router.get('/',
  validateToken(true),
  async (req, res) => {
    const team = await connector.end(
      connector.select()
      .from('characters', 'c')
      .field('c.id, c.position, c.experience')
      .field('cl.name, cl.img, cl.full_img')
      .left_join('character_levels', 'cl', 'cl.id = c.character_level_id')
      .where('user_id = ?', req.user.id)
      .toString()
    )
    res.send(200, team)
  }
)

router.patch('/move-character',
  validateToken(),
  validateReq([
    { name: 'toMove', in: 'body', type: 'number' },
    { name: 'characterId', in: 'body', type: 'number' }
  ]),
  async (req, res) => {
    let { toMove, characterId } = req.body

    position = toMove ? Number(toMove) : null

    if (position < 1 && position > 10) {}
    else {
      const [ isOccupied ] = await connector.end(
        connector.select()
        .from('characters')
        .where('user_id = ?', req.user.id)
        .where('position = ?', position)
        .toString()
      )

      if (isOccupied) {
        res.send(400, {position: 'occupied'})
        return
      }
    }

    await connector.end(
      connector.update()
      .table('characters')
      .set('position', position)
      .where('id = ?', characterId)
      .toString()
    )

    res.send(200)
  }
)

module.exports = router;
