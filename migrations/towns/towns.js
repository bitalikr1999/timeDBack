const connector = require('../../app/systems/connector')


connector.end(
  connector.insert().into('type_towns')
  .setFieldsRows([
    // {
    //   slug: 'old_fortess',
    //   name: 'Старая крепость',
    //   discription: '',
    //   image: '/town/old_fortess.jpg',
    //   rarity: 'Обычный',
    //   chance_find: 0,
    //   gold_bust: 10
    // },
    // {
    //   slug: 'elf-city',
    //   name: 'Ельфийский поселок',
    //   discription: '',
    //   image: '/town/elf-city.jpg',
    //   rarity: 'Обычный',
    //   chance_find: 0,
    //   gold_bust: 10
    // },
    {
      slug: 'orc-settlement',
      name: 'Поселение орков',
      discription: '',
      image: '/town/orc-settlement.jpg',
      rarity: 'Обычный',
      chance_find: 0,
      gold_bust: 10
    },
  ]).toString()
)
