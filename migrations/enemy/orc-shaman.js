const connector = require('../../app/systems/connector')


connector.end(
  connector.insert().into('character_levels')
  .setFieldsRows([
    // {
    //   img: '/character/orc-shaman/thumbnail/orc-shaman.jpg',
    //   name: 'Орк-шаман I',
    //   slug: 'orc-shamanI',
    //   attack: 2,
    //   health: 10,
    //   type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'distance_magic'),
    //   evasion: 0,
    //   full_img: '/character/orc-shaman/orc-shaman.jpg',
    //   initiative: 4,
    //   theft_life: 0,
    //   inheriot_at: 0,
    //   type_attack: 2,
    //   magic_armor: 0,
    //   physical_armor: 0,
    //   number_actions: 1,
    //   need_experience: 0,
    //   critical_hit_chance: 0,
    //   group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'orc')
    // },
    //
    // {
    //   img: '/character/orc-shaman/thumbnail/orc-shaman.jpg',
    //   name: 'Орк-шаман II',
    //   slug: 'orc-shamanII',
    //   attack: 3,
    //   health: 10,
    //   type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'distance_magic'),
    //   evasion: 0,
    //   full_img: '/character/orc-shaman/orc-shaman.jpg',
    //   initiative: 4,
    //   theft_life: 0,
    //   inheriot_at: 'orc-shamanI',
    //   type_attack: 2,
    //   magic_armor: 0,
    //   physical_armor: 0,
    //   number_actions: 1,
    //   need_experience: 200,
    //   critical_hit_chance: 0,
    //   group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'orc')
    // },
    //
    // {
    //   img: '/character/orc-shaman/thumbnail/orc-shaman.jpg',
    //   name: 'Орк-шаман III',
    //   slug: 'orc-shamanIII',
    //   attack: 4,
    //   health: 10,
    //   type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'distance_magic'),
    //   evasion: 0,
    //   full_img: '/character/orc-shaman/orc-shaman.jpg',
    //   initiative: 4,
    //   theft_life: 0,
    //   inheriot_at: 'orc-shamanII',
    //   type_attack: 2,
    //   magic_armor: 0,
    //   physical_armor: 0,
    //   number_actions: 1,
    //   need_experience: 500,
    //   critical_hit_chance: 0,
    //   group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'orc')
    // },
    //
    // {
    //   img: '/character/orc-shaman/thumbnail/orc-shamanIII.jpg',
    //   name: 'Орк-врачеватель',
    //   slug: 'orc-healer',
    //   attack: 3,
    //   health: 15,
    //   type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'distance_magic'),
    //   evasion: 0,
    //   full_img: '/character/orc-shaman/orc-shamanIII.jpg',
    //   initiative: 1,
    //   theft_life: 0,
    //   inheriot_at: 'orc-shamanIII',
    //   type_attack: 2,
    //   magic_armor: 0,
    //   physical_armor: 0,
    //   number_actions: 1,
    //   need_experience: 1000,
    //   critical_hit_chance: 0,
    //   group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'orc')
    // },

    {
      img: '/character/orc-shaman/thumbnail/orc-shamanII.jpg',
      name: 'Орк-призыватель',
      slug: 'orc-summoner',
      attack: 5,
      health: 10,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'distance_magic'),
      evasion: 0,
      full_img: '/character/orc-shaman/orc-shamanII.jpg',
      initiative: 4,
      theft_life: 2,
      inheriot_at: 'orc-shamanIII',
      type_attack: 2,
      magic_armor: 0,
      physical_armor: 0,
      number_actions: 1,
      need_experience: 500,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'orc')
    },
  ]).toString()
)
