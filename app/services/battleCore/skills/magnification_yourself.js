const { calcAttack } = require('../actions/attack')
const connector = require('../../../systems/connector')
const { addBuff } = require('../characters')

module.exports = async (skill, attacker) => {

  const data = JSON.parse(skill.data)

  await addBuff(attacker.id, skill.skill_buff_id, data)

  return {
    isNotAttack: true,
    nextRound: false,
    targetId: attacker.id
  }


}
