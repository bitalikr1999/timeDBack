const { calcPower } = require('../../power')
const battleCharacters = require('../characters')
const charactersService = require('../../characters')
const userService = require('../../users')

module.exports = async (teamWin, teamLoss) => {
  teamWinReal = await battleCharacters.getTeam(teamWin.id)
  teamWin = await charactersService.selectTeam(teamWin.id, teamWin.type)
  teamLoss = await charactersService.selectTeam(teamLoss.id, teamLoss.type)

  let teamWinPower  = 0;
  let teamLossPower = 0;
  let gold = 0;
  let expirienceOne = 0;

  teamWin.characters.forEach(val => {
    teamWinPower += calcPower(val)
  })
  teamLoss.characters.forEach(val => {
    teamLossPower += calcPower(val)
  })

  gold = teamLossPower
  if(teamLossPower > teamWinPower) {
    gold += teamLossPower - teamWinPower
  } else {
    gold -= teamWinPower - (teamLossPower / 2)
    if (gold < 10) gold = 10
  }

  let factor = teamLossPower / teamWinPower
  let expirience = 100 * factor
  let min = teamWinReal.length * 3
  if (expirience < min) expirience = min

  expirienceOne = Math.round(100 / teamWinReal.length)

  const charactersIds = teamWinReal.map(val => {
    return val.parent_id
  })
  await charactersService.addExp(expirienceOne, charactersIds)
  await userService.addGold(gold, teamWin.id)

  return {
    gold,
    exp: expirienceOne
  }

}
