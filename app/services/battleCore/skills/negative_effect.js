const connector = require('../../../systems/connector')
const { addBuff } = require('../characters')

module.exports = async (skill, attacker, round, target) => {

  await addBuff(target.id, skill.skill_buff_id, skill.data)

  return {
    isNotAttack: true,
    nextRound: false,
    targetId: target.id
  }
}
