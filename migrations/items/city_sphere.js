const connector = require('../../app/systems/connector')


connector.end(
  connector.insert().into('items')
  .setFieldsRows([
    {
      slug: 'i-city-old-fortess',
      name: 'Сфера "Старая крепость"',
      discription: 'В сфере заключенный город "Старая крепость". <br> Раса - <span class="fiol">Люди</span>',
      img: 'items-old-fortess',
      type_id: connector.select().from('items_types').field('id').where('slug = ?', 'city_sphere'),
      data: 'old_fortess'
    },
    {
      slug: 'i-city-elf-city',
      name: 'Сфера "Ельфийский город"',
      discription: 'В сфере заключенный город "Ельфийский город". <br> Раса - <span class="fiol">Ельфы</span>',
      img: 'items-elf-city',
      type_id: connector.select().from('items_types').field('id').where('slug = ?', 'city_sphere'),
      data: 'elf-city'
    },
    {
      slug: 'i-city-orc-settlement',
      name: 'Сфера "Поселение орков"',
      discription: 'В сфере заключенный город "Поселение орков". <br> Раса - <span class="fiol">Орки</span>',
      img: 'items-orc-settlement',
      type_id: connector.select().from('items_types').field('id').where('slug = ?', 'city_sphere'),
      data: 'orc-settlement'
    }
  ]).toString()
)
