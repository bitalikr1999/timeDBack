const battleCharacters = require('../characters')
const connector = require('../../../systems/connector')
const skillsService = require('../../skills/helpers')
const skillActions = require('../skills')
const buffsNeed = ['poison']

const checkActiveBuffs = async (characterId, round) => {
  const characterBuffs = await battleCharacters.getBuffs(characterId)
  if (!characterBuffs || !characterBuffs.length) return false;
  const filteredBuffs = []
  const [ character ] = await battleCharacters.getTwoCharacters(characterId)
  const emitedResult = []
  let attackPoint = 0

  characterBuffs.forEach(val => {
    if (buffsNeed.includes(val.slug)) filteredBuffs.push(val)
  })

  if (!filteredBuffs.length) return false;

  filteredBuffs.forEach(val => {
    let data = val.data
    try { data = JSON.parse(data) }catch(e){}

    switch (val.slug) {
      case 'poison':
        emitedResult.push({
          attackPoint: data,
          buffName: val.name
        })
        character.health -= data
      break;

      default:;
    }
  })

  if (character.health < 1) {
    await connector.end(connector.delete().from('battle_characters').where('id = ?', character.id).toString())
  } else {
    await connector.end(connector.update().table('battle_characters').set('health', character.health).where('id = ?', character.id).toString())
  }

  return {
    targetDead: character.health < 1,
    deadId: characterId,
    emitedResult,
    characterTeamId: character.owner_id
  }
}

module.exports = {
  checkActiveBuffs
}
