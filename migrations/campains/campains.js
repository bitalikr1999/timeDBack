const connector = require('../../app/systems/connector')


connector.end(
  connector.insert().into('campains')
  .setFieldsRows([
    {
      town_type_id: connector.select().from('type_towns').field('id').where('slug = ?', 'village')
    },
    {
      town_type_id: connector.select().from('type_towns').field('id').where('slug = ?', 'old_fortess')
    },
    {
      town_type_id: connector.select().from('type_towns').field('id').where('slug = ?', 'elf-city')
    },
    {
      town_type_id: connector.select().from('type_towns').field('id').where('slug = ?', 'orc-settlement')
    },
  ]).toString()
)
