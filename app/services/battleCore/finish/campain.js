const connector = require('../../../systems/connector')
const userService = require('../../users')
const { addItems, getItems } = require('../../items/helpers')
const battleCharacters = require('../characters')
const charactersService = require('../../characters')

const getTeams = async (id) => {
  return await Promise.all([
    await battleCharacters.getTeam(id),
    await charactersService.selectTeam(id, 'user')
  ])
}

module.exports = async (data) => {
  try {
    const { teamWinId, teamLossId, isUserWin, round } = data
    const userId = isUserWin ? teamWinId : teamLossId
    const campainLevelId = isUserWin ? teamLossId : teamWinId
    const items = []
    const complete = {
      is_complete_1: isUserWin ? true : false
    }

    if (!userId) return false;

    const [ userTeam, userRealTeam ] = await getTeams(userId)

    if (userTeam.length >= userRealTeam.characters.length) complete.is_complete_2 = true
    if (userRealTeam.characters.length * 2 > round.roundsCount) complete.is_complete_3 = true

    const [ level ] = await connector.end(
      connector.select()
      .from('campains_levels')
      .where('id = ?', campainLevelId)
      .toString()
    )

    const [ userLevel ] = await connector.end(
      connector.select()
      .from('users_campains_levels')
      .where('campain_level_id = ?', campainLevelId)
      .where('user_campain_id = ?', connector.select().from('users_campains').field('id').where('user_id = ?', userId))
      .toString()
    )

    let rewardItems = []
    if ( !userLevel.is_complete_1) {
      rewardItems = await connector.end(
        connector.select()
        .from('campains_levels_rewards','clr')
        .field('i.*')
        .field('it.name', 'type_name')
        .field('it.slug', 'type_slug')
        .left_join('items', 'i', 'i.id = clr.item_id')
        .left_join('items_types', 'it', 'it.id = i.type_id')
        .where('clr.campain_level_id = ?', campainLevelId)
        .toString()
      )
    }

    await connector.end(
      connector.update()
      .table('users_campains_levels')
      .setFields(complete)
      .where('campain_level_id = ?', campainLevelId)
      .where('user_campain_id = ?', connector.select().from('users_campains').field('id').where('user_id = ?', userId))
      .toString()
    )

    console.log(rewardItems)
    if (!rewardItems.length) return []

    rewardItems.forEach((val, i) => {
      items.push({
        user_id: teamWinId,
        count: 1,
        item_id: val.id
      })
    })

    await addItems(items, teamWinId)

    return await getItems(items)
  } catch (e) {
    throw new Error(e);
  }
}
