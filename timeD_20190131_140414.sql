-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- CREATE TABLE "arena_log" ------------------------------------
CREATE TABLE `arena_log` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`rate` Int( 11 ) NULL,
	`user_win_id` Int( 11 ) NULL,
	`user_loss_id` Int( 11 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 11;
-- -------------------------------------------------------------


-- CREATE TABLE "barrack_available_character" ------------------
CREATE TABLE `barrack_available_character` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`price` Int( 11 ) NULL,
	`town_id` Int( 11 ) NULL,
	`character_level_id` Int( 11 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 58;
-- -------------------------------------------------------------


-- CREATE TABLE "barrack_intervals" ----------------------------
CREATE TABLE `barrack_intervals` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`end` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`town_id` Int( 11 ) NULL,
	`start` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`user_id` Int( 255 ) NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 22;
-- -------------------------------------------------------------


-- CREATE TABLE "barrack_possible_character" -------------------
CREATE TABLE `barrack_possible_character` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`chance` Int( 11 ) NULL,
	`price` Int( 11 ) NULL,
	`town_type_id` Int( 11 ) NULL,
	`barrack_level` Int( 11 ) NULL,
	`character_level_id` Int( 11 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 3;
-- -------------------------------------------------------------


-- CREATE TABLE "barracks" -------------------------------------
CREATE TABLE `barracks` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`level` Int( 11 ) NULL,
	`discount` Int( 11 ) NULL,
	`price` Int( 11 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 12;
-- -------------------------------------------------------------


-- CREATE TABLE "battle_characters" ----------------------------
CREATE TABLE `battle_characters` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`position` Int( 11 ) NOT NULL,
	`health` Int( 11 ) NULL,
	`parent_id` Int( 11 ) NULL,
	`parent_table` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`owner_id` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`character_level_id` Int( 255 ) NOT NULL,
	`owner_type` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`mana` Int( 255 ) NOT NULL DEFAULT 0,
	`battle_id` Int( 255 ) NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "battle_characters_buffs" ----------------------
CREATE TABLE `battle_characters_buffs` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`battle_character_id` Int( 11 ) NULL,
	`skill_buff_id` Int( 11 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`data` Text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "battles" --------------------------------------
CREATE TABLE `battles` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 13;
-- -------------------------------------------------------------


-- CREATE TABLE "campains" -------------------------------------
CREATE TABLE `campains` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`town_type_id` Int( 11 ) NOT NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 2;
-- -------------------------------------------------------------


-- CREATE TABLE "campains_levels" ------------------------------
CREATE TABLE `campains_levels` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`campain_id` Int( 11 ) NOT NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`reward_gold` Int( 11 ) NULL,
	`reward_exp` Int( 11 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`slug` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 6;
-- -------------------------------------------------------------


-- CREATE TABLE "campains_levels_enemy" ------------------------
CREATE TABLE `campains_levels_enemy` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`campain_level_id` Int( 11 ) NOT NULL,
	`character_level_id` Int( 11 ) NOT NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`position` Int( 255 ) NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 16;
-- -------------------------------------------------------------


-- CREATE TABLE "campains_levels_rewards" ----------------------
CREATE TABLE `campains_levels_rewards` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`campain_level_id` Int( 11 ) NOT NULL,
	`item_id` Int( 11 ) NOT NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`count` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 1,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 15;
-- -------------------------------------------------------------


-- CREATE TABLE "character_levels" -----------------------------
CREATE TABLE `character_levels` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`slug` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`full_img` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`img` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`health` Int( 11 ) NOT NULL,
	`attack` Int( 11 ) NULL,
	`evasion` Int( 11 ) NULL,
	`initiative` Int( 11 ) NULL,
	`theft_life` Int( 11 ) NULL,
	`physical_armor` Int( 11 ) NULL,
	`magic_armor` Int( 11 ) NULL,
	`critical_hit_chance` Int( 11 ) NULL,
	`type_attack` Int( 11 ) NULL,
	`number_actions` Int( 11 ) NULL,
	`group_id` Int( 11 ) NULL,
	`type_id` Int( 11 ) NULL,
	`need_experience` Int( 11 ) NULL,
	`inheriot_at` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`mana` Int( 255 ) NOT NULL DEFAULT 5,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `slug` UNIQUE( `slug` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 84;
-- -------------------------------------------------------------


-- CREATE TABLE "characters" -----------------------------------
CREATE TABLE `characters` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`position` Int( 11 ) NULL,
	`experience` Int( 11 ) NOT NULL DEFAULT 0,
	`user_id` Int( 11 ) NULL,
	`character_level_id` Int( 11 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 135;
-- -------------------------------------------------------------


-- CREATE TABLE "characters_groups" ----------------------------
CREATE TABLE `characters_groups` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`slug` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`logo` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `slug` UNIQUE( `slug` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 3;
-- -------------------------------------------------------------


-- CREATE TABLE "characters_levels_skills" ---------------------
CREATE TABLE `characters_levels_skills` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`skill_id` Int( 11 ) NOT NULL,
	`character_level_id` Int( 11 ) NOT NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 28;
-- -------------------------------------------------------------


-- CREATE TABLE "craft_recipes" --------------------------------
CREATE TABLE `craft_recipes` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`min_craft_level` Int( 11 ) NULL,
	`craft_recipe_type` Int( 11 ) NOT NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`item_result` Int( 255 ) NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 5;
-- -------------------------------------------------------------


-- CREATE TABLE "craft_recipes_items" --------------------------
CREATE TABLE `craft_recipes_items` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`craft_recipe_id` Int( 11 ) NOT NULL,
	`item_id` Int( 11 ) NOT NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`count` Int( 255 ) NOT NULL DEFAULT 1,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 5;
-- -------------------------------------------------------------


-- CREATE TABLE "craft_recipes_types" --------------------------
CREATE TABLE `craft_recipes_types` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`slug` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `slug` UNIQUE( `slug` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 7;
-- -------------------------------------------------------------


-- CREATE TABLE "enemy_district_character" ---------------------
CREATE TABLE `enemy_district_character` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`enemy_team_id` Int( 11 ) NULL,
	`character_level_id` Int( 11 ) NULL,
	`position` Int( 11 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 5636;
-- -------------------------------------------------------------


-- CREATE TABLE "enemy_district_interval" ----------------------
CREATE TABLE `enemy_district_interval` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`end` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`town_id` Int( 11 ) NULL,
	`user_id` Int( 11 ) NULL,
	`start` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 2000;
-- -------------------------------------------------------------


-- CREATE TABLE "enemy_district_possible" ----------------------
CREATE TABLE `enemy_district_possible` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`character_level_id` Int( 11 ) NULL,
	`town_type_id` Int( 11 ) NULL,
	`min_town_level` Int( 11 ) NOT NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`chance` Int( 255 ) NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 4;
-- -------------------------------------------------------------


-- CREATE TABLE "enemy_district_teams" -------------------------
CREATE TABLE `enemy_district_teams` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`belonging` Int( 11 ) NULL,
	`number_of` Int( 11 ) NULL,
	`preview` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`town_id` Int( 11 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1131;
-- -------------------------------------------------------------


-- CREATE TABLE "items" ----------------------------------------
CREATE TABLE `items` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`slug` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`discription` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`img` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`type_id` Int( 11 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`data` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `slug` UNIQUE( `slug` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 20;
-- -------------------------------------------------------------


-- CREATE TABLE "items_character_cards" ------------------------
CREATE TABLE `items_character_cards` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`item_id` Int( 11 ) NULL,
	`character_level_id` Int( 11 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "items_chests_items" ---------------------------
CREATE TABLE `items_chests_items` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`item_parent_id` Int( 11 ) NULL,
	`item_id` Int( 11 ) NULL,
	`chance` Int( 11 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 8;
-- -------------------------------------------------------------


-- CREATE TABLE "items_types" ----------------------------------
CREATE TABLE `items_types` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`slug` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`discription` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `slug` UNIQUE( `slug` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 10;
-- -------------------------------------------------------------


-- CREATE TABLE "migrations" -----------------------------------
CREATE TABLE `migrations` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`run_on` DateTime NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 4;
-- -------------------------------------------------------------


-- CREATE TABLE "mines" ----------------------------------------
CREATE TABLE `mines` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`level` Int( 11 ) NULL,
	`gold_per_hour` Int( 11 ) NULL,
	`price` Int( 11 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 12;
-- -------------------------------------------------------------


-- CREATE TABLE "register_characters" --------------------------
CREATE TABLE `register_characters` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`character_level_id` Int( 11 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 5;
-- -------------------------------------------------------------


-- CREATE TABLE "skills" ---------------------------------------
CREATE TABLE `skills` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`slug` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`skill_type_id` Int( 11 ) NOT NULL,
	`data` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`chance` Int( 11 ) NULL,
	`turns_count` Int( 11 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`mana_cost` Int( 255 ) NOT NULL DEFAULT 1,
	`group_id` Int( 255 ) NOT NULL,
	`buff_id` Int( 255 ) NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `slug` UNIQUE( `slug` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 10;
-- -------------------------------------------------------------


-- CREATE TABLE "skills_buffs" ---------------------------------
CREATE TABLE `skills_buffs` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`slug` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`data` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `slug` UNIQUE( `slug` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 3;
-- -------------------------------------------------------------


-- CREATE TABLE "skills_groups" --------------------------------
CREATE TABLE `skills_groups` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`slug` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `slug` UNIQUE( `slug` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 7;
-- -------------------------------------------------------------


-- CREATE TABLE "skills_types" ---------------------------------
CREATE TABLE `skills_types` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`slug` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `slug` UNIQUE( `slug` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 4;
-- -------------------------------------------------------------


-- CREATE TABLE "towns" ----------------------------------------
CREATE TABLE `towns` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`level` Int( 11 ) NULL,
	`user_id` Int( 11 ) NULL,
	`mine_id` Int( 11 ) NULL,
	`barrack_id` Int( 11 ) NULL,
	`type_towns_id` Int( 11 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 12;
-- -------------------------------------------------------------


-- CREATE TABLE "towns_drops_possible" -------------------------
CREATE TABLE `towns_drops_possible` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`chance` Int( 11 ) NULL,
	`max_count` Int( 11 ) NULL,
	`town_type_id` Int( 11 ) NOT NULL,
	`item_id` Int( 11 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 4;
-- -------------------------------------------------------------


-- CREATE TABLE "type_characters" ------------------------------
CREATE TABLE `type_characters` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`slug` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `slug` UNIQUE( `slug` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 5;
-- -------------------------------------------------------------


-- CREATE TABLE "type_towns" -----------------------------------
CREATE TABLE `type_towns` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`slug` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`discription` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`rarity` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`image` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`chance_find` Int( 11 ) NULL,
	`gold_bust` Int( 11 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `slug` UNIQUE( `slug` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 8;
-- -------------------------------------------------------------


-- CREATE TABLE "users" ----------------------------------------
CREATE TABLE `users` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`login` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`email` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`gold` Int( 11 ) NULL,
	`password` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`rating` Int( 11 ) NULL DEFAULT 0,
	`active` TinyInt( 1 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `email` UNIQUE( `email` ),
	CONSTRAINT `login` UNIQUE( `login` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 24;
-- -------------------------------------------------------------


-- CREATE TABLE "users_campains" -------------------------------
CREATE TABLE `users_campains` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`user_id` Int( 11 ) NOT NULL,
	`campain_id` Int( 11 ) NOT NULL,
	`progress` Int( 11 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 5;
-- -------------------------------------------------------------


-- CREATE TABLE "users_campains_levels" ------------------------
CREATE TABLE `users_campains_levels` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`campain_level_id` Int( 11 ) NOT NULL,
	`user_campain_id` Int( 11 ) NOT NULL,
	`is_complete_1` TinyInt( 1 ) NULL,
	`is_complete_2` TinyInt( 1 ) NULL,
	`is_complete_3` TinyInt( 1 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 14;
-- -------------------------------------------------------------


-- CREATE TABLE "users_crafts" ---------------------------------
CREATE TABLE `users_crafts` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`user_id` Int( 11 ) NOT NULL,
	`level` Int( 11 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `user_id` UNIQUE( `user_id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 2;
-- -------------------------------------------------------------


-- CREATE TABLE "users_items" ----------------------------------
CREATE TABLE `users_items` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`item_id` Int( 11 ) NULL,
	`user_id` Int( 11 ) NOT NULL,
	`count` Int( 11 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 61;
-- -------------------------------------------------------------


-- CREATE TABLE "users_visits" ---------------------------------
CREATE TABLE `users_visits` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`user_id` Int( 11 ) NOT NULL,
	`days_count` Int( 11 ) NULL,
	`update_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "visits_rewards" -------------------------------
CREATE TABLE `visits_rewards` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`day` Int( 11 ) NULL,
	`reward_gold` Int( 11 ) NULL,
	`item_id` Int( 11 ) NULL,
	`item_count` Int( 11 ) NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 11;
-- -------------------------------------------------------------


-- Dump data of "arena_log" --------------------------------
INSERT INTO `arena_log`(`id`,`rate`,`user_win_id`,`user_loss_id`,`created_at`) VALUES 
( '1', '15', '17', '18', '2018-12-05 12:20:56' ),
( '2', '31', '17', '18', '2018-12-05 13:00:18' ),
( '3', '29', '17', '18', '2018-12-05 14:08:13' ),
( '4', '18', '18', '17', '2018-12-05 14:11:00' ),
( '5', '31', '18', '17', '2018-12-05 15:04:18' ),
( '6', '31', '17', '18', '2018-12-05 15:05:09' ),
( '7', '41', '17', '18', '2018-12-05 15:06:41' ),
( '8', '25', '19', '17', '2018-12-09 00:52:19' ),
( '9', '49', '19', '17', '2018-12-13 16:30:16' ),
( '10', '22', '17', '23', '2018-12-21 12:21:13' );
-- ---------------------------------------------------------


-- Dump data of "barrack_available_character" --------------
INSERT INTO `barrack_available_character`(`id`,`price`,`town_id`,`character_level_id`,`created_at`) VALUES 
( '1', '30', '5', '2', '2018-10-29 11:21:06' ),
( '2', '30', '5', '2', '2018-10-29 11:21:06' ),
( '3', '30', '5', '2', '2018-10-29 11:37:41' ),
( '4', '30', '5', '2', '2018-10-29 11:37:41' ),
( '5', '30', '5', '2', '2018-10-29 11:37:41' ),
( '6', '30', '5', '2', '2018-10-29 11:38:09' ),
( '7', '30', '5', '2', '2018-10-29 11:38:09' ),
( '8', '30', '5', '2', '2018-10-29 11:38:09' ),
( '9', '30', '5', '2', '2018-10-29 11:38:39' ),
( '10', '30', '5', '2', '2018-10-29 11:38:39' ),
( '11', '30', '5', '2', '2018-10-29 11:40:03' ),
( '12', '30', '5', '2', '2018-10-29 11:40:03' ),
( '13', '30', '5', '2', '2018-10-29 11:40:03' ),
( '14', '30', '5', '2', '2018-10-29 11:46:59' ),
( '15', '30', '5', '2', '2018-12-04 09:31:42' ),
( '16', '30', '5', '2', '2018-12-04 09:31:42' ),
( '17', '30', '5', '2', '2018-12-05 15:58:15' ),
( '18', '30', '5', '2', '2018-12-05 15:58:15' ),
( '19', '30', '5', '2', '2018-12-06 10:02:55' ),
( '20', '30', '5', '2', '2018-12-06 10:02:55' ),
( '21', '30', '5', '2', '2018-12-06 10:02:55' ),
( '22', '30', '5', '2', '2018-12-07 17:37:19' ),
( '23', '30', '5', '2', '2018-12-07 17:37:19' ),
( '24', '30', '5', '2', '2018-12-07 19:57:49' ),
( '25', '30', '5', '2', '2018-12-07 19:57:49' ),
( '26', '30', '5', '2', '2018-12-07 19:57:49' ),
( '27', '30', '5', '2', '2018-12-08 00:33:14' ),
( '28', '30', '5', '2', '2018-12-08 00:33:14' ),
( '29', '30', '5', '2', '2018-12-08 00:33:14' ),
( '30', '30', '5', '2', '2018-12-08 10:33:54' ),
( '31', '30', '5', '2', '2018-12-08 10:33:54' ),
( '32', '30', '5', '2', '2018-12-08 17:19:23' ),
( '33', '30', '5', '2', '2018-12-09 00:18:01' ),
( '34', '30', '5', '2', '2018-12-09 00:18:01' ),
( '35', '30', '5', '2', '2018-12-09 00:18:01' ),
( '36', '30', '5', '2', '2018-12-09 15:26:11' ),
( '37', '30', '5', '2', '2018-12-10 17:12:41' ),
( '38', '30', '5', '2', '2018-12-10 17:12:41' ),
( '39', '30', '5', '2', '2018-12-11 10:27:09' ),
( '40', '30', '5', '2', '2018-12-11 10:27:09' ),
( '41', '30', '5', '2', '2018-12-11 10:27:09' ),
( '42', '30', '5', '2', '2018-12-11 17:56:31' ),
( '43', '30', '5', '2', '2018-12-11 17:56:31' ),
( '44', '30', '5', '2', '2018-12-12 12:35:04' ),
( '45', '30', '5', '2', '2018-12-12 12:35:04' ),
( '46', '30', '5', '2', '2018-12-12 12:35:04' ),
( '47', '30', '5', '2', '2018-12-13 16:25:29' ),
( '48', '30', '5', '2', '2018-12-13 16:25:29' ),
( '49', '30', '5', '2', '2018-12-14 21:12:29' ),
( '50', '30', '5', '2', '2018-12-14 21:12:29' ),
( '51', '30', '5', '2', '2018-12-14 21:12:29' ),
( '52', '30', '5', '2', '2018-12-16 00:34:21' ),
( '53', '30', '5', '2', '2018-12-16 00:34:21' ),
( '54', '30', '5', '2', '2019-01-09 21:03:13' ),
( '55', '30', '5', '2', '2019-01-15 15:47:59' ),
( '56', '30', '5', '2', '2019-01-24 15:44:34' ),
( '57', '30', '5', '2', '2019-01-24 15:44:34' );
-- ---------------------------------------------------------


-- Dump data of "barrack_intervals" ------------------------
INSERT INTO `barrack_intervals`(`id`,`end`,`town_id`,`start`,`user_id`) VALUES 
( '21', '2019-01-25 03:44:34', '5', '2019-01-24 15:44:34', '17' );
-- ---------------------------------------------------------


-- Dump data of "barrack_possible_character" ---------------
INSERT INTO `barrack_possible_character`(`id`,`chance`,`price`,`town_type_id`,`barrack_level`,`character_level_id`,`created_at`) VALUES 
( '1', '100', '30', '1', '1', '2', '2018-10-25 13:20:59' ),
( '2', '50', '100', '1', '2', '5', '2018-10-25 13:20:59' );
-- ---------------------------------------------------------


-- Dump data of "barracks" ---------------------------------
INSERT INTO `barracks`(`id`,`level`,`discount`,`price`,`created_at`) VALUES 
( '1', '0', '0', '0', '2018-10-19 11:59:46' ),
( '2', '1', '1', '20', '2018-10-19 11:59:46' ),
( '3', '2', '2', '150', '2018-10-19 11:59:46' ),
( '4', '3', '3', '300', '2018-10-19 11:59:46' ),
( '5', '4', '5', '600', '2018-10-19 11:59:46' ),
( '6', '5', '10', '1000', '2018-10-19 11:59:46' ),
( '7', '6', '13', '3000', '2018-10-19 11:59:46' ),
( '8', '7', '15', '8000', '2018-10-19 11:59:46' ),
( '9', '8', '20', '30000', '2018-10-19 11:59:46' ),
( '10', '9', '25', '200000', '2018-10-19 11:59:46' ),
( '11', '10', '40', '500000', '2018-10-19 11:59:46' );
-- ---------------------------------------------------------


-- Dump data of "battle_characters" ------------------------
-- ---------------------------------------------------------


-- Dump data of "battle_characters_buffs" ------------------
-- ---------------------------------------------------------


-- Dump data of "battles" ----------------------------------
INSERT INTO `battles`(`id`,`created_at`) VALUES 
( '1', '2018-12-21 16:54:02' ),
( '2', '2018-12-21 16:54:42' ),
( '3', '2018-12-21 16:55:19' ),
( '4', '2019-01-09 21:00:41' ),
( '5', '2019-01-09 21:17:07' ),
( '6', '2019-01-15 15:38:08' ),
( '7', '2019-01-15 15:38:34' ),
( '8', '2019-01-15 15:38:47' ),
( '9', '2019-01-15 16:25:43' ),
( '10', '2019-01-24 14:48:43' ),
( '11', '2019-01-24 15:43:32' ),
( '12', '2019-01-24 15:45:46' );
-- ---------------------------------------------------------


-- Dump data of "campains" ---------------------------------
INSERT INTO `campains`(`id`,`town_type_id`,`created_at`) VALUES 
( '1', '1', '2018-12-16 20:44:20' );
-- ---------------------------------------------------------


-- Dump data of "campains_levels" --------------------------
INSERT INTO `campains_levels`(`id`,`campain_id`,`name`,`reward_gold`,`reward_exp`,`created_at`,`slug`) VALUES 
( '1', '1', 'Уровень 1', '5', '10', '2018-12-15 19:23:13', 'village_lvl_1' ),
( '2', '1', 'Уровень 2', '5', '10', '2018-12-15 19:23:13', 'village_lvl_2' ),
( '3', '1', 'Уровень 3', '5', '10', '2018-12-15 19:23:13', 'village_lvl_3' ),
( '4', '1', 'Уровень 4', '5', '10', '2018-12-15 19:23:13', 'village_lvl_4' ),
( '5', '1', 'Уровень 5', '5', '10', '2018-12-15 19:23:13', 'village_lvl_5' );
-- ---------------------------------------------------------


-- Dump data of "campains_levels_enemy" --------------------
INSERT INTO `campains_levels_enemy`(`id`,`campain_level_id`,`character_level_id`,`created_at`,`position`) VALUES 
( '1', '1', '68', '2018-12-15 19:37:56', '3' ),
( '2', '2', '68', '2018-12-16 18:52:48', '2' ),
( '3', '2', '69', '2018-12-16 18:52:48', '3' ),
( '4', '2', '68', '2018-12-16 18:52:48', '4' ),
( '5', '5', '73', '2018-12-16 20:34:44', '3' ),
( '9', '3', '69', '2018-12-16 20:34:44', '2' ),
( '10', '3', '69', '2018-12-16 20:34:44', '3' ),
( '11', '3', '69', '2018-12-16 20:34:44', '4' ),
( '12', '4', '70', '2018-12-16 20:34:44', '2' ),
( '13', '4', '69', '2018-12-16 20:34:44', '3' ),
( '14', '4', '69', '2018-12-16 20:34:44', '4' ),
( '15', '4', '70', '2018-12-16 20:34:44', '5' );
-- ---------------------------------------------------------


-- Dump data of "campains_levels_rewards" ------------------
INSERT INTO `campains_levels_rewards`(`id`,`campain_level_id`,`item_id`,`created_at`,`count`) VALUES 
( '1', '1', '7', '2018-12-15 19:53:12', '1' ),
( '2', '1', '6', '2018-12-15 19:53:12', '1' ),
( '5', '4', '2', '2018-12-16 19:32:39', '2' ),
( '6', '2', '7', '2018-12-16 19:32:39', '2' ),
( '7', '2', '2', '2018-12-16 19:32:39', '1' ),
( '8', '2', '6', '2018-12-16 19:32:39', '1' ),
( '9', '3', '7', '2018-12-16 19:32:39', '2' ),
( '10', '3', '2', '2018-12-16 19:32:39', '1' ),
( '11', '3', '6', '2018-12-16 19:32:39', '1' ),
( '12', '5', '7', '2018-12-16 19:32:39', '5' ),
( '13', '5', '2', '2018-12-16 19:32:39', '1' ),
( '14', '5', '1', '2018-12-16 19:32:39', '1' );
-- ---------------------------------------------------------


-- Dump data of "character_levels" -------------------------
INSERT INTO `character_levels`(`id`,`name`,`slug`,`full_img`,`img`,`health`,`attack`,`evasion`,`initiative`,`theft_life`,`physical_armor`,`magic_armor`,`critical_hit_chance`,`type_attack`,`number_actions`,`group_id`,`type_id`,`need_experience`,`inheriot_at`,`created_at`,`mana`) VALUES 
( '2', 'Бродяга', 'tramp', '/character/tramp/1.jpg', '/character/tramp/thumbnail/1.jpg', '4', '1', '0', '1', '0', '0', '0', '0', '1', '1', NULL, '1', '0', '0', '2018-10-19 12:30:34', '5' ),
( '5', 'Лучник I', 'boowI', '/character/boow/boow.jpg', '/character/boow/thumbnail/boow.jpg', '4', '1', '0', '5', '0', '0', '0', '0', '2', '2', NULL, '2', '0', '0', '2018-10-19 12:41:12', '5' ),
( '6', 'Лучник II', 'boowII', '/character/boow/boow.jpg', '/character/boow/thumbnail/boow.jpg', '6', '2', '0', '6', '0', '0', '0', '0', '2', '2', NULL, '2', '50', 'boowI', '2018-10-19 12:41:12', '5' ),
( '7', 'Лучник III', 'boowIII', '/character/boow/boow.jpg', '/character/boow/thumbnail/boow.jpg', '6', '2', '0', '6', '0', '1', '1', '0', '2', '2', NULL, '2', '500', 'boowII', '2018-12-05 15:56:21', '5' ),
( '8', 'Усиленый лучник', 'PrivateArcher', '/character/boow/2/PrivateArcher.jpg', '/character/boow/thumbnail/PrivateArcher.jpg', '6', '3', '0', '7', '0', '1', '1', '0', '2', '2', NULL, '2', '700', 'boowIII', '2018-12-05 15:56:21', '5' ),
( '9', 'Исскустный вор', 'thief', '/character/tramp/thief.jpg', '/character/tramp/thumbnail/thief.jpg', '6', '3', '5', '1', '0', '1', '0', '0', '1', '1', NULL, '1', '300', 'tramp3', '2018-12-05 16:08:53', '5' ),
( '10', 'Воин I', 'warriorI', '/character/warrior/warriorI.jpg', '/character/warrior/thumbnail/warriorI.jpg', '6', '1', '0', '1', '0', '1', '0', '0', '1', '1', NULL, '1', '0', NULL, '2018-12-05 16:28:00', '5' ),
( '11', 'Воин II', 'warriorII', '/character/warrior/warriorII.jpg', '/character/warrior/thumbnail/warriorII.jpg', '6', '2', '0', '1', '0', '1', '0', '0', '1', '1', NULL, '1', '150', 'warriorI', '2018-12-05 16:28:00', '5' ),
( '12', 'Воин III', 'warriorIII', '/character/warrior/SwordMaster.jpg', '/character/warrior/thumbnail/SwordMaster.jpg', '6', '2', '0', '1', '0', '2', '0', '0', '1', '1', NULL, '1', '400', 'warriorII', '2018-12-05 16:28:00', '5' ),
( '25', 'Ельфийская лучица I', 'elf-boowI', '/character/elf-boow/elf-boow-1.jpg', '/character/elf-boow/thumbnail/elf-boow-1.jpg', '5', '3', '5', '6', '0', '0', '2', '0', '2', '2', '2', '2', '0', '0', '2018-12-08 19:11:09', '5' ),
( '26', 'Ельфийская лучица II', 'elf-boowII', '/character/elf-boow/elf-boow-1.jpg', '/character/elf-boow/thumbnail/elf-boow-1.jpg', '6', '3', '5', '6', '0', '0', '2', '0', '2', '2', '2', '2', '300', 'elf-boowI', '2018-12-08 19:11:09', '5' ),
( '27', 'Ельфийская лучица III', 'elf-boowIII', '/character/elf-boow/elf-boow-1.jpg', '/character/elf-boow/thumbnail/elf-boow-1.jpg', '6', '3', '10', '6', '0', '0', '3', '5', '2', '2', '2', '2', '500', 'elf-boowII', '2018-12-08 19:11:09', '5' ),
( '28', 'Ельфийская охотница I', 'elf-hunterI', '/character/elf-boow/elf-boow.jpg', '/character/elf-boow/thumbnail/elf-boow.jpg', '5', '4', '5', '7', '0', '0', '2', '0', '2', '2', '2', '2', '700', 'elf-boowIII', '2018-12-08 19:11:09', '5' ),
( '29', 'Ельфийская охотница II', 'elf-hunterII', '/character/elf-boow/elf-boow.jpg', '/character/elf-boow/thumbnail/elf-boow.jpg', '5', '4', '5', '7', '0', '2', '3', '0', '2', '2', '2', '2', '700', 'elf-hunterI', '2018-12-08 19:11:09', '5' ),
( '30', 'Ельфийская охотница III', 'elf-hunterIII', '/character/elf-boow/elf-boow.jpg', '/character/elf-boow/thumbnail/elf-boow.jpg', '5', '5', '15', '7', '0', '2', '3', '0', '2', '2', '2', '2', '1500', 'elf-hunterII', '2018-12-08 19:11:09', '5' ),
( '31', 'Ельфийский воин I', 'elf-warriorI', '/character/elf-warrior/elf-warriorI.jpg', '/character/elf-warrior/thumbnail/elf-warriorI.jpg', '10', '3', '10', '2', '0', '0', '0', '0', '1', '1', '2', '1', '0', '0', '2018-12-08 23:16:29', '5' ),
( '32', 'Ельфийский воин II', 'elf-warriorII', '/character/elf-warrior/elf-warriorII.jpg', '/character/elf-warrior/thumbnail/elf-warriorII.jpg', '10', '4', '10', '2', '0', '2', '2', '0', '1', '1', '2', '1', '200', 'elf-warriorI', '2018-12-08 23:16:29', '5' ),
( '33', 'Ельфийский воин III', 'elf-warriorIII', '/character/elf-warrior/elf-warriorIII.jpg', '/character/elf-warrior/thumbnail/elf-warriorIII.jpg', '10', '4', '15', '2', '0', '2', '2', '0', '1', '1', '2', '1', '400', 'elf-warriorII', '2018-12-08 23:16:29', '5' ),
( '34', 'Главнокомандующий воинов I', 'elf-warriorIV', '/character/elf-warrior/elf-warriorIII.jpg', '/character/elf-warrior/thumbnail/elf-warriorIV.jpg', '13', '5', '15', '3', '0', '2', '2', '0', '1', '1', '2', '1', '1000', 'elf-warriorIII', '2018-12-08 23:16:29', '5' ),
( '35', 'Главнокомандующий воинов II', 'elf-warriorV', '/character/elf-warrior/elf-warriorIII.jpg', '/character/elf-warrior/thumbnail/elf-warriorIV.jpg', '15', '5', '15', '3', '0', '2', '2', '0', '1', '1', '2', '1', '1000', 'elf-warriorIV', '2018-12-08 23:16:29', '5' ),
( '36', 'Главнокомандующий воинов III', 'elf-warriorVI', '/character/elf-warrior/elf-warriorIII.jpg', '/character/elf-warrior/thumbnail/elf-warriorIV.jpg', '15', '5', '20', '3', '0', '4', '2', '0', '1', '1', '2', '1', '1000', 'elf-warriorV', '2018-12-08 23:16:29', '5' ),
( '39', 'Ельфийский снайпер', 'elf-shiper', '/character/elf-boow/stronger-boow.jpg', '/character/elf-boow/thumbnail/stronger-boow.jpg', '5', '15', '15', '7', '0', '2', '3', '0', '2', '1', '2', '2', '1500', 'elf-hunterIII', '2018-12-08 23:50:08', '5' ),
( '40', 'Темный охотник', 'elf-dark-hunter', '/character/elf-boow/dark-boow.jpg', '/character/elf-boow/thumbnail/dark-boow.jpg', '5', '5', '25', '7', '0', '0', '5', '0', '2', '1', '2', '3', '1500', 'elf-hunterIII', '2018-12-08 23:50:08', '5' ),
( '41', 'Орк-шаман I', 'orc-shamanI', '/character/orc-shaman/orc-shaman.jpg', '/character/orc-shaman/thumbnail/orc-shaman.jpg', '10', '2', '0', '4', '0', '0', '0', '0', '2', '1', NULL, '3', '0', '0', '2018-12-09 00:09:59', '5' ),
( '42', 'Орк-шаман II', 'orc-shamanII', '/character/orc-shaman/orc-shaman.jpg', '/character/orc-shaman/thumbnail/orc-shaman.jpg', '10', '3', '0', '4', '0', '0', '0', '0', '2', '1', NULL, '3', '200', 'orc-shamanI', '2018-12-09 00:09:59', '5' ),
( '43', 'Орк-шаман III', 'orc-shamanIII', '/character/orc-shaman/orc-shaman.jpg', '/character/orc-shaman/thumbnail/orc-shaman.jpg', '10', '4', '0', '4', '0', '0', '0', '0', '2', '1', NULL, '3', '500', 'orc-shamanII', '2018-12-09 00:09:59', '5' ),
( '44', 'Орк-врачеватель', 'orc-healer', '/character/orc-shaman/orc-shamanIII.jpg', '/character/orc-shaman/thumbnail/orc-shamanIII.jpg', '15', '3', '0', '1', '0', '0', '0', '0', '2', '1', NULL, '4', '1000', 'orc-shamanIII', '2018-12-09 00:09:59', '5' ),
( '45', 'Орк-призыватель', 'orc-summoner', '/character/orc-shaman/orc-shamanII.jpg', '/character/orc-shaman/thumbnail/orc-shamanII.jpg', '10', '5', '0', '4', '2', '0', '0', '0', '2', '1', NULL, '3', '500', 'orc-shamanIII', '2018-12-09 00:15:55', '5' ),
( '46', 'Орк воин I', 'orc-warrior', '/character/orc-warrior/orc-warrior.jpg', '/character/orc-warrior/thumbnail/orc-warrior.jpg', '10', '5', '0', '4', '2', '0', '0', '15', '1', '1', NULL, '1', '0', '', '2018-12-09 00:27:29', '5' ),
( '47', 'Орк воин II', 'orc-warriorII', '/character/orc-warrior/orc-warrior.jpg', '/character/orc-warrior/thumbnail/orc-warrior.jpg', '10', '6', '0', '4', '2', '0', '0', '15', '1', '1', NULL, '1', '200', 'orc-warrior', '2018-12-09 00:27:29', '5' ),
( '48', 'Орк воин III', 'orc-warriorIII', '/character/orc-warrior/orc-warrior.jpg', '/character/orc-warrior/thumbnail/orc-warrior.jpg', '12', '6', '0', '4', '0', '0', '0', '20', '1', '1', NULL, '1', '500', 'orc-warriorII', '2018-12-09 00:27:29', '5' ),
( '49', 'Вождь орков I', 'orc-leaderI', '/character/orc-warrior/orc-warriorII.jpg', '/character/orc-warrior/thumbnail/orc-warriorII.jpg', '12', '6', '0', '4', '0', '0', '0', '25', '1', '1', NULL, '1', '1000', 'orc-warriorIII', '2018-12-09 00:27:29', '5' ),
( '50', 'Вождь орков II', 'orc-leaderII', '/character/orc-warrior/orc-warriorII.jpg', '/character/orc-warrior/thumbnail/orc-warriorII.jpg', '13', '6', '0', '5', '0', '0', '0', '25', '1', '1', NULL, '1', '1000', 'orc-leaderI', '2018-12-09 00:27:29', '5' ),
( '51', 'Вождь орков III', 'orc-leaderIII', '/character/orc-warrior/orc-warriorII.jpg', '/character/orc-warrior/thumbnail/orc-warriorII.jpg', '15', '6', '5', '5', '0', '0', '0', '30', '1', '1', NULL, '1', '1000', 'orc-leaderII', '2018-12-09 00:27:29', '5' ),
( '53', 'Амун', 'amunI', '/character/heroes/amun.jpg', '/character/heroes/thumbnail/amun.jpg', '15', '3', '5', '1', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '2018-12-13 10:38:51', '3' ),
( '54', 'Амун', 'amunII', '/character/heroes/amun.jpg', '/character/heroes/thumbnail/amun.jpg', '15', '3', '10', '1', '0', '1', '1', '0', '1', '1', '1', '1', '1000', 'amunI', '2018-12-13 10:38:51', '4' ),
( '55', 'Амун', 'amunIII', '/character/heroes/amun.jpg', '/character/heroes/thumbnail/amun.jpg', '15', '3', '10', '1', '0', '1', '1', '0', '1', '1', '1', '1', '1000', 'amunII', '2018-12-13 10:38:51', '5' ),
( '56', 'Амун', 'amunIV', '/character/heroes/amun.jpg', '/character/heroes/thumbnail/amun.jpg', '20', '3', '10', '1', '0', '1', '1', '0', '1', '1', '1', '1', '1000', 'amunIII', '2018-12-13 10:38:51', '5' ),
( '57', 'Амун', 'amunV', '/character/heroes/amun.jpg', '/character/heroes/thumbnail/amun.jpg', '25', '3', '15', '1', '0', '1', '1', '0', '1', '1', '1', '1', '1000', 'amunIV', '2018-12-13 10:38:51', '6' ),
( '63', 'Сехит', 'sehitI', '/character/heroes/sehit.jpg', '/character/heroes/thumbnail/sehit.jpg', '10', '3', '0', '1', '0', '1', '1', '0', '1', '1', '1', '1', '0', '0', '2018-12-13 22:53:07', '3' ),
( '64', 'Сехит', 'sehitII', '/character/heroes/sehit.jpg', '/character/heroes/thumbnail/sehit.jpg', '10', '3', '0', '3', '0', '1', '1', '0', '1', '1', '1', '1', '1000', 'sehitI', '2018-12-13 22:53:07', '3' ),
( '65', 'Сехит', 'sehitIII', '/character/heroes/sehit.jpg', '/character/heroes/thumbnail/sehit.jpg', '15', '3', '0', '3', '0', '1', '1', '0', '1', '1', '1', '1', '1000', 'sehitII', '2018-12-13 22:53:07', '5' ),
( '66', 'Сехит', 'sehitIV', '/character/heroes/sehit.jpg', '/character/heroes/thumbnail/sehit.jpg', '17', '3', '0', '3', '0', '2', '1', '0', '1', '1', '1', '1', '1000', 'sehitIII', '2018-12-13 22:53:07', '5' ),
( '67', 'Сехит', 'sehitV', '/character/heroes/sehit.jpg', '/character/heroes/thumbnail/sehit.jpg', '20', '5', '0', '3', '0', '2', '1', '0', '1', '1', '1', '1', '1000', 'sehitIV', '2018-12-13 22:53:07', '5' ),
( '68', 'Маленький гоблин', 'goblinI', '/character/goblin/small-goblin.jpg', '/character/goblin/thumbnail/small-goblin.jpg', '3', '1', '0', '1', '0', '0', '0', '0', '1', '1', NULL, '1', '0', '0', '2018-12-15 19:34:03', '5' ),
( '69', 'Слабый гоблин', 'goblinII', '/character/goblin/goblin.jpg', '/character/goblin/thumbnail/goblin.jpg', '4', '1', '1', '2', '0', '0', '0', '0', '1', '1', NULL, '1', '100', 'goblinI', '2018-12-15 19:34:03', '5' ),
( '70', 'Гоблин', 'goblinIII', '/character/goblin/goblin.jpg', '/character/goblin/thumbnail/goblin.jpg', '4', '2', '1', '2', '0', '0', '0', '0', '1', '1', NULL, '1', '100', 'goblinII', '2018-12-15 19:34:03', '5' ),
( '71', 'Гоблин шаман', 'goblinShamanI', '/character/goblin/goblin-shaman.jpg', '/character/goblin/thumbnail/goblin-shaman.jpg', '3', '2', '0', '3', '0', '0', '0', '0', '1', '1', NULL, '3', '0', '0', '2018-12-16 19:38:28', '5' ),
( '72', 'Гоблин заклинатель', 'goblinShamanII', '/character/goblin/goblin-shaman.jpg', '/character/goblin/thumbnail/goblin-shaman.jpg', '3', '3', '0', '3', '0', '0', '0', '0', '1', '1', NULL, '1', '0', '0', '2018-12-16 19:38:28', '5' ),
( '73', 'Пещерны змей', 'cavemanSnake', '/character/snake/caveman.jpg', '/character/snake/thumbnail/caveman.jpg', '15', '3', '0', '1', '0', '1', '0', '0', '1', '1', NULL, '1', '0', '0', '2018-12-16 20:31:12', '5' ),
( '74', 'Лотанари', 'lotanariI', '/character/heroes/lotanari.jpg', '/character/heroes/thumbnail/lotanari.jpg', '10', '2', '0', '1', '0', '0', '2', '0', '1', '1', '2', '3', '0', '0', '2018-12-20 18:17:22', '5' ),
( '75', 'Лотанари', 'lotanariII', '/character/heroes/lotanari.jpg', '/character/heroes/thumbnail/lotanari.jpg', '15', '2', '0', '1', '0', '0', '2', '0', '1', '1', '2', '3', '1000', 'lotanariI', '2018-12-20 18:17:22', '5' ),
( '76', 'Лотанари', 'lotanariIII', '/character/heroes/lotanari.jpg', '/character/heroes/thumbnail/lotanari.jpg', '15', '2', '0', '1', '0', '0', '2', '0', '1', '1', '2', '3', '1000', 'lotanariII', '2018-12-20 18:17:22', '5' ),
( '77', 'Лотанари', 'lotanariIV', '/character/heroes/lotanari.jpg', '/character/heroes/thumbnail/lotanari.jpg', '15', '3', '0', '1', '0', '0', '2', '0', '1', '1', '2', '3', '1000', 'lotanariIII', '2018-12-20 18:17:22', '5' ),
( '78', 'Лотанари', 'lotanariV', '/character/heroes/lotanari.jpg', '/character/heroes/thumbnail/lotanari.jpg', '15', '3', '0', '1', '0', '0', '2', '0', '1', '1', '2', '3', '1000', 'lotanariIV', '2018-12-20 18:17:22', '8' ),
( '79', 'Хасан', 'hasanI', '/character/heroes/hasan.jpg', '/character/heroes/thumbnail/hasan.jpg', '10', '2', '0', '4', '0', '0', '0', '0', '1', '2', '1', '2', '0', '0', '2018-12-20 18:43:57', '2' ),
( '80', 'Хасан', 'hasanII', '/character/heroes/hasan.jpg', '/character/heroes/thumbnail/hasan.jpg', '10', '2', '0', '5', '0', '0', '0', '0', '1', '2', '1', '2', '1000', 'hasanI', '2018-12-20 18:43:57', '3' ),
( '81', 'Хасан', 'hasanIII', '/character/heroes/hasan.jpg', '/character/heroes/thumbnail/hasan.jpg', '15', '2', '0', '5', '0', '0', '0', '0', '1', '2', '1', '2', '1000', 'hasanII', '2018-12-20 18:43:57', '3' ),
( '82', 'Хасан', 'hasanIV', '/character/heroes/hasan.jpg', '/character/heroes/thumbnail/hasan.jpg', '15', '2', '0', '6', '0', '0', '0', '0', '1', '2', '1', '2', '1000', 'hasanIII', '2018-12-20 18:43:57', '3' ),
( '83', 'Хасан', 'hasanV', '/character/heroes/hasan.jpg', '/character/heroes/thumbnail/hasan.jpg', '15', '3', '0', '6', '0', '0', '0', '0', '1', '2', '1', '2', '1000', 'hasanIV', '2018-12-20 18:43:57', '3' );
-- ---------------------------------------------------------


-- Dump data of "characters" -------------------------------
INSERT INTO `characters`(`id`,`position`,`experience`,`user_id`,`character_level_id`,`created_at`) VALUES 
( '1', '6', '4813', '19', '40', '2018-12-09 00:34:10' ),
( '2', '9', '10013', '19', '5', '2018-12-09 00:34:10' ),
( '3', '7', '10013', '19', '40', '2018-12-09 00:34:10' ),
( '4', '10', '10013', '19', '40', '2018-12-09 00:34:10' ),
( '5', '8', '4813', '19', '39', '2018-12-09 00:34:10' ),
( '6', '1', '6430', '19', '36', '2018-12-09 00:34:10' ),
( '7', '2', '10030', '19', '36', '2018-12-09 00:34:10' ),
( '8', '3', '10002', '19', '36', '2018-12-09 00:34:10' ),
( '9', '4', '10013', '19', '36', '2018-12-09 00:34:10' ),
( '10', '5', '10030', '19', '36', '2018-12-09 00:34:10' ),
( '11', NULL, '6305', '17', '51', '2018-12-09 00:34:10' ),
( '12', NULL, '9108', '17', '7', '2018-12-09 00:34:10' ),
( '13', '4', '10210', '17', '51', '2018-12-09 00:34:10' ),
( '14', '5', '10105', '17', '51', '2018-12-09 00:34:10' ),
( '15', NULL, '10005', '17', '51', '2018-12-09 00:34:10' ),
( '111', NULL, '8811', '17', '45', '2018-12-09 00:34:10' ),
( '121', NULL, '10005', '17', '45', '2018-12-09 00:34:10' ),
( '122', NULL, '10005', '17', '45', '2018-12-09 00:34:10' ),
( '123', '2', '4438', '17', '67', '2018-12-09 00:34:10' ),
( '124', NULL, '10110', '17', '44', '2018-12-09 00:34:10' ),
( '125', '3', '648', '17', '57', '2018-12-09 16:12:20' ),
( '126', '2', '600', '20', '54', '2018-12-16 20:41:11' ),
( '130', '3', '250', '20', '12', '2018-12-16 21:47:43' ),
( '131', NULL, '0', '21', '79', '2018-12-21 11:42:55' ),
( '132', '3', '100', '22', '79', '2018-12-21 11:48:14' ),
( '133', '8', '200', '23', '79', '2018-12-21 11:50:15' ),
( '134', NULL, '0', '17', '10', '2019-01-24 15:44:40' );
-- ---------------------------------------------------------


-- Dump data of "characters_groups" ------------------------
INSERT INTO `characters_groups`(`id`,`name`,`slug`,`logo`,`created_at`) VALUES 
( '1', 'Люди', 'people', NULL, '2018-12-08 18:59:05' ),
( '2', 'Ельфы', 'elf', NULL, '2018-12-08 18:59:05' );
-- ---------------------------------------------------------


-- Dump data of "characters_levels_skills" -----------------
INSERT INTO `characters_levels_skills`(`id`,`skill_id`,`character_level_id`,`created_at`) VALUES 
( '1', '1', '5', '2018-12-09 19:41:06' ),
( '2', '1', '6', '2018-12-11 09:54:46' ),
( '3', '1', '7', '2018-12-11 09:54:46' ),
( '5', '2', '7', '2018-12-12 11:36:34' ),
( '6', '4', '7', '2018-12-12 11:36:53' ),
( '16', '5', '53', '2018-12-13 11:38:33' ),
( '17', '5', '54', '2018-12-13 11:38:33' ),
( '18', '7', '54', '2018-12-13 11:38:33' ),
( '19', '6', '55', '2018-12-13 11:38:33' ),
( '20', '7', '55', '2018-12-13 11:38:33' ),
( '21', '6', '56', '2018-12-13 11:38:33' ),
( '22', '7', '56', '2018-12-13 11:38:33' ),
( '23', '6', '57', '2018-12-13 11:38:33' ),
( '24', '7', '57', '2018-12-13 11:38:33' ),
( '26', '8', '57', '2018-12-13 11:39:19' ),
( '27', '9', '63', '2018-12-13 22:57:38' );
-- ---------------------------------------------------------


-- Dump data of "craft_recipes" ----------------------------
INSERT INTO `craft_recipes`(`id`,`min_craft_level`,`craft_recipe_type`,`created_at`,`item_result`) VALUES 
( '1', '1', '1', '2019-01-15 11:49:14', '8' ),
( '2', '1', '1', '2019-01-15 11:49:14', '17' ),
( '3', '2', '1', '2019-01-15 11:49:14', '18' ),
( '4', '2', '1', '2019-01-15 11:49:14', '19' );
-- ---------------------------------------------------------


-- Dump data of "craft_recipes_items" ----------------------
INSERT INTO `craft_recipes_items`(`id`,`craft_recipe_id`,`item_id`,`created_at`,`count`) VALUES 
( '1', '1', '7', '2019-01-15 11:49:14', '10' ),
( '2', '2', '8', '2019-01-15 11:49:14', '10' ),
( '3', '3', '17', '2019-01-15 11:49:14', '10' ),
( '4', '4', '18', '2019-01-15 11:49:14', '10' );
-- ---------------------------------------------------------


-- Dump data of "craft_recipes_types" ----------------------
INSERT INTO `craft_recipes_types`(`id`,`name`,`slug`,`created_at`) VALUES 
( '1', 'Ресурсы', 'resources', '2019-01-14 22:40:23' ),
( '2', 'Зелья', 'potions', '2019-01-14 22:40:23' ),
( '3', 'Картоки', 'cards', '2019-01-14 22:40:23' ),
( '4', 'Сферы', 'sheres', '2019-01-14 22:40:23' ),
( '5', 'Кольца', 'rings', '2019-01-14 22:40:23' ),
( '6', 'Другое', 'others', '2019-01-14 22:40:23' );
-- ---------------------------------------------------------


-- Dump data of "enemy_district_character" -----------------
INSERT INTO `enemy_district_character`(`id`,`enemy_team_id`,`character_level_id`,`position`,`created_at`) VALUES 
( '5141', '1039', '69', '1', '2018-12-19 21:26:20' ),
( '5142', '1039', '68', '2', '2018-12-19 21:26:20' ),
( '5143', '1039', '69', '3', '2018-12-19 21:26:20' ),
( '5144', '1039', '70', '4', '2018-12-19 21:26:20' ),
( '5145', '1039', '70', '5', '2018-12-19 21:26:20' ),
( '5146', '1039', '70', '6', '2018-12-19 21:26:20' ),
( '5633', '1130', '70', '1', '2019-01-30 18:31:37' ),
( '5634', '1130', '70', '2', '2019-01-30 18:31:37' ),
( '5635', '1130', '70', '3', '2019-01-30 18:31:37' );
-- ---------------------------------------------------------


-- Dump data of "enemy_district_interval" ------------------
INSERT INTO `enemy_district_interval`(`id`,`end`,`town_id`,`user_id`,`start`) VALUES 
( '1601', '2018-12-19 20:55:55', '7', '19', '2018-12-19 20:55:55' ),
( '1603', '2018-12-19 21:26:19', '7', '19', '2018-12-19 21:26:20' ),
( '1640', '2018-12-19 22:04:56', '8', '20', '2018-12-19 22:04:56' ),
( '1641', '2018-12-19 22:04:56', '8', '20', '2018-12-19 22:04:56' ),
( '1642', '2018-12-19 22:04:56', '8', '20', '2018-12-19 22:04:57' ),
( '1643', '2018-12-19 22:04:57', '8', '20', '2018-12-19 22:04:57' ),
( '1644', '2018-12-19 22:04:57', '8', '20', '2018-12-19 22:04:57' ),
( '1645', '2018-12-19 22:04:57', '8', '20', '2018-12-19 22:04:57' ),
( '1646', '2018-12-19 22:04:57', '8', '20', '2018-12-19 22:04:57' ),
( '1647', '2018-12-19 22:04:57', '8', '20', '2018-12-19 22:04:57' ),
( '1648', '2018-12-19 22:04:57', '8', '20', '2018-12-19 22:04:57' ),
( '1649', '2018-12-19 22:04:57', '8', '20', '2018-12-19 22:04:57' ),
( '1650', '2018-12-19 22:04:57', '8', '20', '2018-12-19 22:04:57' ),
( '1651', '2018-12-19 22:04:57', '8', '20', '2018-12-19 22:04:57' ),
( '1652', '2018-12-19 22:04:57', '8', '20', '2018-12-19 22:04:57' ),
( '1653', '2018-12-19 22:04:57', '8', '20', '2018-12-19 22:04:57' ),
( '1654', '2018-12-19 22:04:57', '8', '20', '2018-12-19 22:04:57' ),
( '1655', '2018-12-19 22:04:57', '8', '20', '2018-12-19 22:04:57' ),
( '1656', '2018-12-19 22:04:57', '8', '20', '2018-12-19 22:04:57' ),
( '1657', '2018-12-19 22:04:57', '8', '20', '2018-12-19 22:04:57' ),
( '1658', '2018-12-19 22:04:57', '8', '20', '2018-12-19 22:04:57' ),
( '1659', '2018-12-19 22:04:57', '8', '20', '2018-12-19 22:04:57' ),
( '1660', '2018-12-19 22:05:16', '8', '20', '2018-12-19 22:05:16' ),
( '1661', '2018-12-19 22:05:17', '8', '20', '2018-12-19 22:05:17' ),
( '1662', '2018-12-19 22:05:17', '8', '20', '2018-12-19 22:05:17' ),
( '1663', '2018-12-19 22:05:17', '8', '20', '2018-12-19 22:05:17' ),
( '1664', '2018-12-19 22:05:17', '8', '20', '2018-12-19 22:05:17' ),
( '1665', '2018-12-19 22:05:17', '8', '20', '2018-12-19 22:05:17' ),
( '1666', '2018-12-19 22:05:17', '8', '20', '2018-12-19 22:05:17' ),
( '1667', '2018-12-19 22:18:53', '8', '20', '2018-12-19 22:18:53' ),
( '1707', '2018-12-21 12:41:09', '11', '23', '2018-12-21 12:41:09' ),
( '1731', '2018-12-21 13:12:12', '10', '22', '2018-12-21 13:12:12' ),
( '1995', '2019-01-30 17:39:51', '5', '17', '2019-01-30 17:39:51' ),
( '1996', '2019-01-30 17:42:32', '5', '17', '2019-01-30 17:42:32' ),
( '1997', '2019-01-30 17:52:56', '5', '17', '2019-01-30 17:52:56' ),
( '1998', '2019-01-30 18:18:50', '5', '17', '2019-01-30 18:18:50' ),
( '1999', '2019-01-30 18:31:37', '5', '17', '2019-01-30 18:31:37' );
-- ---------------------------------------------------------


-- Dump data of "enemy_district_possible" ------------------
INSERT INTO `enemy_district_possible`(`id`,`character_level_id`,`town_type_id`,`min_town_level`,`created_at`,`chance`) VALUES 
( '1', '68', '1', '1', '2018-12-15 19:34:54', '100' ),
( '2', '69', '1', '1', '2018-12-15 19:34:54', '70' ),
( '3', '70', '1', '1', '2018-12-15 19:34:54', '30' );
-- ---------------------------------------------------------


-- Dump data of "enemy_district_teams" ---------------------
INSERT INTO `enemy_district_teams`(`id`,`belonging`,`number_of`,`preview`,`town_id`,`created_at`) VALUES 
( '1039', '1', '6', '/character/goblin/thumbnail/goblin.jpg', '7', '2018-12-19 21:26:20' ),
( '1130', '1', '3', '/character/goblin/thumbnail/goblin.jpg', '5', '2019-01-30 18:31:37' );
-- ---------------------------------------------------------


-- Dump data of "items" ------------------------------------
INSERT INTO `items`(`id`,`slug`,`name`,`discription`,`img`,`type_id`,`created_at`,`data`) VALUES 
( '1', 'i-char-warriorI', 'Карта Воина I', 'При помощи этой карты мы можете призвать к себе на слубжу "Воин I"', 'items-card1', '1', '2018-12-06 14:06:43', 'warriorI' ),
( '2', 'i-exp-scroll-50', 'I Свиток опыта', 'В этом спитке заключен опыт других солдат, можно передать его другому воину. В свитке заключенно 50 единиц опыта', 'items-exp-scroll-1', '3', '2018-12-07 19:14:45', '50' ),
( '3', 'i-exp-scroll-100', 'II Свиток опыта', 'В этом спитке заключен опыт других солдат, можно передать его другому воину. В свитке заключенно 50 единиц опыта', 'items-exp-scroll-2', '3', '2018-12-07 19:14:45', '100' ),
( '4', 'i-crystal-red-1', 'I Красный кристал', 'Простой кристал, пока не понятно для чего он нужен', 'items-crystal-red1', '5', '2018-12-07 19:16:39', '0' ),
( '5', 'i-crystal-red-2', 'II Красный кристал', 'Простой кристал, пока не понятно для чего он нужен', 'items-crystal-red2', '5', '2018-12-07 19:16:39', '0' ),
( '6', 'i-chest_old', 'Старый сундук', 'Открой что бы узнать что внутри!', 'items-chest1', '2', '2018-12-07 21:03:32', NULL ),
( '7', 'i-playoni-shard-1', 'I Осколоки плейоны', 'В этом осколке заключенна древняя магия', 'items-playoni-shard-1', '8', '2018-12-08 11:06:46', NULL ),
( '8', 'i-playoni-shard-2', 'II Осколоки плейоны', 'В этом осколке заключенна древняя магия', 'items-playoni-shard-2', '8', '2018-12-08 11:06:46', NULL ),
( '9', 'i-shere_shard_eather', 'Осколок земли', 'В этом осколке заключенна сила стихи земли', 'items-shere_shard_eather', '9', '2018-12-08 15:21:03', NULL ),
( '10', 'i-shere_shard_wather', 'Осколок воды', 'В этом осколке заключенна сила стихи воды', 'items-shere_shard_wather', '9', '2018-12-08 15:21:03', NULL ),
( '11', 'i-shere_shard_light', 'Осколок света', 'В этом осколке заключенный небесный свет', 'items-shere_shard_light', '9', '2018-12-08 15:21:03', NULL ),
( '12', 'i-shere_shard_dark', 'Осколок темноты', 'В этом осколке заключенна сама темнота', 'items-shere_shard_dark', '9', '2018-12-08 15:21:03', NULL ),
( '13', 'i-shere_shard_fire', 'Осколок огня', 'В этом осколке заключенна сила стихи огня', 'items-shere_shard_fire', '9', '2018-12-08 15:21:03', NULL ),
( '14', 'i-city-old-fortess', 'Сфера "Старая крепость"', 'В сфере заключенный город "Старая крепость". <br> Раса - <span class="fiol">Люди</span>', 'items-old-fortess', '7', '2018-12-08 15:37:13', 'old_fortess' ),
( '15', 'i-city-elf-city', 'Сфера "Ельфийский поселок"', 'В сфере заключенный город "Ельфийский город". <br> Раса - <span class="fiol">Ельфы</span>', 'items-elf-city', '7', '2018-12-08 15:37:13', 'elf-city' ),
( '16', 'i-city-orc-settlement', 'Сфера "Поселение орков"', 'В сфере заключенный город "Поселение орков". <br> Раса - <span class="fiol">Орки</span>', 'items-orc-settlement', '7', '2018-12-08 15:37:13', 'orc-settlement' ),
( '17', 'i-playoni-shard-3', 'III Осколоки плейоны', 'В этом осколке заключенна древняя магия', 'items-playoni-shard-3', '8', '2019-01-15 11:45:43', NULL ),
( '18', 'i-playoni-shard-4', 'IV Осколоки плейоны', 'В этом осколке заключенна древняя магия', 'items-playoni-shard-4', '8', '2019-01-15 11:45:43', NULL ),
( '19', 'i-playoni-shard-5', 'V Осколоки плейоны', 'В этом осколке заключенна древняя магия', 'items-playoni-shard-5', '8', '2019-01-15 11:45:43', NULL );
-- ---------------------------------------------------------


-- Dump data of "items_character_cards" --------------------
-- ---------------------------------------------------------


-- Dump data of "items_chests_items" -----------------------
INSERT INTO `items_chests_items`(`id`,`item_parent_id`,`item_id`,`chance`,`created_at`) VALUES 
( '1', '6', '1', '50', '2018-12-07 21:03:32' ),
( '2', '6', '4', '25', '2018-12-07 21:03:32' ),
( '3', '6', '2', '15', '2018-12-07 21:03:32' ),
( '4', '6', '3', '5', '2018-12-07 21:03:32' ),
( '5', '6', '14', '2', '2018-12-12 13:40:55' ),
( '6', '6', '15', '2', '2018-12-12 13:40:55' ),
( '7', '6', '16', '2', '2018-12-12 13:40:55' );
-- ---------------------------------------------------------


-- Dump data of "items_types" ------------------------------
INSERT INTO `items_types`(`id`,`slug`,`name`,`discription`,`created_at`) VALUES 
( '1', 'character-card', 'Карточка персонажа', 'При помощи карточки можно призвать персонажа', '2018-12-06 14:02:15' ),
( '2', 'chest', 'Сундук', '', '2018-12-07 18:57:01' ),
( '3', 'exp-scroll', 'Свиток опыта', 'Пока нахер не нужен, но в скоре времени будут нужны', '2018-12-07 18:57:01' ),
( '4', 'exp-collector', 'Накопитель опыта', 'Пока нахер не нужен, но в скоре времени будут нужны', '2018-12-07 18:57:01' ),
( '5', 'crystal', 'Магический кристал', 'Пока нахер не нужен, но в скоре времени будут нужны', '2018-12-07 18:57:01' ),
( '6', 'rings', 'Кольцо', 'Пока нахер не нужен, но в скоре времени будут нужны', '2018-12-07 18:57:01' ),
( '7', 'city_sphere', 'Сфера города', 'В етой сфере заключенный древний город', '2018-12-07 18:57:01' ),
( '8', 'material', 'Материал для ремесла', 'Пока нахер не нужен, но в скоре времени будут нужны', '2018-12-08 10:57:44' ),
( '9', 'sphere_shard', 'Осколок сферы', 'С етих осколков можно создать сферу города', '2018-12-08 15:07:09' );
-- ---------------------------------------------------------


-- Dump data of "migrations" -------------------------------
INSERT INTO `migrations`(`id`,`name`,`run_on`) VALUES 
( '1', '/20181017135819-users', '2018-10-24 11:20:32' ),
( '2', '/20181018083237-types-character', '2018-10-24 11:20:32' ),
( '3', '/20181018083248-characters-groups', '2018-10-24 11:20:32' );
-- ---------------------------------------------------------


-- Dump data of "mines" ------------------------------------
INSERT INTO `mines`(`id`,`level`,`gold_per_hour`,`price`,`created_at`) VALUES 
( '1', '0', '0', '0', '2018-10-19 11:59:46' ),
( '2', '1', '15', '20', '2018-10-19 11:59:46' ),
( '3', '2', '30', '150', '2018-10-19 11:59:46' ),
( '4', '3', '60', '300', '2018-10-19 11:59:46' ),
( '5', '4', '120', '600', '2018-10-19 11:59:46' ),
( '6', '5', '300', '1000', '2018-10-19 11:59:46' ),
( '7', '6', '600', '3000', '2018-10-19 11:59:46' ),
( '8', '7', '1500', '8000', '2018-10-19 11:59:46' ),
( '9', '8', '3000', '30000', '2018-10-19 11:59:46' ),
( '10', '9', '10000', '200000', '2018-10-19 11:59:46' ),
( '11', '10', '25000', '500000', '2018-10-19 11:59:46' );
-- ---------------------------------------------------------


-- Dump data of "register_characters" ----------------------
INSERT INTO `register_characters`(`id`,`character_level_id`,`created_at`) VALUES 
( '1', '53', '2018-12-19 22:12:24' ),
( '2', '63', '2018-12-19 22:12:24' ),
( '3', '74', '2018-12-20 18:18:03' ),
( '4', '79', '2018-12-20 18:44:08' );
-- ---------------------------------------------------------


-- Dump data of "skills" -----------------------------------
INSERT INTO `skills`(`id`,`name`,`slug`,`skill_type_id`,`data`,`chance`,`turns_count`,`created_at`,`mana_cost`,`group_id`,`buff_id`) VALUES 
( '1', 'Двойной выстрел', 'double_shot', '1', '4', '0', '0', '2018-12-09 19:36:25', '3', '1', '0' ),
( '2', 'Усиленный выстрел', 'reinforced_shot', '1', '{"critical_hit_chance":25,"attack":2}', '0', '0', '2018-12-11 13:47:49', '3', '2', '0' ),
( '4', 'Защитная стойка', 'protective_rack', '1', '{"magic_armor":2,"physical_armor":2}', '0', '2', '2018-12-12 11:36:34', '4', '4', '1' ),
( '5', 'Амун предвотель', 'amun_comanderI', '2', '{"evasion":10}', '0', '0', '2018-12-13 11:36:37', '0', '5', '1' ),
( '6', 'Амун предвотель', 'amun_comanderII', '2', '{"evasion":10}', '0', '0', '2018-12-13 11:36:37', '0', '5', '1' ),
( '7', 'Отравленный удар', 'poisonI', '1', '1', '0', '2', '2018-12-13 11:36:37', '2', '6', '2' ),
( '8', 'Скрытность', 'stealth', '1', '{"evasion":10,"attack":1}', '0', '3', '2018-12-13 11:36:37', '5', '4', NULL ),
( '9', 'Сехит предвотель', 'sehit_comanderI', '2', '{"physical_armor":1,"critical_hit_chance":5}', '0', '0', '2018-12-13 22:57:03', '0', '5', NULL );
-- ---------------------------------------------------------


-- Dump data of "skills_buffs" -----------------------------
INSERT INTO `skills_buffs`(`id`,`name`,`slug`,`data`,`created_at`) VALUES 
( '1', 'Базовое усиление', 'basic_magnification', '', '2018-12-12 11:31:31' ),
( '2', 'Яд', 'poison', '', '2018-12-13 11:26:11' );
-- ---------------------------------------------------------


-- Dump data of "skills_groups" ----------------------------
INSERT INTO `skills_groups`(`id`,`slug`,`created_at`,`name`) VALUES 
( '1', 'few_blows', '2018-12-11 13:25:59', 'Несколько ударов' ),
( '2', 'enhanced_blow', '2018-12-11 13:25:59', 'Усиленный удар' ),
( '3', 'stun', '2018-12-11 13:25:59', 'Оглушение' ),
( '4', 'magnification_yourself', '2018-12-12 11:34:54', 'Усиление себя' ),
( '5', 'pas_magnification_rase', '2018-12-13 11:34:03', 'Пасивное усиление расы' ),
( '6', 'negative_effect', '2018-12-21 15:26:02', 'Накладиваные негативного ефекта' );
-- ---------------------------------------------------------


-- Dump data of "skills_types" -----------------------------
INSERT INTO `skills_types`(`id`,`name`,`slug`,`created_at`) VALUES 
( '1', 'Активный', 'active', '2018-12-09 19:26:59' ),
( '2', 'Пасивный одиночный', 'passive_single', '2018-12-09 19:26:59' ),
( '3', 'Пасивный командный', 'passive_multiple', '2018-12-09 19:26:59' );
-- ---------------------------------------------------------


-- Dump data of "towns" ------------------------------------
INSERT INTO `towns`(`id`,`level`,`user_id`,`mine_id`,`barrack_id`,`type_towns_id`,`created_at`) VALUES 
( '1', NULL, NULL, NULL, NULL, NULL, '2018-10-18 16:18:47' ),
( '4', NULL, '14', '1', '1', '1', '2018-10-19 12:45:21' ),
( '5', NULL, '17', '1', '2', '1', '2018-10-19 12:48:29' ),
( '6', NULL, '18', '1', '1', '1', '2018-12-05 11:17:53' ),
( '7', NULL, '19', '1', '1', '1', '2018-12-09 00:33:25' ),
( '8', NULL, '20', '1', '1', '1', '2018-12-16 20:41:11' ),
( '9', NULL, '21', '1', '1', '1', '2018-12-21 11:42:55' ),
( '10', NULL, '22', '1', '1', '1', '2018-12-21 11:48:14' ),
( '11', NULL, '23', '1', '1', '1', '2018-12-21 11:50:15' );
-- ---------------------------------------------------------


-- Dump data of "towns_drops_possible" ---------------------
INSERT INTO `towns_drops_possible`(`id`,`chance`,`max_count`,`town_type_id`,`item_id`,`created_at`) VALUES 
( '1', '75', '10', '1', '7', '2018-12-08 11:07:08' ),
( '2', '35', '2', '1', '8', '2018-12-08 11:07:08' ),
( '3', '50', '5', '1', '6', '2018-12-08 11:07:08' );
-- ---------------------------------------------------------


-- Dump data of "type_characters" --------------------------
INSERT INTO `type_characters`(`id`,`name`,`slug`,`created_at`) VALUES 
( '1', 'Ближний бой', 'melee', '2018-10-19 12:00:03' ),
( '2', 'Дальний - стрелковый', 'distance_rifle', '2018-10-19 12:00:03' ),
( '3', 'Дальний - магический', 'distance_magic', '2018-10-19 12:00:03' ),
( '4', 'Лечение', 'medication', '2018-12-09 15:23:50' );
-- ---------------------------------------------------------


-- Dump data of "type_towns" -------------------------------
INSERT INTO `type_towns`(`id`,`name`,`slug`,`discription`,`rarity`,`image`,`chance_find`,`gold_bust`,`created_at`) VALUES 
( '1', 'Поселок', 'village', NULL, 'Обычный', '/town/village.jpg', '1000', '0', '2018-10-19 11:59:46' ),
( '5', 'Старая крепость', 'old_fortess', '', 'Обычный', '/town/old_fortess.jpg', '0', '10', '2018-12-08 17:37:17' ),
( '6', 'Ельфийский город', 'elf-city', '', 'Обычный', '/town/elf-city.jpg', '0', '10', '2018-12-08 17:56:47' ),
( '7', 'Поселение орков', 'orc-settlement', '', 'Обычный', '/town/orc-settlement.jpg', '0', '10', '2018-12-08 18:27:45' );
-- ---------------------------------------------------------


-- Dump data of "users" ------------------------------------
INSERT INTO `users`(`id`,`name`,`login`,`email`,`gold`,`password`,`rating`,`active`,`created_at`) VALUES 
( '1', 'bitalli', 'admin', 'admin', '900', '21232f297a57a5a743894a0e4a801fc3', '0', '0', '2018-10-18 14:59:17' ),
( '14', 'asd', 'admin21', 'admin2@mail.ru', '900', '21232f297a57a5a743894a0e4a801fc3', '0', NULL, '2018-10-19 12:45:21' ),
( '17', 'asdsad', 'admin2', '1admin2@mail.ru', '900', '21232f297a57a5a743894a0e4a801fc3', '-52', NULL, '2018-10-19 12:48:29' ),
( '18', 'admin', 'admin1', 'admin@mail.ru', NULL, '21232f297a57a5a743894a0e4a801fc3', '0', NULL, '2018-12-05 11:17:53' ),
( '19', 'Настя', 'vknasty', 'vknaty07@yandex.ru', NULL, '21232f297a57a5a743894a0e4a801fc3', '74', NULL, '2018-12-09 00:33:25' ),
( '20', 'кутасик', 'xalk', 'xalk10@mail.ry', NULL, '21232f297a57a5a743894a0e4a801fc3', '0', NULL, '2018-12-16 20:41:11' ),
( '21', 'Vitalik', 'admin3', 'admin3@gmail.com', NULL, '32cacb2f994f6b42183a1300d9a3e8d6', '0', NULL, '2018-12-21 11:42:55' ),
( '22', 'Vitalik2', 'admin4', 'admin4@gmail.com', NULL, '32cacb2f994f6b42183a1300d9a3e8d6', '0', NULL, '2018-12-21 11:48:14' ),
( '23', 'Vitalik2', 'admin5', 'admin5@gmail.com', NULL, '32cacb2f994f6b42183a1300d9a3e8d6', '-22', NULL, '2018-12-21 11:50:15' );
-- ---------------------------------------------------------


-- Dump data of "users_campains" ---------------------------
INSERT INTO `users_campains`(`id`,`user_id`,`campain_id`,`progress`,`created_at`) VALUES 
( '1', '17', '1', '0', '2018-12-15 21:15:26' ),
( '3', '20', '1', '0', '2018-12-16 20:48:50' ),
( '4', '22', '1', '0', '2018-12-21 11:50:15' );
-- ---------------------------------------------------------


-- Dump data of "users_campains_levels" --------------------
INSERT INTO `users_campains_levels`(`id`,`campain_level_id`,`user_campain_id`,`is_complete_1`,`is_complete_2`,`is_complete_3`,`created_at`) VALUES 
( '1', '2', '1', '1', '1', '1', '2018-12-16 00:56:13' ),
( '2', '1', '1', '1', '1', '1', '2018-12-16 11:20:28' ),
( '3', '3', '1', '1', '1', '0', '2018-12-16 20:38:15' ),
( '4', '4', '1', '1', '1', '1', '2018-12-16 20:38:24' ),
( '5', '5', '1', '1', '1', '1', '2018-12-16 20:39:15' ),
( '6', '1', '3', '1', '1', '1', '2018-12-16 21:13:57' ),
( '7', '2', '3', '1', '1', '0', '2018-12-16 21:27:44' ),
( '8', '3', '3', '1', '1', '0', '2018-12-16 21:29:31' ),
( '9', '4', '3', '1', '1', '0', '2018-12-16 21:29:53' ),
( '10', '5', '3', '1', '1', '0', '2018-12-16 21:47:09' ),
( '11', '1', '4', '1', '1', '0', '2018-12-21 11:50:41' ),
( '12', '2', '4', '1', '1', '0', '2018-12-21 11:51:04' ),
( '13', '3', '4', '0', '0', '0', '2018-12-21 11:53:37' );
-- ---------------------------------------------------------


-- Dump data of "users_crafts" -----------------------------
INSERT INTO `users_crafts`(`id`,`user_id`,`level`,`created_at`) VALUES 
( '1', '17', '1', '2019-01-15 13:43:08' );
-- ---------------------------------------------------------


-- Dump data of "users_items" ------------------------------
INSERT INTO `users_items`(`id`,`item_id`,`user_id`,`count`,`created_at`) VALUES 
( '3', '4', '17', '81', '2018-12-07 23:39:10' ),
( '6', '1', '17', '210', '2018-12-08 00:22:00' ),
( '11', '3', '17', '16', '2018-12-08 00:32:21' ),
( '12', '7', '17', '539', '2018-12-08 11:52:18' ),
( '13', '8', '17', '2', '2018-12-08 11:52:18' ),
( '14', '9', '17', '10', '2018-12-08 15:21:36' ),
( '19', '2', '17', '49', '2018-12-08 20:58:29' ),
( '25', '7', '19', '6', '2018-12-09 00:52:46' ),
( '26', '8', '19', '1', '2018-12-09 00:52:46' ),
( '28', '2', '19', '1', '2018-12-09 00:53:09' ),
( '34', '14', '17', '5', '2018-12-12 13:41:29' ),
( '35', '16', '17', '5', '2018-12-12 13:41:38' ),
( '36', '15', '17', '8', '2018-12-12 13:41:47' ),
( '39', '7', '20', '18', '2018-12-16 21:09:44' ),
( '41', '2', '20', '13', '2018-12-16 21:27:54' ),
( '42', '1', '20', '8', '2018-12-16 21:28:04' ),
( '43', '15', '20', '1', '2018-12-16 21:28:04' ),
( '44', '4', '20', '3', '2018-12-16 21:28:14' ),
( '50', '8', '20', '2', '2018-12-16 21:48:44' ),
( '51', '16', '20', '1', '2018-12-16 21:52:25' ),
( '53', '6', '20', '2', '2018-12-18 23:27:15' ),
( '54', '7', '23', '2', '2018-12-21 11:50:45' ),
( '56', '2', '23', '1', '2018-12-21 11:52:18' ),
( '58', '18', '17', '1', '2019-01-30 16:36:41' ),
( '59', '17', '17', '7', '2019-01-30 17:01:01' ),
( '60', '19', '17', '1', '2019-01-30 18:12:20' );
-- ---------------------------------------------------------


-- Dump data of "users_visits" -----------------------------
-- ---------------------------------------------------------


-- Dump data of "visits_rewards" ---------------------------
INSERT INTO `visits_rewards`(`id`,`day`,`reward_gold`,`item_id`,`item_count`,`created_at`) VALUES 
( '1', '1', '10', NULL, NULL, '2019-01-23 17:22:58' ),
( '2', '2', NULL, '2', '1', '2019-01-23 17:22:58' ),
( '3', '3', NULL, '6', '1', '2019-01-23 17:22:58' ),
( '4', '4', NULL, '9', '1', '2019-01-23 17:22:58' ),
( '5', '5', '100', NULL, NULL, '2019-01-23 17:22:58' ),
( '6', '6', NULL, '6', '3', '2019-01-23 17:22:58' ),
( '7', '7', NULL, '9', '3', '2019-01-23 17:22:58' ),
( '8', '8', NULL, '10', '1', '2019-01-23 17:22:58' ),
( '9', '9', NULL, '2', '5', '2019-01-23 17:22:58' ),
( '10', '10', NULL, '3', '5', '2019-01-23 17:22:58' );
-- ---------------------------------------------------------


-- CREATE INDEX "arena_log_user1_f" ----------------------------
CREATE INDEX `arena_log_user1_f` USING BTREE ON `arena_log`( `user_win_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "arena_log_user2_f" ----------------------------
CREATE INDEX `arena_log_user2_f` USING BTREE ON `arena_log`( `user_loss_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "character_avaliable_character_f" --------------
CREATE INDEX `character_avaliable_character_f` USING BTREE ON `barrack_available_character`( `character_level_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "towns_avaliable_character_f" ------------------
CREATE INDEX `towns_avaliable_character_f` USING BTREE ON `barrack_available_character`( `town_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "towns_barrack_intervals_f" --------------------
CREATE INDEX `towns_barrack_intervals_f` USING BTREE ON `barrack_intervals`( `town_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "barrack_level_possible_character_barrack_f" ---
CREATE INDEX `barrack_level_possible_character_barrack_f` USING BTREE ON `barrack_possible_character`( `barrack_level` );
-- -------------------------------------------------------------


-- CREATE INDEX "character_possible_barrack_character_f" -------
CREATE INDEX `character_possible_barrack_character_f` USING BTREE ON `barrack_possible_character`( `character_level_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "towns_possible_character_barrack_f" -----------
CREATE INDEX `towns_possible_character_barrack_f` USING BTREE ON `barrack_possible_character`( `town_type_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "battle_character_battle_characters_buffs_f" ---
CREATE INDEX `battle_character_battle_characters_buffs_f` USING BTREE ON `battle_characters_buffs`( `battle_character_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "skills_buffs_battle_characters_buffs_f" -------
CREATE INDEX `skills_buffs_battle_characters_buffs_f` USING BTREE ON `battle_characters_buffs`( `skill_buff_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "towns_type_campains_f" ------------------------
CREATE INDEX `towns_type_campains_f` USING BTREE ON `campains`( `town_type_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "campains_levels_campain_f" --------------------
CREATE INDEX `campains_levels_campain_f` USING BTREE ON `campains_levels`( `campain_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "campains_levels_campains_enemy_f" -------------
CREATE INDEX `campains_levels_campains_enemy_f` USING BTREE ON `campains_levels_enemy`( `campain_level_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "character_levels_campains_enemy_f" ------------
CREATE INDEX `character_levels_campains_enemy_f` USING BTREE ON `campains_levels_enemy`( `character_level_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "campains_levels_campains_rewards_f" -----------
CREATE INDEX `campains_levels_campains_rewards_f` USING BTREE ON `campains_levels_rewards`( `campain_level_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "items_campains_rewards_f" ---------------------
CREATE INDEX `items_campains_rewards_f` USING BTREE ON `campains_levels_rewards`( `item_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "group_character_levels_f" ---------------------
CREATE INDEX `group_character_levels_f` USING BTREE ON `character_levels`( `group_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "type_character_level_f" -----------------------
CREATE INDEX `type_character_level_f` USING BTREE ON `character_levels`( `type_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "character_levels_characters_f" ----------------
CREATE INDEX `character_levels_characters_f` USING BTREE ON `characters`( `character_level_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "user_characters_f" ----------------------------
CREATE INDEX `user_characters_f` USING BTREE ON `characters`( `user_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "characters_characters_levels_skills_f" --------
CREATE INDEX `characters_characters_levels_skills_f` USING BTREE ON `characters_levels_skills`( `character_level_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "skill_characters_levels_skills_f" -------------
CREATE INDEX `skill_characters_levels_skills_f` USING BTREE ON `characters_levels_skills`( `skill_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "craft_recipes_craft_recipes_type_f" -----------
CREATE INDEX `craft_recipes_craft_recipes_type_f` USING BTREE ON `craft_recipes`( `craft_recipe_type` );
-- -------------------------------------------------------------


-- CREATE INDEX "item_result_craft_recipes_items_f" ------------
CREATE INDEX `item_result_craft_recipes_items_f` USING BTREE ON `craft_recipes`( `item_result` );
-- -------------------------------------------------------------


-- CREATE INDEX "craft_recipes_items_f" ------------------------
CREATE INDEX `craft_recipes_items_f` USING BTREE ON `craft_recipes_items`( `craft_recipe_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "items_craft_recipes_items_f" ------------------
CREATE INDEX `items_craft_recipes_items_f` USING BTREE ON `craft_recipes_items`( `item_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "character_enemy_district_character_f" ---------
CREATE INDEX `character_enemy_district_character_f` USING BTREE ON `enemy_district_character`( `character_level_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "enemy_team_enemy_district_character_f" --------
CREATE INDEX `enemy_team_enemy_district_character_f` USING BTREE ON `enemy_district_character`( `enemy_team_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "enemy_district_interval_user_f" ---------------
CREATE INDEX `enemy_district_interval_user_f` USING BTREE ON `enemy_district_interval`( `user_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "character_district_possible_enemy_f" ----------
CREATE INDEX `character_district_possible_enemy_f` USING BTREE ON `enemy_district_possible`( `character_level_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "town_type_district_possible_enemy_f" ----------
CREATE INDEX `town_type_district_possible_enemy_f` USING BTREE ON `enemy_district_possible`( `town_type_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "towns_enemy_district_teams_f" -----------------
CREATE INDEX `towns_enemy_district_teams_f` USING BTREE ON `enemy_district_teams`( `town_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "items_types_items_f" --------------------------
CREATE INDEX `items_types_items_f` USING BTREE ON `items`( `type_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "items_character_cards_items_f" ----------------
CREATE INDEX `items_character_cards_items_f` USING BTREE ON `items_character_cards`( `item_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "items_character_cards_levels_characters_f" ----
CREATE INDEX `items_character_cards_levels_characters_f` USING BTREE ON `items_character_cards`( `character_level_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "items_chests_items_item_f" --------------------
CREATE INDEX `items_chests_items_item_f` USING BTREE ON `items_chests_items`( `item_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "items_chests_items_item_parent_f" -------------
CREATE INDEX `items_chests_items_item_parent_f` USING BTREE ON `items_chests_items`( `item_parent_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "register_characters_levels_characters_f" ------
CREATE INDEX `register_characters_levels_characters_f` USING BTREE ON `register_characters`( `character_level_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "skills_types_skills_f" ------------------------
CREATE INDEX `skills_types_skills_f` USING BTREE ON `skills`( `skill_type_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "towns_barracks_f" -----------------------------
CREATE INDEX `towns_barracks_f` USING BTREE ON `towns`( `barrack_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "towns_characters_f" ---------------------------
CREATE INDEX `towns_characters_f` USING BTREE ON `towns`( `user_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "towns_mine_f" ---------------------------------
CREATE INDEX `towns_mine_f` USING BTREE ON `towns`( `mine_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "towns_type_towns_f" ---------------------------
CREATE INDEX `towns_type_towns_f` USING BTREE ON `towns`( `type_towns_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "item_towns_drop_possible_f" -------------------
CREATE INDEX `item_towns_drop_possible_f` USING BTREE ON `towns_drops_possible`( `item_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "town_type_drops_possible_f" -------------------
CREATE INDEX `town_type_drops_possible_f` USING BTREE ON `towns_drops_possible`( `town_type_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "users_campains_campain_f" ---------------------
CREATE INDEX `users_campains_campain_f` USING BTREE ON `users_campains`( `campain_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "users_campains_user_f" ------------------------
CREATE INDEX `users_campains_user_f` USING BTREE ON `users_campains`( `user_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "campains_levels_users_campains_levels_f" ------
CREATE INDEX `campains_levels_users_campains_levels_f` USING BTREE ON `users_campains_levels`( `campain_level_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "users_campains_users_campains_levels_f" -------
CREATE INDEX `users_campains_users_campains_levels_f` USING BTREE ON `users_campains_levels`( `user_campain_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "user_users_items_f" ---------------------------
CREATE INDEX `user_users_items_f` USING BTREE ON `users_items`( `user_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "users_users_visits_f" -------------------------
CREATE INDEX `users_users_visits_f` USING BTREE ON `users_visits`( `user_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "items_visits_rewards_f" -----------------------
CREATE INDEX `items_visits_rewards_f` USING BTREE ON `visits_rewards`( `item_id` );
-- -------------------------------------------------------------


-- CREATE LINK "character_avaliable_character_f" ---------------
ALTER TABLE `barrack_available_character`
	ADD CONSTRAINT `character_avaliable_character_f` FOREIGN KEY ( `character_level_id` )
	REFERENCES `character_levels`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;
-- -------------------------------------------------------------


-- CREATE LINK "towns_avaliable_character_f" -------------------
ALTER TABLE `barrack_available_character`
	ADD CONSTRAINT `towns_avaliable_character_f` FOREIGN KEY ( `town_id` )
	REFERENCES `towns`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;
-- -------------------------------------------------------------


-- CREATE LINK "towns_barrack_intervals_f" ---------------------
ALTER TABLE `barrack_intervals`
	ADD CONSTRAINT `towns_barrack_intervals_f` FOREIGN KEY ( `town_id` )
	REFERENCES `towns`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "barrack_level_possible_character_barrack_f" ----
ALTER TABLE `barrack_possible_character`
	ADD CONSTRAINT `barrack_level_possible_character_barrack_f` FOREIGN KEY ( `barrack_level` )
	REFERENCES `barracks`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "character_possible_barrack_character_f" --------
ALTER TABLE `barrack_possible_character`
	ADD CONSTRAINT `character_possible_barrack_character_f` FOREIGN KEY ( `character_level_id` )
	REFERENCES `character_levels`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;
-- -------------------------------------------------------------


-- CREATE LINK "towns_possible_character_barrack_f" ------------
ALTER TABLE `barrack_possible_character`
	ADD CONSTRAINT `towns_possible_character_barrack_f` FOREIGN KEY ( `town_type_id` )
	REFERENCES `type_towns`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "battle_character_battle_characters_buffs_f" ----
ALTER TABLE `battle_characters_buffs`
	ADD CONSTRAINT `battle_character_battle_characters_buffs_f` FOREIGN KEY ( `battle_character_id` )
	REFERENCES `battle_characters`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "skills_buffs_battle_characters_buffs_f" --------
ALTER TABLE `battle_characters_buffs`
	ADD CONSTRAINT `skills_buffs_battle_characters_buffs_f` FOREIGN KEY ( `skill_buff_id` )
	REFERENCES `skills_buffs`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "towns_type_campains_f" -------------------------
ALTER TABLE `campains`
	ADD CONSTRAINT `towns_type_campains_f` FOREIGN KEY ( `town_type_id` )
	REFERENCES `type_towns`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "campains_levels_campain_f" ---------------------
ALTER TABLE `campains_levels`
	ADD CONSTRAINT `campains_levels_campain_f` FOREIGN KEY ( `campain_id` )
	REFERENCES `users`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "campains_levels_campains_enemy_f" --------------
ALTER TABLE `campains_levels_enemy`
	ADD CONSTRAINT `campains_levels_campains_enemy_f` FOREIGN KEY ( `campain_level_id` )
	REFERENCES `campains_levels`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "character_levels_campains_enemy_f" -------------
ALTER TABLE `campains_levels_enemy`
	ADD CONSTRAINT `character_levels_campains_enemy_f` FOREIGN KEY ( `character_level_id` )
	REFERENCES `character_levels`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "campains_levels_campains_rewards_f" ------------
ALTER TABLE `campains_levels_rewards`
	ADD CONSTRAINT `campains_levels_campains_rewards_f` FOREIGN KEY ( `campain_level_id` )
	REFERENCES `campains_levels`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "items_campains_rewards_f" ----------------------
ALTER TABLE `campains_levels_rewards`
	ADD CONSTRAINT `items_campains_rewards_f` FOREIGN KEY ( `item_id` )
	REFERENCES `items`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "group_character_levels_f" ----------------------
ALTER TABLE `character_levels`
	ADD CONSTRAINT `group_character_levels_f` FOREIGN KEY ( `group_id` )
	REFERENCES `characters_groups`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;
-- -------------------------------------------------------------


-- CREATE LINK "type_character_level_f" ------------------------
ALTER TABLE `character_levels`
	ADD CONSTRAINT `type_character_level_f` FOREIGN KEY ( `type_id` )
	REFERENCES `type_characters`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;
-- -------------------------------------------------------------


-- CREATE LINK "character_levels_characters_f" -----------------
ALTER TABLE `characters`
	ADD CONSTRAINT `character_levels_characters_f` FOREIGN KEY ( `character_level_id` )
	REFERENCES `character_levels`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "user_characters_f" -----------------------------
ALTER TABLE `characters`
	ADD CONSTRAINT `user_characters_f` FOREIGN KEY ( `user_id` )
	REFERENCES `users`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "characters_characters_levels_skills_f" ---------
ALTER TABLE `characters_levels_skills`
	ADD CONSTRAINT `characters_characters_levels_skills_f` FOREIGN KEY ( `character_level_id` )
	REFERENCES `character_levels`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "skill_characters_levels_skills_f" --------------
ALTER TABLE `characters_levels_skills`
	ADD CONSTRAINT `skill_characters_levels_skills_f` FOREIGN KEY ( `skill_id` )
	REFERENCES `skills`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "craft_recipes_craft_recipes_type_f" ------------
ALTER TABLE `craft_recipes`
	ADD CONSTRAINT `craft_recipes_craft_recipes_type_f` FOREIGN KEY ( `craft_recipe_type` )
	REFERENCES `craft_recipes_types`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "craft_recipes_items_f" -------------------------
ALTER TABLE `craft_recipes_items`
	ADD CONSTRAINT `craft_recipes_items_f` FOREIGN KEY ( `craft_recipe_id` )
	REFERENCES `craft_recipes`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "items_craft_recipes_items_f" -------------------
ALTER TABLE `craft_recipes_items`
	ADD CONSTRAINT `items_craft_recipes_items_f` FOREIGN KEY ( `item_id` )
	REFERENCES `items`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "character_enemy_district_character_f" ----------
ALTER TABLE `enemy_district_character`
	ADD CONSTRAINT `character_enemy_district_character_f` FOREIGN KEY ( `character_level_id` )
	REFERENCES `character_levels`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "enemy_team_enemy_district_character_f" ---------
ALTER TABLE `enemy_district_character`
	ADD CONSTRAINT `enemy_team_enemy_district_character_f` FOREIGN KEY ( `enemy_team_id` )
	REFERENCES `enemy_district_teams`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "enemy_district_interval_user_f" ----------------
ALTER TABLE `enemy_district_interval`
	ADD CONSTRAINT `enemy_district_interval_user_f` FOREIGN KEY ( `user_id` )
	REFERENCES `users`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "character_district_possible_enemy_f" -----------
ALTER TABLE `enemy_district_possible`
	ADD CONSTRAINT `character_district_possible_enemy_f` FOREIGN KEY ( `character_level_id` )
	REFERENCES `character_levels`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "town_type_district_possible_enemy_f" -----------
ALTER TABLE `enemy_district_possible`
	ADD CONSTRAINT `town_type_district_possible_enemy_f` FOREIGN KEY ( `town_type_id` )
	REFERENCES `type_towns`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "towns_enemy_district_teams_f" ------------------
ALTER TABLE `enemy_district_teams`
	ADD CONSTRAINT `towns_enemy_district_teams_f` FOREIGN KEY ( `town_id` )
	REFERENCES `towns`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "items_character_cards_items_f" -----------------
ALTER TABLE `items_character_cards`
	ADD CONSTRAINT `items_character_cards_items_f` FOREIGN KEY ( `item_id` )
	REFERENCES `items`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "items_character_cards_levels_characters_f" -----
ALTER TABLE `items_character_cards`
	ADD CONSTRAINT `items_character_cards_levels_characters_f` FOREIGN KEY ( `character_level_id` )
	REFERENCES `character_levels`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "items_chests_items_item_f" ---------------------
ALTER TABLE `items_chests_items`
	ADD CONSTRAINT `items_chests_items_item_f` FOREIGN KEY ( `item_id` )
	REFERENCES `items`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "items_chests_items_item_parent_f" --------------
ALTER TABLE `items_chests_items`
	ADD CONSTRAINT `items_chests_items_item_parent_f` FOREIGN KEY ( `item_parent_id` )
	REFERENCES `items`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "register_characters_levels_characters_f" -------
ALTER TABLE `register_characters`
	ADD CONSTRAINT `register_characters_levels_characters_f` FOREIGN KEY ( `character_level_id` )
	REFERENCES `character_levels`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "skills_types_skills_f" -------------------------
ALTER TABLE `skills`
	ADD CONSTRAINT `skills_types_skills_f` FOREIGN KEY ( `skill_type_id` )
	REFERENCES `skills_types`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "towns_barracks_f" ------------------------------
ALTER TABLE `towns`
	ADD CONSTRAINT `towns_barracks_f` FOREIGN KEY ( `barrack_id` )
	REFERENCES `barracks`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;
-- -------------------------------------------------------------


-- CREATE LINK "towns_characters_f" ----------------------------
ALTER TABLE `towns`
	ADD CONSTRAINT `towns_characters_f` FOREIGN KEY ( `user_id` )
	REFERENCES `users`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "towns_mine_f" ----------------------------------
ALTER TABLE `towns`
	ADD CONSTRAINT `towns_mine_f` FOREIGN KEY ( `mine_id` )
	REFERENCES `mines`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;
-- -------------------------------------------------------------


-- CREATE LINK "towns_type_towns_f" ----------------------------
ALTER TABLE `towns`
	ADD CONSTRAINT `towns_type_towns_f` FOREIGN KEY ( `type_towns_id` )
	REFERENCES `type_towns`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;
-- -------------------------------------------------------------


-- CREATE LINK "item_towns_drop_possible_f" --------------------
ALTER TABLE `towns_drops_possible`
	ADD CONSTRAINT `item_towns_drop_possible_f` FOREIGN KEY ( `item_id` )
	REFERENCES `items`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "town_type_drops_possible_f" --------------------
ALTER TABLE `towns_drops_possible`
	ADD CONSTRAINT `town_type_drops_possible_f` FOREIGN KEY ( `town_type_id` )
	REFERENCES `type_towns`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "users_campains_campain_f" ----------------------
ALTER TABLE `users_campains`
	ADD CONSTRAINT `users_campains_campain_f` FOREIGN KEY ( `campain_id` )
	REFERENCES `users`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "users_campains_user_f" -------------------------
ALTER TABLE `users_campains`
	ADD CONSTRAINT `users_campains_user_f` FOREIGN KEY ( `user_id` )
	REFERENCES `users`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "craft_recipes_user_id_f" -----------------------
ALTER TABLE `users_crafts`
	ADD CONSTRAINT `craft_recipes_user_id_f` FOREIGN KEY ( `user_id` )
	REFERENCES `users`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "user_users_items_f" ----------------------------
ALTER TABLE `users_items`
	ADD CONSTRAINT `user_users_items_f` FOREIGN KEY ( `user_id` )
	REFERENCES `users`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "users_users_visits_f" --------------------------
ALTER TABLE `users_visits`
	ADD CONSTRAINT `users_users_visits_f` FOREIGN KEY ( `user_id` )
	REFERENCES `users`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "items_visits_rewards_f" ------------------------
ALTER TABLE `visits_rewards`
	ADD CONSTRAINT `items_visits_rewards_f` FOREIGN KEY ( `item_id` )
	REFERENCES `items`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


