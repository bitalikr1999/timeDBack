const connector = require('../../../systems/connector')

const newBattle = async () => {
  return await connector.end(
    connector.insert().into('battles').toString() + ' VALUES ()'
  )

}

module.exports = {
  newBattle
}
