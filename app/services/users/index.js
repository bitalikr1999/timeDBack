const connector = require('../../systems/connector')
const logger = require('../../systems/logger')

const addGold = async (gold, userId) => {
  return await connector.end(
    connector.update()
    .table('users')
    .set(`gold = gold + ${gold}`)
    .where('id = ?', userId)
    .toString()
  )
}

const addRate = async (userId, rate) => {
  return await connector.end(
    connector.update()
    .table('users')
    .set(`rating = rating + ${rate}`)
    .where('id = ?', userId)
    .toString()
  )
}

const removeRate = async (userId, rate) => {
  const user = await connector.end(
    connector.select().from('users').field('rating').where('id = ?', userId).toString()
  )
  if (user.rating - rate < 1) rate = 0;
  return await connector.end(
    connector.update()
    .table('users')
    .set(`rating = rating - ${rate}`)
    .where('id = ?', userId)
    .toString()
  )
}

module.exports = {
  addGold,
  addRate,
  removeRate
}
