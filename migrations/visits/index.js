const connector = require('../../app/systems/connector')


connector.end(
  connector.insert().into('visits_rewards')
  .setFieldsRows([
    {
      reward_gold: 10,
      day: 1,
      item_count: null,
      item_id: null
    },
    {
      reward_gold: null,
      day: 2,
      item_count: 1,
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-exp-scroll-50')
    },
    {
      reward_gold: null,
      day: 3,
      item_count: 1,
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-chest_old')
    },
    {
      reward_gold: null,
      day: 4,
      item_count: 1,
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-shere_shard_eather')
    },
    {
      reward_gold: 100,
      day: 5,
      item_count: null,
      item_id: null
    },
    {
      reward_gold: null,
      day: 6,
      item_count: 3,
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-chest_old')
    },
    {
      reward_gold: null,
      day: 7,
      item_count: 3,
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-shere_shard_eather')
    },
    {
      reward_gold: null,
      day: 8,
      item_count: 1,
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-shere_shard_wather')
    },
    {
      reward_gold: null,
      day: 9,
      item_count: 5,
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-exp-scroll-50')
    },
    {
      reward_gold: null,
      day: 10,
      item_count: 5,
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-exp-scroll-100')
    },
  ]).toString()
)
