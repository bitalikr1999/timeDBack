const mysql = require('mysql')
const config = require('../config')
const squel = require('squel')
const logger = require('./logger')
const mysqlConfig = config.mysqlConfig

Date.prototype.toMysqlFormat = function() {
  return "'" + this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds()) + "'"
}

const db = mysql.createPool({
  host     : mysqlConfig.host || 'localhost',
  port     : mysqlConfig.port || '3306',
  user     : mysqlConfig.user,
  password : mysqlConfig.password,
  database : mysqlConfig.name
})

squel.registerValueHandler(Date, function(date) {
  return date.toMysqlFormat();
});

const connector = {
  start: squel,
  select: squel.select,
  update: squel.update,
  insert: squel.insert,
  delete: squel.delete,
  mysql: db
}
connector.end = (query, one) => {
  return new Promise((resolve, reject) => {
    connector.mysql.query(query, (error, result) => {
      if (error) {
        logger.error(query)
        logger.error('connector error', error)
        return reject(error)
      }
      if (one) result = result[0]
      return resolve(result)
    })
  })
}

connector.getConnection = () => {
  return new Promise((resolve, reject) => {
    connector.mysql.getConnection((err, connection) => {
      if (err) return reject(err)
      else return resolve(connection)
    })
  })
}

const twoDigits = (d) => {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

module.exports = connector
