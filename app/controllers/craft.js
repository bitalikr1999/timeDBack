const router = require('express')();
const connector = require('../systems/connector')
const { validateToken } = require('../middleware')
const { craftsService, itemsService } = require('../services')

router.get('/',
  validateToken(),
  async (req, res) => {
    const userId = req.user.id
    const userCraft = await connector.end(
      connector.select()
      .from('users_crafts')
      .where('user_id = ?', userId)
      .toString()
    )

    res.send(200, craftsService.data.getAll(userCraft.level))
  }
)

router.post('/',
  validateToken(true),
  async (req, res) => {
    try {
      const user = req.user
      const userCraft = await connector.end(
        connector.select()
        .from('users_crafts')
        .where('user_id = ?', user.id)
        .toString()
      )
      const [ recipe ] = await connector.end(
        connector.select()
        .from('craft_recipes', 'cr')
        .field('i.id', 'result_id')
        .field('cr.*')
        .left_join('items', 'i', 'i.id = cr.item_result')
        .where('cr.id = ?', req.body.recipeId)
        .toString()
      )

      if (userCraft.level < recipe.min_craft_level) return res.send(400)

      const needsItems = await connector.end(
        connector.select()
        .from('craft_recipes_items', 'cri')
        .field('i.*')
        .field('cri.id')
        .field('cri.count')
        .field('i.id', 'item_id')
        .left_join('items', 'i', 'i.id = cri.item_id')
        .where('cri.craft_recipe_id = ?', recipe.id)
        .toString()
      )

      const userItems = await itemsService.getUserItems(user.id)

      const isAvalilabity = await itemsService.checkAvailabityItems(needsItems, user.id, userItems)

      if (!isAvalilabity) return res.send(400)

      const prepareUserItems = {}
      userItems.forEach(val => {
        prepareUserItems[val.id] = val
      })

      Promise.all(needsItems.map(async (val) => {
        if (prepareUserItems[val.item_id].count == val.count)
          await itemsService.removeItem(prepareUserItems[val.item_id].users_items_id)
        else
          await itemsService.minusCount(prepareUserItems[val.item_id].users_items_id, val.count)
      }))

      await itemsService.addItems([
        { item_id: recipe.result_id, count: 1, user_id: user.id }
      ], user.id, userItems)

      res.send(200)
    } catch(e) {
      console.log(e)
    }
  }
)

module.exports = router;
