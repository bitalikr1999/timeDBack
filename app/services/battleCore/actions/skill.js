const battleCharacters = require('../characters')
const connector = require('../../../systems/connector')
const skillsService = require('../../skills/helpers')
const skillActions = require('../skills')

const useActive = async (data) => {
  console.log(data)
  const { attackerId, targetId, skillId, round } = data
  const [ attacker, target ] = await battleCharacters.getTwoCharacters(attackerId, targetId)
  const [ skill ] = await skillsService.getSkill(skillId)
  const skillAction = skillActions[skill.group_slug]
  const manaCost = await battleCharacters.minusMana(attacker.id, skill.mana_cost)


  console.log(skill)
  console.log(attackerId)
  console.log(targetId)
  if (manaCost === false) return {
    result: false,
    error: 'mana'
  }

  const result = await skillAction(skill, attacker, round, target)
  result.characterMana = manaCost
  result.skillSlug = skill.group_slug
  result.skillName = skill.name

  return result
}

module.exports = {
  useActive
}
// personal/vitaliy
