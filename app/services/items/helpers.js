const connector = require('../../systems/connector')

const getUserItem = async (itemId) => {
  return connector.end(
    connector.select()
    .from('users_items', 'ui')
    .field('ui.count')
    .field('i.*')
    .field('it.name', 'type_name')
    .field('it.slug', 'type_slug')
    .field('ui.id', 'users_items_id')
    .field('i.id')
    .left_join('items', 'i', 'i.id = ui.item_id')
    .left_join('items_types', 'it', 'it.id = i.type_id')
    .where('ui.id = ?', itemId)
    .toString()
  )
}

const getUserItems = async (userId) => {
  return connector.end(
    connector.select()
    .from('users_items', 'ui')
    .field('ui.count')
    .field('i.*')
    .field('it.name', 'type_name')
    .field('it.slug', 'type_slug')
    .field('ui.id', 'users_items_id')
    .field('i.id')
    .field('ui.id', 'user_id')
    .left_join('items', 'i', 'i.id = ui.item_id')
    .left_join('items_types', 'it', 'it.id = i.type_id')
    .where('ui.user_id = ?', userId)
    .toString()
  )
}

const getItems = async (items) => {
  const query = connector.select()
    .from('items', 'i')
    .field('i.*')
    .field('it.name', 'type_name')
    .field('it.slug', 'type_slug')
    .field('i.id')
    .left_join('items_types', 'it', 'it.id = i.type_id')

  let where = ''

  items.forEach((val, i) => {
    where += `i.id = ${val.item_id}`
    if (i < items.length - 1) where += ' OR '
  })

  return connector.end(query.where(where).toString())
}

const addItems = async (newItems, userId, userItems) => {
  if (!userItems) userItems = await getUserItems(userId)
  const itemsToCreat = []
  const itemsToUpdate = []

  const prepareUserItems = []

  userItems.forEach(val => {
    prepareUserItems[val.id] = val
  })

  newItems.forEach(val => {
    if (prepareUserItems[val.item_id]) {
      itemsToUpdate.push({
        id: prepareUserItems[val.item_id].user_id,
        count: prepareUserItems[val.item_id].count + 1
      })
    } else {
      itemsToCreat.push(val)
    }
  })

  if (userItems.length < 25 && itemsToCreat.length) {
    console.log(itemsToCreat)
    await connector.end(
      connector.insert()
      .into('users_items')
      .setFieldsRows(itemsToCreat)
      .toString()
    )
  }

  if (itemsToUpdate.length) {
    let query = `
      UPDATE users_items
      SET count =
        CASE id
    `

    itemsToUpdate.forEach((val) => {
      query += `WHEN ${val.id} THEN ${val.count} `
    })

    query += ' END WHERE id IN ('

    itemsToUpdate.forEach((val, i) => {
      query += `${val.id}`

      if(i + 1 != itemsToUpdate.length) {
        query += ', '
      }
    })

    query += ')'

    await connector.end(query)
  }

  return
}

const removeItem = async (itemId) => {
  return connector.end(
    connector.delete()
    .from('users_items')
    .where('id = ?', itemId)
    .toString()
  )
}

const minusCount = async (itemId, count) => {
  if (!count) count = 1
  return connector.end(
    connector.update()
    .table('users_items')
    .set(`count = count - ${count}`)
    .where('id = ?', itemId)
    .toString()
  )
}

const checkAvailabityItems = async (needsItems, userId, userItems) => {
  if (!userItems) userItems = await getUserItems(userId)
  const prepareUserItems = {}
  let result = true


  userItems.forEach(val => {
    prepareUserItems[val.id] = val
  })
  needsItems.forEach(val => {
    if (!prepareUserItems[val.item_id]) result = false
    else if (prepareUserItems[val.item_id].count < val.count) result = false
  })

  return result
}

module.exports = {
  getUserItem,
  removeItem,
  getUserItems,
  addItems,
  minusCount,
  getItems,
  checkAvailabityItems
}
