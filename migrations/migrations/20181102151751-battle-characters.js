'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('battle_characters', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true
      },
      position: {
        type: 'int',
        notNull: true
      },
      health: {
        type: 'int'
      },
      mana: {
        type: 'int'
      },
      parent_id: {
        type: 'int'
      },
      parent_table: {
        type: 'string'
      },
      owner_id: {
        type: 'int'
      },
      character_level_id: {
        type: 'int',
        foreignKey: {
          name: 'battle_character_chatacter_level_f',
          table: 'character_levels',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      battle_id: {
        type: 'int',
        foreignKey: {
          name: 'battles_characters_battle_f',
          table: 'battles',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      created_at: {
        type: 'timestamp',
        notNull: true,
        defaultValue: 'CURRENT_TIMESTAMP'
      }
    },
    ifNotExists: true
  })
};

exports.down = function(db) {
  return db.dropTable('battle_characters');
};

exports._meta = {
  "version": 1
};
