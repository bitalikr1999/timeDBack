const connector = require('../../../app/systems/connector')


connector.end(
  connector.insert().into('enemy_district_possible')
  .setFieldsRows([
    {
      character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'goblinI'),
      min_town_level: 1,
      town_type_id: connector.select().from('type_towns').field('id').where('slug = ?', 'village'),
      chance: 100
    },

    {
      character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'goblinII'),
      min_town_level: 1,
      town_type_id: connector.select().from('type_towns').field('id').where('slug = ?', 'village'),
      chance: 70
    },

    {
      character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'goblinIII'),
      min_town_level: 1,
      town_type_id: connector.select().from('type_towns').field('id').where('slug = ?', 'village'),
      chance: 30
    },

  ]).toString()
)
