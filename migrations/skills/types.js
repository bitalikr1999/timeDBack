const connector = require('../../app/systems/connector')


connector.end(
  connector.insert().into('skills_types')
  .setFieldsRows([
    {
      slug: 'active',
      name: 'Активный',
    },
    {
      slug: 'passive_single',
      name: 'Пасивный'
    }
  ]).toString()
)
