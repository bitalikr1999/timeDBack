const jsonwebtoken = require('jsonwebtoken')
const config = require('../../config')

const create = (id) => {
  return new Promise((resolve, reject) => {
    jsonwebtoken.sign({
    		id: id,
    		exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 1000),
    	}, config.jwt, (err, token) => {
          if(err)
            reject(err)
          else
            resolve(token)
      	}
    )
  })
}

const verify = (token) => {
  return new Promise((resolve, reject) => {
    jsonwebtoken.verify(token, config.jwt, (err, decoded) => {
      if (err) return reject(err)
      return resolve(decoded)
    })
  })
}

module.exports = {
  create,
  verify
}
