const connector = require('../../app/systems/connector')


connector.end(
  connector.insert().into('craft_recipes_types')
  .setFieldsRows([
    {
      slug: 'resources',
      name: 'Ресурсы'
    },
    {
      slug: 'potions',
      name: 'Зелья'
    },
    {
      slug: 'cards',
      name: 'Картоки'
    },
    {
      slug: 'sheres',
      name: 'Сферы'
    },
    {
      slug: 'rings',
      name: 'Кольца'
    },{
      slug: 'others',
      name: 'Другое'
    }
  ]).toString()
)
