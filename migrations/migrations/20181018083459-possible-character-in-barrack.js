'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('barrack_possible_character', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true
      },
      chance: {
        type: 'int'
      },
      price: {
        type: 'int'
      },
      town_type_id: {
        type: 'int',
        foreignKey: {
          name: 'towns_possible_character_barrack_f',
          table: 'type_towns',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      barrack_level: {
        type: 'int',
        foreignKey: {
          name: 'barrack_level_possible_character_barrack_f',
          table: 'barracks',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      character_level_id: {
        type: 'int',
        foreignKey: {
          name: 'character_possible_barrack_character_f',
          table: 'character_levels',
          mapping: 'id',
          rules: {
            onDelete: 'RESTRICT',
            onUpdate: 'RESTRICT'
          }
        }
      },
      created_at: {
        type: 'timestamp',
        notNull: true,
        defaultValue: 'CURRENT_TIMESTAMP'
      }
    },
    ifNotExists: true
  })
};

exports.down = function(db) {
  return db.dropTable('barrack_possible_character');
};

exports._meta = {
  "version": 1
};
