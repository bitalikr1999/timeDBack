const config = require('../../config')
const logger = require('../../systems/logger')
const connector = require('../../systems/connector')
const { getCharacterSkills } = require('../skills/helpers')

const getRandom = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}

const addCharactersToUser = async (characterSlugs, userId) => {
  const rows = []

  characterSlugs.forEach(val => {
      rows.push({
        user_id: userId,
        character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', val)
      })
  })

  const query = connector.insert().into('characters').setFieldsRows(rows).toString()

  return connector.end(query)

}

const getDistrictTeam = async (userId, isMy) => {
  const query =
    connector.select()
      .from('enemy_district_character', 'e')
      .field('ch.*')
      .field('ed.id', 'teamId')
      .field('g.logo', 'groupLogo')
      .field('g.name', 'groupName')
      .left_join('enemy_district_teams', 'ed', 'ed.id = e.enemy_team_id')
      .left_join('character_levels', 'ch', 'ch.id = e.character_level_id')
      .left_join('characters_groups', 'g', 'g.id = ch.group_id')
      .where('ed.town_id = ?', connector.select().from('towns').field('id').where('user_id = ?', userId))
      .toString()

  return connector.end(query)
}

const getDistrictTeamPreview = async (userId, isMy) => {
  const teams = await connector.end(
    connector.select()
      .from('enemy_district_teams')
      .where('town_id = ?', connector.select().from('towns').field('id').where('user_id = ?', userId))
      .where('belonging = ?', isMy ? 1 : 0)
      .toString()
  )
  if (teams.length > 1) {
      await connector.end(
        connector.delete()
        .from('enemy_district_teams')
        .where('town_id = ?', connector.select().from('towns').field('id').where('user_id = ?', userId))
        .where('belonging = ?', isMy ? 1 : 0)
        .toString()
      )
      return false
  } else {
    return teams[0] ? teams[0] : false
  }
}

const generetedDistrictTeam = async (userId, town) => {
  try{
    const possibleEnemy = await connector.end(
      connector.select()
      .from('enemy_district_possible', 'ep')
      .field('ch.*')
      .field('tc.slug', 'type')
      .left_join('character_levels', 'ch', 'ch.id = ep.character_level_id')
      .left_join('type_towns', 't', 't.id = ep.town_type_id')
      .left_join('type_characters', 'tc', 'tc.id = ch.type_id')
      .where('t.id = ?', town.type_towns_id)
      .toString()
    )

    const numberOfEnemy = getRandom(3, 8)
    let teamFull = false
    let firstRowPosition = 1
    let secondRowPosition = 6
    let teamEnemys = []

    while (!teamFull) {
      const characterLevel = possibleEnemy[getRandom(0, possibleEnemy.length - 1)]

      let position

      if (characterLevel.type_attack == 1 && firstRowPosition < 6) {
        position = firstRowPosition
        firstRowPosition++
      } else if (secondRowPosition < 11) {
        position = secondRowPosition
        secondRowPosition++
      } else {
        position = firstRowPosition
        firstRowPosition++
      }

      teamEnemys.push({
        character_level_id: characterLevel.id,
        position,
        img: characterLevel.img
      })

      if (teamEnemys.length == numberOfEnemy) teamFull = true
    }

    const team = {
      number_of: numberOfEnemy,
      preview: teamEnemys[0].img,
      town_id: town.id
    }

    if (town.user_id == userId) team.belonging = 1
    else team.belonging = 0

    const { insertId } = await connector.end(
      connector.insert()
      .into('enemy_district_teams')
      .setFields(team)
      .toString()
    )

    const enemysForInsert = []

    teamEnemys.forEach(val => {
      enemysForInsert.push({
        character_level_id : val.character_level_id,
        position            : val.position,
        enemy_team_id       : insertId
      })
    })

    const result = await connector.end(
      connector.insert()
      .into('enemy_district_character')
      .setFieldsRows(enemysForInsert)
      .toString()
    )
    return result
  } catch (e) {
    logger.error(e)
  }
}

const selectTeam = async (parentId, parentType, specEnemy) => {
  let realParentId = parentId
  const query = connector.select()
    .field('cl.*')
    .field('c.id')
    .field('cl.id', 'characterLevelId')
    .field('c.position')
    .field('cl.id', 'character_level_id')
    .field('tc.slug', 'character_type_slug')

  if (specEnemy) {
    parentId = parentId.split(specEnemy + '/')[1]
  }

  switch (parentType) {
    case 'user':
      query.from('characters', 'c')
      query.field('c.user_id', 'parentId')
    break;

    case 'enemy':
      query.from('enemy_district_character', 'c')
      query.field('c.enemy_team_id', 'parentId')
    break;

    case 'campain':
      query.from('campains_levels_enemy', 'c')
      query.field('c.campain_level_id', 'parentId')
    break;
  }

  query.left_join('character_levels', 'cl', 'cl.id = c.character_level_id')
  query.left_join('type_characters', 'tc', 'tc.id = cl.type_id')

  switch (parentType) {
    case 'user': query.where('c.user_id = ?', parentId); break;
    case 'enemy': query.where('c.enemy_team_id = ?', parentId); break;
    case 'campain': query.where('c.campain_level_id = ?', parentId); break;
  }

  query.where('c.position > 0')
  query.where('c.position < 11')

  const team = {
    parentId: realParentId,
    parentType: parentType,
    characters: null
  }

  team.characters = await connector.end(query.toString())

  await Promise.all(
    team.characters.map(async (val, i, arr) => {
      arr[i].skills = await getCharacterSkills(val.character_level_id)
    })
  );

  return team
}

const addExp = async (exp, charactersIds) => {
  const query = connector.update()
    .table('characters')
    .set(`experience = experience + ${exp}`)
  let where = ''
  charactersIds.forEach((val, i, arr) => {
    where += `id = ${val}`
    if (i < arr.length - 1) where += ' OR '
  })
  query.where(where)
  return await connector.end(query.toString())
}

const removeDistrictTeam = async (teamId, townId) => {
  await connector.end(
    connector.delete()
    .from('enemy_district_character')
    .where('enemy_team_id = ?', teamId)
    .toString()
  )

  await connector.end(
    connector.delete()
    .from('enemy_district_teams')
    .where('id = ?',  teamId)
    .toString()
  )

  const endNewInterval = new Date(new Date().getTime() + 0)
  await connector.end(
    connector.update()
    .table('enemy_district_interval')
    .set('end', endNewInterval)
    .where('town_id = ?', townId)
    .toString()
  )
  return true;
}

module.exports = {
  addCharactersToUser,
  getDistrictTeam,
  generetedDistrictTeam,
  getDistrictTeamPreview,
  selectTeam,
  addExp,
  removeDistrictTeam
}
