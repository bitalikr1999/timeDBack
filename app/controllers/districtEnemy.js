const router = require('express')();
const connector = require('../systems/connector')
const { validateToken, validateReq, parseDb } = require('../middleware')
const async = require('async')
const { townLevels } = require('../config')
const { charactersService } = require('../services')


// router.get('/',
//   validateToken(),
//   async (req, res) => {
//     const enemyTeam = await charactersService.getDistrictTeamPreview(req.user.id, true)
//     res.send(200, enemyTeam)
//   }
// )

router.get('/',
  validateToken(),
  parseDb([
    { name: 'token', model: 'towns', one: true, forResult: 'town', dbField: 'user_id' }
  ]),
  async (req, res) => {
    let { town } = req.dbFields
    const userId = req.user.id
    const nowDate = new Date().getTime()

    let enemyTeam = await charactersService.getDistrictTeamPreview(userId, true)

    if (enemyTeam && enemyTeam.length) res.send(200, enemyTeam)

    const [ interval ] = await connector.end(
      connector.select()
      .from('enemy_district_interval')
      .where('user_id = ?', userId)
      .where('town_id = ?', town.id)
      .toString()
    )

    if (!interval || (new Date(interval.end).getTime() < nowDate)) {

      if (interval) {
        await connector.end(connector.delete().from('enemy_district_interval').where('id = ?', interval.id).toString())
      }

      const $genereteEnemy = charactersService.generetedDistrictTeam(userId, town)
      const endNewInterval = new Date(nowDate + 0 * 1000)
      const $createInterval = connector.end(
        connector.insert()
        .into('enemy_district_interval')
        .setFields({
          'end'     : endNewInterval,
          'town_id' : town.id,
          'user_id' : userId
        })
        .toString()
      )

      await Promise.all([$genereteEnemy, $createInterval])
    }

    enemyTeam = await charactersService.getDistrictTeamPreview(userId, true)
    res.send(200, enemyTeam)
  }
)

module.exports = router;
