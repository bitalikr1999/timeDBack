'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('characters_levels_skills', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true
      },
      skill_id: {
        notNull: true,
        type: 'int',
        foreignKey: {
          name: 'skill_characters_levels_skills_f',
          table: 'skills',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      character_level_id: {
        notNull: true,
        type: 'int',
        foreignKey: {
          name: 'characters_characters_levels_skills_f',
          table: 'character_levels',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      created_at: {
        type: 'timestamp',
        notNull: true,
        defaultValue: 'CURRENT_TIMESTAMP'
      }
    },
    ifNotExists: true
  })
};

exports.down = function(db) {
  return db.dropTable('characters_levels_skills');
};

exports._meta = {
  "version": 1
};
