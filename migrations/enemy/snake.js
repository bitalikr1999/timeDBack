const connector = require('../../app/systems/connector')

connector.end(
  connector.insert().into('character_levels')
  .setFieldsRows([
    {
      img: '/character/snake/thumbnail/caveman.jpg',
      name: 'Пещерны змей',
      slug: 'cavemanSnake',
      attack: 3,
      health: 15,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 0,
      full_img: '/character/snake/caveman.jpg',
      initiative: 1,
      theft_life: 0,
      inheriot_at: 0,
      type_attack: 1,
      magic_armor: 0,
      physical_armor: 1,
      number_actions: 1,
      need_experience: 0,
      critical_hit_chance: 0
    }
  ]).toString()
)
