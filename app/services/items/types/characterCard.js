const connector = require('../../../systems/connector')

module.exports = async (item, userId) => {

  await connector.end(
    connector.insert()
    .into('characters')
    .setFields({
      user_id: userId,
      character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', item.data)
    })
    .toString()
  )

  const [ character ] = await connector.end(
    connector.select()
    .from('character_levels')
    .where('slug = ?', item.data)
    .toString()
  )

  return character
}
