'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('enemy_district_possible', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true
      },
      character_level_id: {
        type: 'int',
        foreignKey: {
          name: 'character_district_possible_enemy_f',
          table: 'character_levels',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      town_type_id: {
        type: 'int',
        foreignKey: {
          name: 'town_type_district_possible_enemy_f',
          table: 'type_towns',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      min_town_level: {
        type: 'int',
        notNull: true
      },
      created_at: {
        type: 'timestamp',
        notNull: true,
        defaultValue: 'CURRENT_TIMESTAMP'
      }
    },
    ifNotExists: true
  })
};

exports.down = function(db) {
  return db.dropTable('enemy_district_possible');
};

exports._meta = {
  "version": 1
};
