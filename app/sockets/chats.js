const connector = require('../systems/connector')
const { jwtService } = require('../services')
const uuidv4 = require('uuid/v4');
const chatId = uuidv4()

const joinToChat = async (socket) => {
  socket.join(chatId)

}

const sendMessage = async (socket, data) => {
  data.user = {
    login: global.Sockets[socket.id].user.login,
    name: global.Sockets[socket.id].user.name
  }
  global.io.sockets.to(chatId).emit('chat/message', data)
}

module.exports = (socket) => {
  socket.on('auth', (data) => {
    joinToChat(socket, data)
  })

  socket.on('chat/send', (data) => {
    sendMessage(socket, data)
  })
}
