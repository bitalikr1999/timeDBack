const connector = require('../../app/systems/connector')


connector.end(
  connector.insert().into('character_levels')
  .setFieldsRows([
    {
      img: '/character/elf-warrior/thumbnail/elf-warriorI.jpg',
      name: 'Ельфийский воин I',
      slug: 'elf-warriorI',
      attack: 3,
      health: 10,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 10,
      full_img: '/character/elf-warrior/elf-warriorI.jpg',
      initiative: 2,
      theft_life: 0,
      inheriot_at: 0,
      type_attack: 1,
      magic_armor: 0,
      physical_armor: 0,
      number_actions: 1,
      need_experience: 0,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'elf')
    },

    {
      img: '/character/elf-warrior/thumbnail/elf-warriorII.jpg',
      name: 'Ельфийский воин II',
      slug: 'elf-warriorII',
      attack: 4,
      health: 10,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 10,
      full_img: '/character/elf-warrior/elf-warriorII.jpg',
      initiative: 2,
      theft_life: 0,
      inheriot_at: 'elf-warriorI',
      type_attack: 1,
      magic_armor: 2,
      physical_armor: 2,
      number_actions: 1,
      need_experience: 200,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'elf')
    },

    {
      img: '/character/elf-warrior/thumbnail/elf-warriorIII.jpg',
      name: 'Ельфийский воин III',
      slug: 'elf-warriorIII',
      attack: 4,
      health: 10,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 15,
      full_img: '/character/elf-warrior/elf-warriorIII.jpg',
      initiative: 2,
      theft_life: 0,
      inheriot_at: 'elf-warriorII',
      type_attack: 1,
      magic_armor: 2,
      physical_armor: 2,
      number_actions: 1,
      need_experience: 400,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'elf')
    },

    {
      img: '/character/elf-warrior/thumbnail/elf-warriorIV.jpg',
      name: 'Главнокомандующий воинов I',
      slug: 'elf-warriorIV',
      attack: 5,
      health: 13,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 15,
      full_img: '/character/elf-warrior/elf-warriorIII.jpg',
      initiative: 3,
      theft_life: 0,
      inheriot_at: 'elf-warriorIII',
      type_attack: 1,
      magic_armor: 2,
      physical_armor: 2,
      number_actions: 1,
      need_experience: 1000,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'elf')
    },

    {
      img: '/character/elf-warrior/thumbnail/elf-warriorIV.jpg',
      name: 'Главнокомандующий воинов II',
      slug: 'elf-warriorV',
      attack: 5,
      health: 15,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 15,
      full_img: '/character/elf-warrior/elf-warriorIII.jpg',
      initiative: 3,
      theft_life: 0,
      inheriot_at: 'elf-warriorIV',
      type_attack: 1,
      magic_armor: 2,
      physical_armor: 2,
      number_actions: 1,
      need_experience: 1000,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'elf')
    },

    {
      img: '/character/elf-warrior/thumbnail/elf-warriorIV.jpg',
      name: 'Главнокомандующий воинов III',
      slug: 'elf-warriorVI',
      attack: 5,
      health: 15,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 20,
      full_img: '/character/elf-warrior/elf-warriorIII.jpg',
      initiative: 3,
      theft_life: 0,
      inheriot_at: 'elf-warriorV',
      type_attack: 1,
      magic_armor: 2 ,
      physical_armor: 4,
      number_actions: 1,
      need_experience: 1000,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'elf')
    }
  ]
).toString()
)
