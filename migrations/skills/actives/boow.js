const connector = require('../../../app/systems/connector')
const func = async () => {

  // await connector.end(
  //   connector.insert().into('skills')
  //   .setFieldsRows([
  //     {
  //       slug: 'double_shot',
  //       name: 'Двойной выстрел',
  //       chance: 0,
  //       data: 0,
  //       turns_count: 0,
  //       mana_cost: 3,
  //       skill_type_id: connector.select().from('skills_types').field('id').where('slug = ?', 'active'),
  //     },
  //   ]).toString()
  // )


  // await connector.end(
  //   connector.insert().into('skills')
  //   .setFieldsRows([
  //     {
  //       slug: 'reinforced_shot',
  //       name: 'Усиленный выстрел',
  //       chance: 0,
  //       data: JSON.stringify({
  //         critical_hit_chance: 35,
  //         attack: 2
  //       }),
  //       turns_count: 0,
  //       mana_cost: 3,
  //       group_id: connector.select().from('skills_groups').field('id').where('slug = ?', 'enhanced_blow'),
  //       skill_type_id: connector.select().from('skills_types').field('id').where('slug = ?', 'active'),
  //     },
  //   ]).toString()
  // )

  // await connector.end(
  //   connector.insert().into('skills')
  //   .setFieldsRows([
  //     {
  //       slug: 'protective_rack',
  //       name: 'Защитная стойка',
  //       chance: 0,
  //       data: JSON.stringify({
  //         magic_armor: 2,
  //         physical_armor: 2
  //       }),
  //       turns_count: 2,
  //       mana_cost: 4,
  //       group_id: connector.select().from('skills_groups').field('id').where('slug = ?', 'magnification_yourself'),
  //       skill_type_id: connector.select().from('skills_types').field('id').where('slug = ?', 'active'),
  //     },
  //   ]).toString()
  // )



  // await connector.end(
  //   connector.insert().into('characters_levels_skills')
  //   .setFieldsRows([
  //     {
  //       skill_id: connector.select().from('skills').field('id').where('slug = ?', 'double_shot'),
  //       character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'boowI'),
  //     },
  //   ]).toString()
  // )

  // await connector.end(
  //   connector.insert().into('characters_levels_skills')
  //   .setFieldsRows([
  //     {
  //       skill_id: connector.select().from('skills').field('id').where('slug = ?', 'double_shot'),
  //       character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'boowII'),
  //     },
  //   ]).toString()
  // )
  //
  // await connector.end(
  //   connector.insert().into('characters_levels_skills')
  //   .setFieldsRows([
  //     {
  //       skill_id: connector.select().from('skills').field('id').where('slug = ?', 'double_shot'),
  //       character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'boowIII'),
  //     },
  //   ]).toString()
  // )

  // await connector.end(
  //   connector.insert().into('characters_levels_skills')
  //   .setFieldsRows([
  //     {
  //       skill_id: connector.select().from('skills').field('id').where('slug = ?', 'reinforced_shot'),
  //       character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'boowIII'),
  //     },
  //   ]).toString()
  // )

  await connector.end(
    connector.insert().into('characters_levels_skills')
    .setFieldsRows([
      {
        skill_id: connector.select().from('skills').field('id').where('slug = ?', 'protective_rack'),
        character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'boowIII'),
      },
    ]).toString()
  )
}

func()
