const userService = require('../../users')
const dropService = require('../../items/drops')
const connector = require('../../../systems/connector')
const { addItems, getItems } = require('../../items/helpers')

module.exports = async (data) => {
  try {
    const { teamWinId, teamLossId } = data
    const possibleDrops = await connector.end(
      connector.select()
      .from('towns_drops_possible')
      .where('town_type_id = ?', (
        connector.select()
        .from('towns')
        .field('type_towns_id')
        .where('user_id = ?', teamWinId)
      ))
      .toString()
    )
    
    const items = dropService(possibleDrops)

    items.forEach((val, i) => {
      items[i].user_id = teamWinId
    })

    await addItems(items, teamWinId)

    return await getItems(items)

  } catch (e) {
    throw new Error(e);
  }
}
