const connector = require('../../../app/systems/connector')
const func = async () => {

  // await connector.end(
  //   connector.insert().into('skills')
  //   .setFieldsRows([
  //     {
  //       slug: 'sehit_comanderI',
  //       name: 'Сехит предвотель',
  //       chance: 0,
  //       data: JSON.stringify({
  //         physical_armor: 1,
  //         critical_hit_chance: 5
  //       }),
  //       turns_count: 0,
  //       mana_cost: 0,
  //       buff_id: connector.select().from('skills_buffs').field('id').where('slug = ?', 'basic_magnification'),
  //       group_id: connector.select().from('skills_groups').field('id').where('slug = ?', 'pas_magnification_rase'),
  //       skill_type_id: connector.select().from('skills_types').field('id').where('slug = ?', 'passive_single'),
  //     },
  //   ]).toString()
  // )

  await connector.end(
    connector.insert().into('characters_levels_skills')
    .setFieldsRows([
      {
        skill_id: connector.select().from('skills').field('id').where('slug = ?', 'sehit_comanderI'),
        character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'sehitI'),
      }
    ]).toString()
  )


}

func()
