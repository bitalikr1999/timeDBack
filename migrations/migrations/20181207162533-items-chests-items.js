'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('items_chests_items', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true
      },
      item_parent_id: {
        type: 'int',
        foreignKey: {
          name: 'items_chests_items_item_parent_f',
          table: 'items',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      item_id: {
        type: 'int',
        foreignKey: {
          name: 'items_chests_items_item_f',
          table: 'items',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      chance: {
        type: 'int',
        default: 25
      },
      created_at: {
        type: 'timestamp',
        notNull: true,
        defaultValue: 'CURRENT_TIMESTAMP'
      }
    },
    ifNotExists: true
  })
};

exports.down = function(db) {
  return db.dropTable('items');
};

exports._meta = {
  "version": 1
};
