const config = require('../../config')
const logger = require('../../systems/logger')
const connector = require('../../systems/connector')

const getCharacterSkills = async (characterLevelId) => {
  return await connector.end(
    connector.select()
    .from('characters_levels_skills', 'cls')
    .field('s.*')
    .field('sg.slug', 'group_slug')
    .field('st.name', 'type_name')
    .left_join('skills', 's', 's.id = cls.skill_id')
    .left_join('skills_groups', 'sg', 'sg.id = s.group_id')
    .left_join('skills_types', 'st', 'st.id = s.skill_type_id')
    .where('cls.character_level_id = ?', characterLevelId)
    .toString()
  )
}

const getSkill = async (skillId) => {

  return await connector.end(
    connector.select()
    .from('skills', 's')
    .field('s.*')
    .field('st.name', 'type_name')
    .field('sg.slug', 'group_slug')
    .field('sb.id', 'skill_buff_id')
    .left_join('skills_groups', 'sg', 'sg.id = s.group_id')
    .left_join('skills_types', 'st', 'st.id = s.skill_type_id')
    .left_join('skills_buffs', 'sb', 'sb.id = s.buff_id')
    .where('s.id = ?', skillId)
    .toString()
  )
}

const getSkillsByGroup = async (characterLevelId, group) => {
  return await connector.end(
    connector.select()
    .from('skills', 's')
    .field('s.*')
    .field('st.name', 'type_name')
    .field('sg.slug', 'group_slug')
    .field('sb.id', 'skill_buff_id')
    .left_join('skills_groups', 'sg', 'sg.id = s.group_id')
    .left_join('skills_types', 'st', 'st.id = s.skill_type_id')
    .left_join('skills_buffs', 'sb', 'sb.id = s.buff_id')
    .left_join('characters_levels_skills', 'cls', 'cls.skill_id = s.id')
    .where('cls.character_level_id = ?', characterLevelId)
    .where('sg.slug = ?', group)
    .toString()
  )
}


module.exports = {
  getCharacterSkills,
  getSkill,
  getSkillsByGroup
}
