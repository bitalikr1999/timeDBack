const connector = require('../../../app/systems/connector')

connector.end(
  connector.insert().into('towns_drops_possible')
  .setFieldsRows([
    {
      chance: 75,
      max_count: 10,
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-playoni-shard-1'),
      town_type_id: connector.select().from('type_towns').field('id').where('slug = ?', 'village')
    },

    {
      chance: 35,
      max_count: 2,
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-playoni-shard-2'),
      town_type_id: connector.select().from('type_towns').field('id').where('slug = ?', 'village')
    },

    {
      chance: 50,
      max_count: 5,
      item_id: connector.select().from('items').field('id').where('slug = ?', 'i-chest_old'),
      town_type_id: connector.select().from('type_towns').field('id').where('slug = ?', 'village')
    }

  ]).toString()
)
