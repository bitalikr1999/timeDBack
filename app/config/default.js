const config = {

  jwt: 'some7Jysnx8j9KS90M&m@*',

  mysqlConfig: {
    "host"      : "localhost",
    "port"      : "3306",
    "name"      : "timeD",
    "user"      : "root",
    "password"  : "1065473"
  },

  startCharacters: ['tramp', 'tramp', 'boowI', 'boowI'],

  townLevels: {
    1: { cost: 50 },
    2: { cost: 200 },
    3: { cost: 500 },
    4: { cost: 1500 },
    5: { cost: 3000 },
    6: { cost: 5000 },
    7: { cost: 10000 },
    8: { cost: 50000 },
    9: { cost: 300000 }
  },

  playerType: ['user', 'enemy']
}

module.exports = config
