const connector = require('../../app/systems/connector')


connector.end(
  connector.insert().into('items')
  .setFieldsRows([
    {
      slug: 'i-char-warriorI',
      name: 'Карта Воина I',
      discription: 'При помощи этой карты мы можете призвать к себе на слубжу "Воин I"',
      img: 'items-card1',
      type_id: connector.select().from('items_types').field('id').where('slug = ?', 'character-card')
    },
  ]).toString()
)
