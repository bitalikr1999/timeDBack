'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('skills', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: 'string'
      },
      slug: {
        type: 'string',
        unique: true,
        notNull: true
      },
      group_id: {
        notNull: true,
        type: 'int',
        foreignKey: {
          name: 'skills_groups_skills_f',
          table: 'skills_groups',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      skill_type_id: {
        notNull: true,
        type: 'int',
        foreignKey: {
          name: 'skills_types_skills_f',
          table: 'skills_types',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      data: {
        type: 'text'
      },
      chance: {
        type: 'int',
        default: 100
      },
      turns_count: {
        type: 'int',
        default: 1
      },
      img: {
        type: 'string'
      },
      buff_id: {
        type: 'int',
        foreignKey: {
          name: 'skills_buffs_skills_f',
          table: 'skills_buffs',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      created_at: {
        type: 'timestamp',
        notNull: true,
        defaultValue: 'CURRENT_TIMESTAMP'
      }
    },
    ifNotExists: true
  })
};

exports.down = function(db) {
  return db.dropTable('skills');
};

exports._meta = {
  "version": 1
};
