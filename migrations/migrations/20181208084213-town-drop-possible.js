'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('towns_drops_possible', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true
      },
      chance: {
        type: 'int',
        default: 25
      },
      max_count: {
        type: 'int',
        default: 1
      },
      town_type_id: {
        notNull: true,
        type: 'int',
        foreignKey: {
          name: 'town_type_drops_possible_f',
          table: 'type_towns',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      item_id: {
        type: 'int',
        foreignKey: {
          name: 'item_towns_drop_possible_f',
          table: 'items',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      created_at: {
        type: 'timestamp',
        notNull: true,
        defaultValue: 'CURRENT_TIMESTAMP'
      }
    },
    ifNotExists: true
  })
};

exports.down = function(db) {
  return db.dropTable('arena_log');
};

exports._meta = {
  "version": 1
};
