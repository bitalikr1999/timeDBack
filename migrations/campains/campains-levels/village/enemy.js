const connector = require('../../../../app/systems/connector')


// connector.end(
//   connector.insert().into('campains_levels_enemy')
//   .setFieldsRows([
//     {
//       campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_1'),
//       character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'goblinI'),
//       position: 2
//     },
//   ]).toString()
// )


connector.end(
  connector.insert().into('campains_levels_enemy')
  .setFieldsRows([
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_2'),
      character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'goblinI'),
      position: 2
    },
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_2'),
      character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'goblinII'),
      position: 3
    },
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_2'),
      character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'goblinI'),
      position: 4
    },
  ]).toString()
)

connector.end(
  connector.insert().into('campains_levels_enemy')
  .setFieldsRows([
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_3'),
      character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'goblinII'),
      position: 2
    },
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_3'),
      character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'goblinII'),
      position: 3
    },
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_3'),
      character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'goblinII'),
      position: 4
    },
  ]).toString()
)

connector.end(
  connector.insert().into('campains_levels_enemy')
  .setFieldsRows([
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_4'),
      character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'goblinIII'),
      position: 2
    },
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_4'),
      character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'goblinII'),
      position: 3
    },
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_4'),
      character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'goblinII'),
      position: 4
    },
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_4'),
      character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'goblinIII'),
      position: 5
    },
  ]).toString()
)

connector.end(
  connector.insert().into('campains_levels_enemy')
  .setFieldsRows([
    {
      campain_level_id: connector.select().from('campains_levels').field('id').where('slug = ?', 'village_lvl_5'),
      character_level_id: connector.select().from('character_levels').field('id').where('slug = ?', 'cavemanSnake'),
      position: 3
    }
  ]).toString()
)
