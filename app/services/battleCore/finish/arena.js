const userService = require('../../users')
const randomIntFromInterval = (min,max) => Math.floor(Math.random()*(max-min+1)+min)
const connector = require('../../../systems/connector')

module.exports = async (data) => {
  try {
    const { teamWinId, teamLossId } = data
    const rate = randomIntFromInterval(15, 50)
    const query = connector.insert().into('arena_log')
      .setFields({
        rate,
        user_win_id: teamWinId,
        user_loss_id: teamLossId
      })

    await Promise.all([
      userService.addRate(teamWinId, rate),
      userService.removeRate(teamLossId, rate),
      connector.end(query.toString())
    ])
    return {rate}

  } catch (e) {
    throw new Error(e);
  }
}
