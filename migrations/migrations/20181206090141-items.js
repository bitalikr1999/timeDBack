'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('items', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true
      },
      slug: {
        type: 'string',
        unique: true,
        notNull: true
      },
      name: {
        type: 'string',
      },
      discription: {
        type: 'string'
      },
      img: {
        type: 'string'
      },
      type_id: {
        type: 'int',
        foreignKey: {
          name: 'items_types_items_f',
          table: 'items_types',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      data: {
        type: 'string',
      },
      created_at: {
        type: 'timestamp',
        notNull: true,
        defaultValue: 'CURRENT_TIMESTAMP'
      }
    },
    ifNotExists: true
  })
};

exports.down = function(db) {
  return db.dropTable('items');
};

exports._meta = {
  "version": 1
};
