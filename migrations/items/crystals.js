const connector = require('../../app/systems/connector')


connector.end(
  connector.insert().into('items')
  .setFieldsRows([
    {
      slug: 'i-crystal-red-1',
      name: 'I Красный кристал',
      discription: 'Простой кристал, пока не понятно для чего он нужен',
      img: 'items-crystal-red1',
      type_id: connector.select().from('items_types').field('id').where('slug = ?', 'crystal'),
      data: 0
    },
    {
      slug: 'i-crystal-red-2',
      name: 'II Красный кристал',
      discription: 'Простой кристал, пока не понятно для чего он нужен',
      img: 'items-crystal-red2',
      type_id: connector.select().from('items_types').field('id').where('slug = ?', 'crystal'),
      data: 0
    },
  ]).toString()
)
