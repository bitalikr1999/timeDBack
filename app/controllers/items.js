const router = require('express')();
const connector = require('../systems/connector')
const { validateToken } = require('../middleware')
const { itemsService } = require('../services')

router.get('/',
  validateToken(true),
  async (req, res) => {
    const user = req.user
    const items = await connector.end(
      connector.select()
      .from('users_items', 'ui')
      .field('ui.count')
      .field('i.*')
      .field('it.name', 'type_name')
      .field('it.slug', 'type_slug')
      .field('ui.id')
      .field('i.id', 'item_id')
      .left_join('items', 'i', 'i.id = ui.item_id')
      .left_join('items_types', 'it', 'it.id = i.type_id')
      .where('ui.user_id = ?', user.id)
      .toString()
    )
    res.send(200, items)
  }
)

router.post('/',
  validateToken(true),
  async (req, res) => {
    const user = req.user
    const { itemId } = req.body

    const [ item ] = await connector.end(
      connector.select().from('users_items').where('id = ?', itemId).toString()
    )

    if (!item) res.send(400)
    if (item.user_id != user.id) res.send(401)

    const result = await itemsService.useItem(itemId, user.id)

    res.send(200, result)

  }
)

router.post('/town-shere',
  validateToken(true),
  async (req, res) => {
    const user = req.user
    const { sphereId } = req.body

    const [ item ] = await connector.end(
      connector.select()
      .from('users_items', 'ui')
      .field('i.*')
      .field('ui.user_id')
      .left_join('items', 'i', 'i.id = ui.item_id')
      .where('ui.id = ?', sphereId).toString()
    )
    if (!item) res.send(400)
    if (item.user_id != user.id) res.send(401)

    const [ townType ] = await connector.end(
      connector.select()
      .from('type_towns')
      .where('slug = ?', item.data)
      .toString()
    )

    res.send(200, townType)

  }
)
module.exports = router;
