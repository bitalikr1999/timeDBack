const battleCharacters = require('../characters')
const connector = require('../../../systems/connector')

module.exports = async (heallerId, targetId) => {
  const [ healler, target ] = await battleCharacters.getTwoCharacters(heallerId, targetId)
  const [ realTarget ] = await connector.end(
    connector.select()
    .from('character_levels')
    .where('id = ?', target.character_level_id)
    .toString()
  )

  console.log('healler', healler)
  console.log('target', target)
  let result = {}

  let healPoint = healler.attack
  
  if (healPoint + target.health > target['max-health']) {
    healPoint = realTarget.health - target.health
  }

  await connector.end(
    connector.update()
    .table('battle_characters')
    .set('health', target.health + healPoint)
    .where('id = ?', targetId)
    .toString()
  )

  return {
    healPoint
  }

}
