const connector = require('../../../app/systems/connector')

const func = async () => {
  await connector.end(
    connector.insert().into('craft_recipes')
    .setFieldsRows([
      {
        name: 'Ресурсы',
        min_craft_level: 1,
        craft_recipe_type: connector.select().from('craft_recipes_types').field('id').where('slug = ?', 'resources')
      },
    ]).toString()
  )
}

module.exports = func()
