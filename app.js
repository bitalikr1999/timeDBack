
const express     = require('express')
const cors        = require('cors')
const bodyParser  = require('body-parser')
const debug       = require('debug')('app');
const logger      = require('./app/systems/logger')
const router      = require('./app/controllers')
const socketSignals = require('./app/sockets')
const io          = require('socket.io')()
const middleware  = require('socketio-wildcard')()
const app         = express()

app.use(express.static(process.env.NODE_PATH + '/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors())

app.use(router)

app.use((req, res) => {
  res.send(404)
});

app.use((err, req, res, next) => {
  logger.fatal(err)
  res.json(500, {message: err.message})
});

app.set('port', process.env.PORT || 3000);

const server = app.listen(app.get('port'), function() {
  logger.info('Express server listening on port ' + server.address().port);
});

io.use(middleware)
io.attach(server)
socketSignals(io)
