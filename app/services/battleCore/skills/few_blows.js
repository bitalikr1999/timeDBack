const connector = require('../../../systems/connector')

const randomIntFromInterval = (min,max) => Math.floor(Math.random()*(max-min+1)+min)

const getChance = (chance) => {
  return chance > randomIntFromInterval(0, 100)
}

const calcAttack = (attacker, target, minusArmor) => {
  let resultAttack = attacker.attack
  let evasion = false
  let resultNull = false
  if (!minusArmor) minusArmor = 0
  else target.physical_armor -= minusArmor

  if (getChance(attacker.critical_hit_chance)) {
    resultAttack *= 2
  }
  if (getChance(target.evasion)) return {
    evasion: true,
    attackPoint: 0
  }

  if(target.physical_armor > 0) resultAttack -= target.physical_armor
  if (getChance(target.evasion)) {
    evasion = true
    resultAttack = 0
  } else if (getChance(target.evasion / 2)) {
    resultAttack = resultAttack / 2
  }

  if (target.physical_armor >= attacker.attack * 2) resultNull = true

  minusArmor = target.physical_armor - attacker.attack

  if (evasion || resultNull) return 0
  return {
    attackPoint: ( resultAttack > 0) ? resultAttack : 1,
    minusArmor: ( resultAttack > 0) ? resultAttack : 0
  }
}

module.exports = async (skill, attacker, round, target) => {

  const countBlows = skill.data
  let attackPoint = 0
  let prevResult = null

  for (var i = 0; i < countBlows; i++) {
    let result = calcAttack(attacker, target, (prevResult ? prevResult.minusArmor : 0))
    if (result.attackPoint) attackPoint += result.attackPoint
    prevResult = result
  }

  target.health -= attackPoint

  if (target.health < 1) {
    await connector.end(connector.delete().from('battle_characters').where('id = ?', target.id).toString())
  } else {
    await connector.end(connector.update().table('battle_characters').set('health', target.health).where('id = ?', target.id).toString())
  }

  round.nextRound()

  return {
    targetDead: target.health < 1,
    targetId: target.id,
    attackPoint
  }
}
