const router = require('express')();

router.use('/user/', require('./user'))
router.use('/auth/', require('./auth'))
router.use('/town/', require('./town'))
router.use('/team/', require('./team'))
router.use('/barrack/', require('./barrack'))
router.use('/character/', require('./character'))
router.use('/district-enemy/', require('./districtEnemy'))
router.use('/arena/', require('./arena'))
router.use('/items/', require('./items'))
router.use('/campains/', require('./campains'))
router.use('/craft/', require('./craft'))
router.use('/visites/', require('./visites'))

module.exports = router;
