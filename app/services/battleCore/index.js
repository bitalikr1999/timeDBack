const Battle = require('./core')
const logger = require('../../systems/logger')
const allBattles = {}
const linkBattles = {}
const battleEvents = require('./events')
let emitInit = false

exports.newBatte = async (...params) => {
  logger.info('New battle')
  console.log(params)
  allBattles[params[2]] = new Battle(...params)

}

if (!emitInit) {
  battleEvents.on('delete-battle', (data) => {
    delete allBattles[data.battleId]
  })
  emitInit = true
}
