const router = require('express')();
const connector = require('../systems/connector')
const { validateToken, validateReq, parseDb } = require('../middleware')
const async = require('async')
const { townLevels } = require('../config')

router.get('/',
  validateToken(),
  async (req, res) => {
    const [ town ] = await connector.end(
      connector.select()
      .from('towns', 't')
      .field('t.id, t.level')
      .field('tt.gold_bust, tt.image, tt.name, tt.discription, tt.rarity')
      .field('m.level', 'mine_level').field('m.price', 'mine_price').field('m.gold_per_hour')
      .field('b.discount', 'barracks_discount')
      .field('b.price', 'barracks_price').field('b.level', 'barracks_level')
      .left_join('type_towns', 'tt', 'tt.id = t.type_towns_id')
      .left_join('mines', 'm', 'm.id = t.mine_id')
      .left_join('barracks', 'b', 'b.id = t.barrack_id')
      .where('t.user_id = ?', req.user.id)
      .toString()
    )
    town.level = town.level ? town.level : 1
    res.send(200, town)
  }
)

router.post('/upgrade',
  validateToken(true),
  parseDb([
    { name: 'token', model: 'towns', one: true, forResult: 'town' }
  ]),
  async (req, res) => {
    const { town } = req.dbFields
    const user = req.user
    const costUpgrade = townLevels[town.level ? town.level : 1].cost

    if (town.level > 9) return res.send(400, {town: 'Max level'})
    if (user.gold < costUpgrade) return res.send(400, {gold: 'Not have'})

    const $pay = (next) => {
      connector.end(connector.update().table('users').set(`gold = gold - ${costUpgrade}`).where('id = ?', user.id).toString())
      .then(() => next())
    }

    const $upgrade = (next) => {
      connector.end(connector.update().table('towns').set('level = level + 1').where('user_id = ?', user.id).toString())
      .then(() => next())
    }

    async.parallel([$pay, $upgrade], (error, result) => {
      if(error) {
        logger.error(error)
        return res.send(500)
      } else res.send(200)
    })
  }
)

router.post('/mine/upgrade',
  validateToken(true),
  async (req, res) => {
    const user = req.user
    let query = connector.select().from('towns', 't').field('t.id').field('m.price', 'minePrice').field('m.level')
      .left_join('mines', 'm', 't.mine_id = m.id')
      .where('t.user_id = ?', user.id)
      .toString()
    const nextLevel = await connector.end(query)
    if (nextLevel.level > 9) return res.send(400, {mine: 'Max level'})

    const userGold = user.gold
    const needGold = nextLevel.minePrice

    if (userGold < needGold) return res.send(400, {gold: 'Not have'})

    const $pay = (next) => {
      connector.end(connector.update().table('users').set(`gold = gold - ${needGold}`).where('id = ?', user.id).toString())
      .then(() => next())
    }

    const $upgrade = (next) => {
      connector.end(connector.update().table('towns').set('mine_id = mine_id + 1').where('user_id = ?', user.id).toString())
      .then(() => next())
    }

    async.parallel([$pay, $upgrade], (error, result) => {
      if(error) {
        logger.error(error)
        return res.send(500)
      } else res.send(200)

    })

  }
)
module.exports = router;
