const router = require('express')();
const connector = require('../systems/connector')
const { validateToken, validateReq, parseDb } = require('../middleware')
const { calcPower } = require('../services/power')

router.get('/:characterId',
  validateToken(),
  validateReq([
    { name: 'characterId', in: 'params', type: 'number' }
  ]),
  async (req, res) => {
    const characterId = req.params.characterId
    const userId = req.user.id

    const [ character ] = await connector.end(
      connector.select()
      .from('characters', 'ch')
      .field('ch.experience')
      .field('cl.*')
      .field('ch.id')
      .field('cl.id', 'character_level_id')
      .left_join('character_levels', 'cl', 'cl.id = ch.character_level_id')
      .where('ch.id = ?', characterId)
      .where('ch.user_id = ?', userId)
      .toString()
    )

    character.experience = character.experience ? character.experience : 0
    character.power = calcPower(character)

    const characterSkills = await connector.end(
      connector.select()
      .from('characters_levels_skills', 'cls')
      .field('s.*')
      .field('st.name', 'type_name')
      .left_join('skills', 's', 's.id = cls.skill_id')
      .left_join('skills_types', 'st', 'st.id = s.skill_type_id')
      .where('cls.character_level_id = ?', character.character_level_id)
      .toString()
    )
    character.skills = characterSkills

    const characterUpgrades = await connector.end(
      connector.select()
      .from('character_levels', 'cl')
      .where('cl.inheriot_at = ?', character.slug)
      .toString()
    )

    res.send(200, {
      character,
      characterUpgrades
    })
  }
)

router.patch('/update/:characterId',
  validateToken(),
  validateReq([
    { name: 'characterId', in: 'params', type: 'number' },
    { name: 'updateTo', in: 'body', type: 'string' }
  ]),
  parseDb([
    { name: 'updateTo', model: 'character_levels', one: true, dbField: 'slug', in: 'body' },
    { name: 'characterId', model: 'characters', one: true, dbField: 'id', in: 'params', forResult: 'character' }
  ]),
  async (req, res) => {
    const userId = req.user.id
    const { updateTo, character } = req.dbFields

    if (!updateTo || !character) {
      res.send(400)
      return
    }

    if (updateTo.need_experience > character.experience) {
      res.send(400, {
        experience: 'invalid'
      })
      return
    }

    try {
      await connector.end(
        connector.update()
        .table('characters')
        .set('character_level_id', updateTo.id)
        .set(`experience = experience - ${updateTo.need_experience}`)
        .where('id = ?', character.id)
        .toString()
      )
    } catch (error) {
      res.send(500)
      return
    }
    res.send(200)
  }
)

module.exports = router;
