'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('enemy_district_character', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true
      },
      enemy_team_id: {
        type: 'int',
        foreignKey: {
          name: 'enemy_team_enemy_district_character_f',
          table: 'enemy_district_teams',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      character_level_id: {
        type: 'int',
        foreignKey: {
          name: 'character_enemy_district_character_f',
          table: 'character_levels',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      position: {
        type: 'int'
      },
      created_at: {
        type: 'timestamp',
        notNull: true,
        defaultValue: 'CURRENT_TIMESTAMP'
      }
    },
    ifNotExists: true
  })
};

exports.down = function(db) {
  return db.dropTable('enemy_district_character');
};

exports._meta = {
  "version": 1
};
