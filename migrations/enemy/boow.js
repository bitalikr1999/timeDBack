const connector = require('../../app/systems/connector')


connector.end(
  connector.insert().into('character_levels')
  .setFieldsRows([
    // {
    //   img: '/character/boow/thumbnail/boow.jpg',
    //   name: 'Лучник I',
    //   slug: 'boowI',
    //   attack: 1,
    //   health: 4,
    //   type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
    //   evasion: 0,
    //   full_img: '/character/boow/boow.jpg',
    //   initiative: 5,
    //   theft_life: 0,
    //   inheriot_at: 0,
    //   type_attack: 2,
    //   magic_armor: 0,
    //   physical_armor: 0,
    //   number_actions: 2,
    //   need_experience: 0,
    //   critical_hit_chance: 0,
    //   group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'people')
    // },
    // {
    //   img:'/character/boow/thumbnail/boow.jpg',
    //   name: 'Лучник II',
    //   slug: 'boowII',
    //   attack: 2,
    //   health: 6,
    //   type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'distance_rifle'),
    //   evasion: 0,
    //   full_img: '/character/boow/boow.jpg',
    //   initiative: 6,
    //   theft_life: 0,
    //   inheriot_at: 'boowI',
    //   type_attack: 2,
    //   magic_armor: 0,
    //   physical_armor: 0,
    //   number_actions: 2,
    //   need_experience: 200,
    //   critical_hit_chance: 0,
    //   group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'people')
    // },
    {
      img:'/character/boow/thumbnail/boow.jpg',
      name: 'Лучник III',
      slug: 'boowIII',
      attack: 2,
      health: 6,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'distance_rifle'),
      evasion: 0,
      full_img: '/character/boow/boow.jpg',
      initiative: 6,
      theft_life: 0,
      inheriot_at: 'boowII',
      type_attack: 2,
      magic_armor: 1,
      physical_armor: 1,
      number_actions: 2,
      need_experience: 500,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'people')
    },
    {
      img:'/character/boow/thumbnail/PrivateArcher.jpg',
      name: 'Усиленый лучник',
      slug: 'PrivateArcher',
      attack: 3,
      health: 6,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'distance_rifle'),
      evasion: 0,
      full_img: '/character/boow/2/PrivateArcher.jpg',
      initiative:7,
      theft_life: 0,
      inheriot_at: 'boowIII',
      type_attack: 2,
      magic_armor: 1,
      physical_armor: 1,
      number_actions: 2,
      need_experience: 700,
      critical_hit_chance: 0,
      group_id: connector.select().from('characters_groups').field('id').where('slug = ?', 'people')
    }
  ]).toString()
)
