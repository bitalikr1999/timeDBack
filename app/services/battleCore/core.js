const Round = require('./round')
const charactersService = require('../characters')
const battleCharacters = require('./characters')
const logger = require('../../systems/logger')
const battleEvents = require('./events')
const { battles: battlesH } = require('./helpers')

const { attack: AttackAction } = require('./actions/attack')
const HealAction = require('./actions/heal')
const BotAction = require('./actions/bot')
const WinAction = require('./actions/winCalc')
const Finishes = require('./finish')
const skillAction = require('./actions/skill')
const buffsAction = require('./actions/buffs')

const timmerCount = 30
const battleTypes = ['enemy', 'arena', 'disctrict', 'campain']

class Turn {
  constructor (actionName, time, timeOut, failCallback) {
    this.actionName = actionName
    this.time = time
    this.timeOut = timeOut
    this.failCallback = failCallback
  }
}

module.exports = class Battle {
  constructor (parentFirst, parentSecond, roomId, typeBattle ) {
    this.parentFirst = parentFirst
    this.parentSecond = parentSecond
    if (battleTypes.includes(typeBattle)) this.typeBattle = typeBattle
    else this.typeBattle = 'simple'
    this.actionsTurn = []
    this.round = new Round()
    this.timmer = null
    this.roomId = roomId
    this.sockets = {}
    this.init()
  }

  emit(emit, data, to) {
    console.log('Battle: '+this.roomId+ '| Emit: ' + emit + 'To:' + to)
    if (to) {
      if (Number(to) === Number(this.parentFirst.id)) to = this.parentFirst.login
      else to = this.parentSecond.login

      if (to) global.io.sockets.to( global.userSockets[to] ).emit(emit, data);
    } else {
      global.io.sockets.to(this.roomId).emit(emit, data)
    }
  }

  async init () {
    if (!this.parentFirst.isUser || !this.parentSecond.isUser) {
      this.withBot = true
      this.parentSecond.id = this.typeBattle + '/' + this.parentSecond.id
    }

    /* Init round */
    await battleCharacters.clearCharacters(this.parentFirst.id, this.parentSecond.id)

    const $getFirstTeam = charactersService.selectTeam(this.parentFirst.id, this.parentFirst.type)
    const $getSeconTeam = charactersService.selectTeam(this.parentSecond.id, this.parentSecond.type, ( this.withBot ? this.typeBattle : false))

    let [ team1, team2 ] = await Promise.all([$getFirstTeam, $getSeconTeam])

    /* Create battle characters */
    const { insertId: battleId } = await battlesH.newBattle()
    const battleTeam1 = await battleCharacters.createBattleCharacters(team1, battleId)
    const battleTeam2 = await battleCharacters.createBattleCharacters(team2, battleId)

    team1 = battleCharacters.mergeTeam(team1, battleTeam1)
    team2 = battleCharacters.mergeTeam(team2, battleTeam2)

    await battleCharacters.initPassiveBuffs(team1)

    this.round.init(team1.characters, team2.characters)

    this.emit('battle/new', {
      status: true,
      teams: {
        [this.parentFirst.id]: team1.characters,
        [this.parentSecond.id]: team2.characters
      },
      round: this.round.getAll()
    })

    this.addActions('waitAttack', timmerCount, 'attackNotReceived', [this.round.getNextAttacker().parentId])
    this.setSockets()
    if (this.withBot) this.checkBotTurn()
  }

  addActions (name, time, callback, parramsForCallback) {
    if (this.actionsTurn.length !== 0) this.endTimmer()
    this.actionsTurn.push(
      new Turn('waitAttack',
        new Date().getTime(),
        new Date().getTime() + time * 1000,
        this[callback](...parramsForCallback)
      )
    )
    if (this.actionsTurn.length === 1) this.startTimmer()
  }

  startTimmer () {
    this.timmer = setInterval(() => {
      const now = new Date().getTime()
      if (now >= this.actionsTurn[0]['timeOut']) {
        this.actionsTurn[0].failCallback()
        this.actionsTurn.shift()
        if (!this.actionsTurn.length) clearInterval(this.timmer)
      }
    }, 1000)
  }

  endTimmer () {
    this.actionsTurn = []
    clearInterval(this.timmer)
  }

  attackNotReceived (attackerId) {
    return () => {
      setTimeout(() => {
        this.addActions('waitAttack', timmerCount, 'attackNotReceived', [this.round.getNextAttacker().parentId])
        this.emit('battle/next-turn')
        this.round.nextRound()
        if (this.withBot) this.checkBotTurn()
      }, 100)
    }
  }

  setSockets () {
    const socketFirst = global.Sockets[global.userSockets[this.parentFirst.login]]
    socketFirst.socket.on('battle/attack', (data) => this.attack(data, this.parentFirst))
    socketFirst.socket.on('battle/move', (data) => this.move(data, this.parentFirst))
    socketFirst.socket.on('battle/heal', (data) => this.heal(data, this.parentFirst))
    socketFirst.socket.on('battle/use-skill', (data) => this.useSkill(data, this.parentFirst))

    if (this.withBot) return;

    const socketSecond = global.Sockets[global.userSockets[this.parentSecond.login]]
    socketSecond.socket.on('battle/attack', (data) => this.attack(data, this.parentSecond))
    socketSecond.socket.on('battle/move', (data) => this.move(data, this.parentSecond))
    socketFirst.socket.on('battle/heal', (data) => this.heal(data, this.parentSecond))
    socketFirst.socket.on('battle/use-skill', (data) => this.useSkill(data, this.parentSecond))

  }

  async nextTurn (socketUrl, data, updateMana, targetDead, nextRound) {
    let buffsResult = null;

    if (targetDead) {
      data.newRound = this.round.deleteCharacter(data.targetId)
      data.teamWinId = this.round.checkIsTeamDead()
    }

    if (updateMana && data.attackerId) data.mana = await battleCharacters.addMana(data.attackerId, 1)

    if (nextRound) {
      buffsResult = await buffsAction.checkActiveBuffs(data.attackerId, this.round)
    }

    this.emit(socketUrl, data)

    if (buffsResult) {
      if (buffsResult.targetDead) {
        data.newRound = this.round.deleteCharacter(buffsResult.deadId)
        data.teamWinId = this.round.checkIsTeamDead()
      }
      this.emit('battle/buffs', {
        buffs: buffsResult.emitedResult,
        characterId: data.attackerId,
        newRound: data.newRound,
        teamId: (buffsResult.characterTeamId == this.parentFirst.id) ? this.parentFirst.id : this.parentSecond.id
      })
    }

    if (data.teamWinId) return this.finishFight(data.teamWinId)

    this.addActions('waitAttack', timmerCount, 'attackNotReceived', [this.round.getNextAttacker().parentId])

    setTimeout(() => {
      if (this.withBot) this.checkBotTurn()
    }, 0)
  }

  async attack (data, parent) {
    const attacker  = this.round.getNextAttacker()

    if (attacker.parentId != parent.id) {
      this.emit('battle/not-you-turn', {}, parent.id)
      return;
    }

    const { attackPoint, targetDead} = await AttackAction({
      targetId: data.targetId,
      attackerId: attacker.id
    })

    this.round.nextRound()

    this.nextTurn('battle/attack-sus', {
      attackerTeamId: attacker.parentId,
      attackerId: attacker.id,
      attackPoint,
      targetId: data.targetId
    }, true, targetDead, true)
  }

  async checkBotTurn () {
    if (this.round.getNextAttacker().parentId !== this.parentSecond.id) return;

    const botAction = await BotAction({
      targetTeamId: this.parentFirst.id,
      attackerId: this.round.getNextAttacker().id
    })

    switch (botAction.code) {
      case 1:
        setTimeout(() => {
          this.attack({ targetId: botAction.data.id }, this.parentSecond)
        }, 500);
      break;
      case 2:
        setTimeout(() => {
          this.move({
            characterId: this.round.getNextAttacker().id,
            moveTo: botAction.data
          }, this.parentSecond)
        }, 500);
      break;
      case 3:
        setTimeout(() => {
          this.skip()
        }, 500);
      break;
      default:;
    }
  }

  async move (data, parent) {
    const { characterId, moveTo } = data
    await battleCharacters.moveCharacter(characterId, moveTo)

    this.round.nextRound()

    this.nextTurn('battle/move', {
      teamId: parent.id,
      moveTo: moveTo,
      attackerId: characterId
    }, true, false, true)
  }

  async skip () {
    const attackerId = this.round.getNextAttacker().id
    this.round.nextRound()
    this.nextTurn('battle/next-turn', {
      attackerId
    }, true, false, true)
  }

  async heal (data, parent) {
    const healler = this.round.getNextAttacker()

    if (healler.parentId != parent.id) return this.emit('battle/not-you-turn', {}, parent.id)

    const result = await HealAction(healler.id, data.targetId)

    this.round.nextRound()

    this.nextTurn('battle/heal', {
      teamId: parent.id,
      healPoint: result.healPoint,
      targetId: data.targetId
    }, true, false, true)

  }

  async finishFight (winTeamId) {
    let isUserWin = (this.withBot && this.parentSecond.id === winTeamId) ? false : true
    let lossTeamId = (winTeamId == this.parentSecond.id) ? this.parentFirst.id : this.parentSecond.id
    let savedIds = {
      win: winTeamId,
      loss: lossTeamId
    }

    if (this.withBot) {
      if (isUserWin) {
        lossTeamId = lossTeamId.split(this.typeBattle + '/')[1]
      } else {
        winTeamId = winTeamId.split(this.typeBattle + '/')[1]
      }
      if (this.typeBattle == 'enemy') await charactersService.removeDistrictTeam(this.parentSecond.id.split(this.typeBattle + '/')[1], this.parentSecond.town_id)
    }

    this.endTimmer()

    this.emit('battle/end', { winTeamId })

    if (!isUserWin) this.emit('battle/loss', {}, lossTeamId)

    const resultBattleByType = await this.finishByType(winTeamId, lossTeamId, isUserWin)

    if (isUserWin) {
      let result = await WinAction({
        id: winTeamId,
        type: 'user'
      },
      {
        id: lossTeamId,
        type: this.withBot ? 'enemy' : 'user'
      })

      this.emit('battle/win', {
        gold: result.gold,
        exp: result.exp,
        typeBattle: this.typeBattle,
        data: resultBattleByType
      }, winTeamId)
    }

    await battleCharacters.clearCharacters(savedIds.win, savedIds.loss)

    battleEvents.emit('delete-battle', {
      battleId: this.roomId
    })
  }

  async finishByType (teamWinId, teamLossId, isUserWin) {
    if (!Finishes[this.typeBattle]) return false
    return await Finishes[this.typeBattle]({
      teamWinId,
      teamLossId,
      isUserWin,
      round: this.round
    })
  }


  async useSkill (data, parent) {
    const activeCharacter  = this.round.getNextAttacker()
    let   newRound  = false
    let   teamWinId = null

    if (activeCharacter.parentId != parent.id) {
      this.emit('battle/not-you-turn', {}, parent.id)
      return;
    }

    const {
      attackPoint,
      targetDead,
      skillName,
      isNotAttack,
      nextRound,
      characterMana,
      error,
      targetId,
      skillSlug
    } = await skillAction.useActive({
      targetId: data.targetId,
      attackerId: activeCharacter.id,
      skillId: data.skillId,
      round: this.round
    })

    if (error) return this.emit('battle/skill-sus', {
        error: true,
        field: 'mana'
      })


    this.nextTurn('battle/skill-sus', {
      attackerTeamId: activeCharacter.parentId,
      targetId: data.targetId,
      attackPoint,
      nextRound,
      newRound,
      isNotAttack,
      skillName,
      mana: characterMana,
      targetId,
      skillSlug
    }, false, targetDead, (nextRound === false) ? false : true)

  }




}
