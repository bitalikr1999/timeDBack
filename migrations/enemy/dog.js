const connector = require('../../app/systems/connector')


connector.end(
  connector.insert().into('character_levels')
  .setFieldsRows([
    {
      img: '/character/warrior/thumbnail/warriorI.jpg',
      name: 'Воин I',
      slug: 'warriorI',
      attack: 1,
      health: 6,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 0,
      full_img: '/character/warrior/warriorI.jpg',
      initiative: 1,
      theft_life: 0,
      inheriot_at: null,
      type_attack: 1,
      magic_armor: 0,
      physical_armor: 1,
      number_actions: 1,
      need_experience: 0,
      critical_hit_chance: 0
    },
    {
      img: '/character/warrior/thumbnail/warriorII.jpg',
      name: 'Воин II',
      slug: 'warriorII',
      attack: 2,
      health: 6,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 0,
      full_img: '/character/warrior/warriorII.jpg',
      initiative: 1,
      theft_life: 0,
      inheriot_at: 'warriorI',
      type_attack: 1,
      magic_armor: 0,
      physical_armor: 1,
      number_actions: 1,
      need_experience: 150,
      critical_hit_chance: 0
    },
    {
      img: '/character/warrior/thumbnail/SwordMaster.jpg',
      name: 'Воин III',
      slug: 'warriorIII',
      attack: 2,
      health: 6,
      type_id: connector.select().from('type_characters').field('id').where('slug = ?', 'melee'),
      evasion: 0,
      full_img: '/character/warrior/SwordMaster.jpg',
      initiative: 1,
      theft_life: 0,
      inheriot_at: 'warriorII',
      type_attack: 1,
      magic_armor: 0,
      physical_armor: 2,
      number_actions: 1,
      need_experience: 400,
      critical_hit_chance: 0
    }
  ]).toString()
)
