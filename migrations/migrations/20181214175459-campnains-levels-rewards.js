'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('campains_levels_rewards', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true
      },
      campain_level_id: {
        notNull: true,
        type: 'int',
        foreignKey: {
          name: 'campains_levels_campains_rewards_f',
          table: 'campains_levels',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      item_id: {
        notNull: true,
        type: 'int',
        foreignKey: {
          name: 'items_campains_rewards_f',
          table: 'items',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      min_count: {
        type: 'int',
        default: 0
      },
      max_count: {
        type: 'int',
        default: 2
      },
      created_at: {
        type: 'timestamp',
        notNull: true,
        defaultValue: 'CURRENT_TIMESTAMP'
      }
    },
    ifNotExists: true
  })
};

exports.down = function(db) {
  return db.dropTable('campains');
};

exports._meta = {
  "version": 1
};
