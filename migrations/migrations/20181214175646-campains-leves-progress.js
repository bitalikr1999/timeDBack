'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('users_campains_levels', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true
      },
      campain_level_id: {
        notNull: true,
        type: 'int',
        foreignKey: {
          name: 'campains_levels_users_campains_levels_f',
          table: 'campains_levels',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      user_campain_id: {
        notNull: true,
        type: 'int',
        foreignKey: {
          name: 'users_campains_users_campains_levels_f',
          table: 'users_campains',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      is_complete_1: {
        type: 'boolean',
        default: false
      },
      is_complete_2: {
        type: 'boolean',
        default: false
      },
      is_complete_3: {
        type: 'boolean',
        default: false
      },
      created_at: {
        type: 'timestamp',
        notNull: true,
        defaultValue: 'CURRENT_TIMESTAMP'
      }
    },
    ifNotExists: true
  })
};

exports.down = function(db) {
  return db.dropTable('campains');
};

exports._meta = {
  "version": 1
};
