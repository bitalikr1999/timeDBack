'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('users', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true
      },
      name: 'string',
      login: {
        type: 'string',
        unique: true,
        notNull: true
      },
      email: {
        type: 'string',
        unique: true,
        notNull: true
      },
      gold: {
        type: 'int',
        default: 0
      },
      password: {
        type: 'string',
        notNull: true
      },
      rating: {
        type: 'int',
        default: 0
      },
      active: {
        type: 'boolean',
        default: true
      },
      created_at: {
        type: 'timestamp',
        notNull: true,
        defaultValue: 'CURRENT_TIMESTAMP'
      }

    },
    ifNotExists: true
  })
};

exports.down = function(db) {
  return db.dropTable('users');
};

exports._meta = {
  "version": 1
};
