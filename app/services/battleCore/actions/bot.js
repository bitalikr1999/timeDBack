const connector = require('../../../systems/connector')
const battleCharacters = require('../characters')

const clearDeath = (team) => {
  let result = []
  for (let index = 0; index < team.length; index++) {
    const element = team[index];
    if (element.health > 0) result.push(element)
  }

  return result
}

const getTypeCodePosition = (target) => {
  switch (target.typeAttack) {
    case 'melee':
      return 1;
    break;
    case 'distance_rifle':
    case 'distance_magic':
      return 2
    break;
    case 'medication':
      return 3;
    break;
  }
}

const getTypeCodeAttack = (target) => {
  switch (target.typeAttack) {
    case 'melee':
    case 'distance_rifle':
      return 1
    break;
    case 'distance_magic':
    case 'medication':
      return 2
    break;
  }
}

const clearByType = (attacker, team) => {
  const attackerCode = getTypeCodePosition(attacker)
  if(attackerCode == 1) {
    if(attacker.position > 5) return false

    let array = []
    team.forEach((element, i) => {
      if(element.position < 6) array.push(element)
    })

    if(array.length > 0) return array
    else return team
  } else return team
}

const clearByArmor = (attacker, team) => {
  const typeAttack = getTypeCodeAttack(attacker)

  if (!Array.isArray(team)) return false

  let array = []
  if(typeAttack == 1) {
    team.forEach((element, i) => {
      if(element.physical_armor < element.magic_armor) {
        array.push(element)
      }
    })

    if(!array || !array.length || array.length < 1) {
      array = team
      array.sort((a, b) => a.physical_armor > b.physical_armor)
      return [array[0]]
    } else return [array]
  } else {
    team.forEach((element, i) => {
      if(element.physical_armor > element.magic_armor) {
        array.push(element)
      }
    })

    if(!array || !array.length || array.length < 1) {
      array = team
      array.sort((a, b) => a.physical_armor < b.physical_armor)
      return [array[0]]
    } else return array
  }
}

const clearByHealth = (team) => {
  if (!Array.isArray(team)) return false
  return team.sort((a, b) => a.health > b.health)
}

const moveTo = (team) => {
  let result = null
  let freePosition = [0, 0, 0, 0, 0]
  team.forEach((val) => {
    if(val.position < 6 && val.health > 0) freePosition[val.position - 1] = 1
  })
  let haveFree = false
  freePosition.forEach((val, i) => {
    if (!val) {
      result = i + 1
    }
  })
  return result ? result : false
}

module.exports = async (data) => {
  const { attackerId, targetTeamId } = data
  const [ attacker ] = await battleCharacters.getTwoCharacters(attackerId, null)
  console.log()
  let   team = await battleCharacters.getTeam(targetTeamId)
  let   teamByType = []

  if(team.length) team = clearDeath(team)
  if(team.length) {
    team = clearByType(attacker, team)
    teamByType = JSON.parse(JSON.stringify(team))
  }
  if(team) team = clearByArmor(attacker, team)
  if(team) team = clearByHealth(team)

  if(Array.isArray(team)) {
    if (team.length > 0) {
      return {
        code: 1,
        data: team[0]
      }
    } else {
      return {
        code: 1,
        data: teamByType[0]
      }
    }
  }

  const attackerTeam = await battleCharacters.getTeam(attacker.owner_id)
  const moveToPosition = moveTo(attackerTeam)
  if (moveToPosition)
    return {
      code: 2,
      data: moveToPosition
    }
  else
    return {
      code: 3
    }

}
