'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('craft_recipes', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: 'string'
      },
      slug: {
        type: 'string',
        unique: true,
        notNull: true
      },
      min_craft_level: {
        type: 'int',
        default: 0,
      },
      craft_recipe_type: {
        notNull: true,
        type: 'int',
        foreignKey: {
          name: 'craft_recipes_craft_recipes_type_f',
          table: 'craft_recipes_types',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      created_at: {
        type: 'timestamp',
        notNull: true,
        defaultValue: 'CURRENT_TIMESTAMP'
      }
    },
    ifNotExists: true
  })
};

exports.down = function(db) {
  return db.dropTable('craft_recipes');
};

exports._meta = {
  "version": 1
};
