const connector = require('../../../../app/systems/connector')

const campain = connector.select().from('campains').field('id').where(
  'town_type_id = ?', (connector.select().from('type_towns').field('id').where('slug = ?', 'village'))
)

connector.end(
  connector.insert().into('campains_levels')
  .setFieldsRows([
    {
      campain_id: campain,
      name: 'Уровень 1',
      reward_exp: 10,
      reward_gold: 5,
      slug: 'village_lvl_1'
    },
    {
      campain_id: campain,
      name: 'Уровень 2',
      reward_exp: 10,
      reward_gold: 5,
      slug: 'village_lvl_2'
    },
    {
      campain_id: campain,
      name: 'Уровень 3',
      reward_exp: 10,
      reward_gold: 5,
      slug: 'village_lvl_3'
    },
    {
      campain_id: campain,
      name: 'Уровень 4',
      reward_exp: 10,
      reward_gold: 5,
      slug: 'village_lvl_4'
    },
    {
      campain_id: campain,
      name: 'Уровень 5',
      reward_exp: 10,
      reward_gold: 5,
      slug: 'village_lvl_5'
    },
  ]).toString()
)
