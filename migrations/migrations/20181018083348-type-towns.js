'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('type_towns', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true
      },
      name: 'string',
      slug: {
        type: 'string',
        unique: true,
        notNull: true
      },
      discription: {
        type: 'text'
      },
      rarity: {
        type: 'string'
      },
      image: {
        type: 'string'
      },
      chance_find: {
        type: 'int'
      },
      gold_bust: {
        type: 'int'
      },
      created_at: {
        type: 'timestamp',
        notNull: true,
        defaultValue: 'CURRENT_TIMESTAMP'
      }
    },
    ifNotExists: true
  })
};

exports.down = function(db) {
  return db.dropTable('type_towns');
};

exports._meta = {
  "version": 1
};
