const connector = require('../../app/systems/connector')


connector.end(
  connector.insert().into('items')
  .setFieldsRows([
    {
      slug: 'i-playoni-shard-1',
      name: 'I Осколоки плейоны',
      discription: 'В этом осколке заключенна древняя магия',
      img: 'items-playoni-shard-1',
      type_id: connector.select().from('items_types').field('id').where('slug = ?', 'material')
    },
    {
      slug: 'i-playoni-shard-2',
      name: 'II Осколоки плейоны',
      discription: 'В этом осколке заключенна древняя магия',
      img: 'items-playoni-shard-2',
      type_id: connector.select().from('items_types').field('id').where('slug = ?', 'material')
    },
    {
      slug: 'i-wood-1',
      name: 'Бревно',
      discription: '',
      img: 'items-wood-1',
      type_id: connector.select().from('items_types').field('id').where('slug = ?', 'material')
    },
    {
      slug: 'i-wood-2',
      name: 'Простая доска',
      discription: '',
      img: 'items-wood-2',
      type_id: connector.select().from('items_types').field('id').where('slug = ?', 'material')
    },
    {
      slug: 'i-wood-3',
      name: 'Точеная доска',
      discription: '',
      img: 'items-wood-3',
      type_id: connector.select().from('items_types').field('id').where('slug = ?', 'material')
    },
    {
      slug: 'i-stone-1',
      name: 'Камни',
      discription: '',
      img: 'items-stone-1',
      type_id: connector.select().from('items_types').field('id').where('slug = ?', 'material')
    },
    {
      slug: 'i-stone-2',
      name: 'Каменый блок',
      discription: '',
      img: 'items-stone-2',
      type_id: connector.select().from('items_types').field('id').where('slug = ?', 'material')
    },
    {
      slug: 'i-stone-3',
      name: 'Полированый каменный блок',
      discription: '',
      img: 'items-stone-3',
      type_id: connector.select().from('items_types').field('id').where('slug = ?', 'material')
    },
  ]).toString()
)
