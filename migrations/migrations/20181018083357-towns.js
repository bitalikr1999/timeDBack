'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('towns', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true
      },
      level: {
        type: 'int'
      },
      user_id: {
        type: 'int',
        foreignKey: {
          name: 'towns_characters_f',
          table: 'users',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
          }
        }
      },
      mine_id: {
        type: 'int',
        foreignKey: {
          name: 'towns_mine_f',
          table: 'mines',
          mapping: 'id',
          rules: {
            onDelete: 'RESTRICT',
            onUpdate: 'RESTRICT'
          }
        }
      },
      barrack_id: {
        type: 'int',
        foreignKey: {
          name: 'towns_barracks_f',
          table: 'barracks',
          mapping: 'id',
          rules: {
            onDelete: 'RESTRICT',
            onUpdate: 'RESTRICT'
          }
        }
      },
      type_towns_id: {
        type: 'int',
        foreignKey: {
          name: 'towns_type_towns_f',
          table: 'type_towns',
          mapping: 'id',
          rules: {
            onDelete: 'RESTRICT',
            onUpdate: 'RESTRICT'
          }
        }
      },
      created_at: {
        type: 'timestamp',
        notNull: true,
        defaultValue: 'CURRENT_TIMESTAMP'
      }
    },
    ifNotExists: true
  })
};

exports.down = function(db) {
  return db.dropTable('towns');
};

exports._meta = {
  "version": 1
};
