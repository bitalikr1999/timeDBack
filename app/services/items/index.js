const characterCardAction = require('./types/characterCard')
const chestAction = require('./types/chest')
const citySphereAction = require('./types/citySphere')
const connector = require('../../systems/connector')
const {
  getUserItem,
  removeItem,
  getUserItems,
  addItems,
  minusCount,
  checkAvailabityItems
} = require('./helpers')


const useItem = async (itemId, userId) => {
  const [ item ] = await getUserItem(itemId)
  let result;

  switch(item.type_slug) {
    case 'character-card':
      result = await characterCardAction(item, userId)
    break;

    case 'chest':
      result = await chestAction(item, userId)
    break;

    case 'city_sphere':
      result = await citySphereAction(item, userId)
    break;
  }

  if (item.count < 2) {
    await removeItem(itemId)
  } else {
    await minusCount(itemId)
  }

  return result
}

module.exports = {
  useItem,
  getUserItem,
  removeItem,
  getUserItems,
  addItems,
  minusCount,
  checkAvailabityItems 
}
