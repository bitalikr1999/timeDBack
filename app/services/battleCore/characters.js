const config = require('../../config')
const connector = require('../../systems/connector')
const logger = require('../../systems/logger')
const { getSkillsByGroup } = require('../skills')

connector.end('DELETE FROM battle_characters')
.then(() => logger.info('Battle characters is clean'))

const createBattleCharacters = async (team, battleId) => {
  const characters = team.characters
  const query = connector.insert().into('battle_characters')
  const characterToSave = []
  console.log(battleId)
  characters.forEach(val => {
    const battleChar = {
      position  : val.position,
      health    : val.health,
      mana      : 5,
      parent_id : val.id,
      character_level_id: val.characterLevelId,
      owner_id  : team.parentId,
      battle_id : battleId
    }

    switch (team.parentType) {
      case 'user'   : battleChar.parent_table = 'characters'; battleChar.owner_type = 'user';break;
      case 'enemy'  : battleChar.parent_table = 'enemy_district_character'; battleChar.owner_type = 'enemy'; break;
      case 'campain'  : battleChar.parent_table = 'campains_levels_enemy'; battleChar.owner_type = 'campain'; break;
    }

    characterToSave.push(battleChar)
  })

  query.setFieldsRows(characterToSave)

  await connector.end(query.toString())

  return await connector.end(
     connector.select()
    .from('battle_characters', 'bc')
    .field('bc.id, bc.parent_id, bc.owner_id')
    .field('cl.id', 'character_level_id')
    .left_join('character_levels', 'cl', 'cl.id = bc.character_level_id')
    .where('bc.owner_id = ?', team.parentId)
    .where('bc.owner_type = ?', team.parentType)
    .toString()
  )
}

const getTwoCharacters = async (firstId, secondId, autoMergeBuffs) => {
  const query = connector.select()
    .from('battle_characters', 'bc')
    .field('cl.*')
    .field('cl.health', 'max-health')
    .field('cl.mana', 'max-mana')
    .field('bc.*')
    .field('tc.slug', 'typeAttack')
    .left_join('character_levels', 'cl', 'cl.id = bc.character_level_id')
    .left_join('type_characters', 'tc', 'tc.id = cl.type_id')

  if (secondId) {
    query.where(`bc.id = ${firstId} OR bc.id = ${secondId}`)
  } else {
    query.where(`bc.id = ${firstId}`)
  }

  const characters = await connector.end(query.toString())

  await Promise.all(
    characters.map(async (val, i) => {
      characters[i].buffs = await getCharacterBuffs(val.id)
    })
  );

  const charactersObj = {}
  characters.forEach(val => {
    if (autoMergeBuffs) val = mergWithBuffs(val)
    charactersObj[val.id] = val
  })

  return [charactersObj[firstId], charactersObj[secondId]]
}

const getCharacterBuffs = async (characterId) => {
  return await connector.end(
    connector.select()
    .from('battle_characters_buffs', 'bcb')
    .field('sb.*')
    .field('bcb.id')
    .field('bcb.data')
    .field('sb.id', 'buff_id')
    .field('sb.data', 'buff_data')
    .left_join('skills_buffs', 'sb', 'sb.id = bcb.skill_buff_id')
    .where('bcb.battle_character_id = ?', characterId)
    .toString()
  )
}

const mergWithBuffs = (character) => {
  const buffs = character.buffs

  if (!buffs.length) return character

  buffs.forEach(val => {
    switch (val.slug) {
      case 'basic_magnification':
        const data = JSON.parse(val.data)
        Object.keys(data).forEach(key => {
          character[key] = Number(character[key]) + Number(data[key])
        })
      break;
    }
  })

  return character
}

const mergeTeam = (team, newTeam) => {
  const teamForAdds = {}
  newTeam.forEach(val => {
    teamForAdds[val.parent_id] = val
  })

  team.characters.forEach((val, i, arr) => {
    arr[i].max_health = val.health
    arr[i].max_mana = val.mana
    arr[i].mana = 1
    arr[i].parentId = teamForAdds[val.id].owner_id
    arr[i].id = teamForAdds[val.id].id
  })

  return team
}

const getTeam = async (teamId) => {
  const query = connector.select()
    .from('battle_characters', 'bc')
    .field('cl.*')
    .field('cl.health', 'max_health')
    .field('cl.mana', 'max_mana')
    .field('bc.*')
    .field('tc.slug', 'typeAttack')
    .left_join('character_levels', 'cl', 'cl.id = bc.character_level_id')
    .left_join('type_characters', 'tc', 'tc.id = cl.type_id')
    .where(`bc.owner_id = ?`, teamId)
    .toString()
  return await connector.end(query)
}

const moveCharacter = async (characterId, moveTo) => {
  return await connector.end(
    connector.update()
    .table('battle_characters')
    .set('position', moveTo)
    .where('id = ?', characterId)
    .toString()
  )
}

const clearCharacters = async (teamId1, teamId2) => {
  return await connector.end(
    connector.delete()
    .from('battle_characters')
    .where('')
    .where(`owner_id = '${teamId1}' OR owner_id = '${teamId2}'`)
    .toString()
  )
}

const addMana = async (characterId, manaPoint) => {
  const [ character ] = await getTwoCharacters(characterId)
  if (manaPoint + character.mana > character['max-mana']) {
    manaPoint = character['max-mana'] - character.mana
  }
  await connector.end(
    connector.update()
    .table('battle_characters')
    .set('mana', character.mana + manaPoint)
    .where('id = ?', characterId)
    .toString()
  )

  return character.mana + manaPoint
}

const minusMana = async (characterId, manaPoint) => {
  const [ character ] = await getTwoCharacters(characterId)
  if (character.mana < manaPoint) return false;
  if (manaPoint + character.mana > character['max-mana']) {
    manaPoint = character['max-mana'] - character.mana
  }
  await connector.end(
    connector.update()
    .table('battle_characters')
    .set('mana', character.mana - manaPoint)
    .where('id = ?', characterId)
    .toString()
  )
  return character.mana - manaPoint
}

const addBuff = async (characterId, buffId, data) => {
  return await connector.end(
    connector.insert()
    .into('battle_characters_buffs')
    .setFields({
      battle_character_id: characterId,
      skill_buff_id: buffId,
      data: JSON.stringify(data)
    })
    .toString()
  )
}

const initPassiveBuffs = async (team1) => {
  const buffs = []
  const characterBuffs = []

  team1.characters.forEach(val => {
    val.skills.forEach(skill => {
      if (skill.group_slug != 'pas_magnification_rase') return;
      buffs.push({
        skill_buff_id: skill.buff_id,
        data: skill.data
      })
    })

  })

  buffs.forEach(skill => {
    team1.characters.forEach(character => {
      characterBuffs.push({
        skill_buff_id: skill.skill_buff_id,
        data: skill.data,
        battle_character_id: character.id
      })
    })
  })

  if (!characterBuffs.length) return false
  return await connector.end(
    connector.insert()
    .into('battle_characters_buffs')
    .setFieldsRows(characterBuffs)
    .toString()
  )
}

const getBuffs = async (characterId) => {
  return await connector.end(
    connector.select()
    .from('battle_characters_buffs', 'bcb')
    .field('bcb.*')
    .field('sb.slug')
    .field('sb.name')
    .left_join('skills_buffs', 'sb', 'sb.id = bcb.skill_buff_id')
    .where('bcb.battle_character_id = ?', characterId)
    .toString()
  )
}

module.exports = {
  createBattleCharacters,
  getTwoCharacters,
  mergeTeam,
  getTeam,
  moveCharacter,
  clearCharacters,
  addMana,
  minusMana,
  addBuff,
  mergWithBuffs,
  getCharacterBuffs,
  initPassiveBuffs,
  getBuffs
}
